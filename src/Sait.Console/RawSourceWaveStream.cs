﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NAudio.Wave;

namespace Sait.Console
{
    public class RawSourceWaveStream : WaveStream
    {
        private readonly Stream _source;

        public RawSourceWaveStream(Stream source, WaveFormat format)
        {
            _source = source;
            WaveFormat = format;
        }

        public override WaveFormat WaveFormat { get; }

        public override long Length => _source.Length;

        public override long Position
        {
            get => _source.Position;
            set => _source.Position = value;
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return _source.Read(buffer, offset, count);
        }
    }
}
