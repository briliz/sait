﻿using JetBrains.Annotations;

namespace Sait.Console
{
    using System;
    using System.IO;
    using NAudio.Wave;
    using System.Management.Automation;

    [Cmdlet(VerbsCommon.New, "SampleProvider")]
    public class NewSampleProviderCmdlet : Cmdlet
    {
        private static readonly ILogger Logger = new Log4NetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Parameter(Mandatory = true, HelpMessage = "Wav File Path")]
        public string Path { get; [UsedImplicitly] private set; }

        protected override void ProcessRecord()
        {
            Logger.LogDebug("ProcessRecord()");

            // Get path and check it is valid
            if (string.IsNullOrEmpty(Path))
            {
                WriteWarning(Logger.LogError("Empty wav file path. Operation aborted"));
                return;
            }

            if (!File.Exists(Path))
            {
                WriteWarning(Logger.LogError($"Cannot find wav file with path: {Path}"));
                return;
            }

            // Open Wav File and convert to a IEEE Sample Provider
            try
            {
                using (var reader = new WaveFileReader(Path))
                {
                    Logger.LogDebug($"Created new Wav file reader: {reader.WaveFormat} for file with Path: {Path}");

                    var waveFormat = WaveFormat.CreateALawFormat(8000, 1);
                    var aLawStream = new RawSourceWaveStream(reader, waveFormat);
                    Logger.LogDebug($"Created new Raw Wav Stream: {aLawStream.WaveFormat} for file with path: {Path}");

                    using (var pcmStream = WaveFormatConversionStream.CreatePcmStream(aLawStream))
                    {
                        Logger.LogDebug($"Created new PCM Stream: {pcmStream.WaveFormat} for file with path: {Path}");

                        var floatProvider = new Wave16ToFloatProvider(pcmStream);

                        WriteObject(floatProvider, false);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException($"Exception when opening wav file: {Path}", ex);
            }
        }
    }
}
