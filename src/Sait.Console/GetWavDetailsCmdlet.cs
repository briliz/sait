﻿using JetBrains.Annotations;

namespace Sait.Console
{
    using System;
    using System.IO;
    using System.Text;
    using NAudio.Wave;
    using System.Management.Automation;

    [Cmdlet(VerbsCommon.Get, "WavDetails")]
    public class GetWavDetailsCmdlet : Cmdlet
    {
        private static readonly ILogger Logger = new Log4NetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Parameter(Mandatory = true, HelpMessage = "Wav File Path")]
        public string Path { get; [UsedImplicitly] private set; }

        protected override void ProcessRecord()
        {
            Logger.LogDebug("ProcessRecord()");

            // Get path and check it is valid
            if (string.IsNullOrEmpty(Path))
            {
                WriteWarning(Logger.LogError("Empty wav file path. Operation aborted"));
                return;
            }

            if (!File.Exists(Path))
            {
                WriteWarning(Logger.LogError($"Cannot find wav file with path: {Path}"));
                return;
            }

            // Open file and extract the wav file format
            try
            {
                using (var reader = new WaveFileReader(Path))
                {
                    Logger.LogDebug($"Opened wav file: {Path}");
                    WriteObject(reader.WaveFormat, false);
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LogException($"Exception when opening wav file: {Path}", ex);
            }
        }
    }
}
