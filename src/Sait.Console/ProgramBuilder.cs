﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Container.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the Container type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.ComponentModel.Composition.Hosting;

namespace Sait.Console
{
    using System;
    using System.Collections.Generic;
    using System.Management.Automation.Runspaces;
    using System.ComponentModel.Composition;
    using System.Threading;
    using Autofac.Core;

    using Autofac;

    public class ProgramBuilder
    {
        private static readonly ILogger Logger = new Log4NetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [ImportMany(typeof(IModule))]
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private IModule[] _modules = null;

        private static readonly Dictionary<string, Type> Cmdlets = new Dictionary<string, Type>
                                                                       {
                                                                           { "Get-WavDetails",     typeof(GetWavDetailsCmdlet) },
                                                                           { "New-SampleProvider", typeof(NewSampleProviderCmdlet) }
                                                                       };

        public IContainer Build()
        {
            Logger.LogDebug("Build()");

            var builder = new ContainerBuilder();

            builder.RegisterType<CancellationTokenSource>().AsSelf().SingleInstance();
            builder.Register(
                c =>
                {
                    var tokenSource = c.Resolve<CancellationTokenSource>();
                    return tokenSource.Token;
                });

            Logger.LogDebug("Core Objects Registered");

            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new DirectoryCatalog("."));

            var mefContainer = new CompositionContainer(catalog);
            mefContainer.ComposeParts(this);

            foreach (var m in _modules)
            {
                builder.RegisterModule(m);
            }

            Logger.LogDebug("Autofac Modules Registered");

            // Add the Powershell Instances
            builder.Register(
                c =>
                    {
                        var iss = InitialSessionState.CreateDefault();
                        foreach (var key in Cmdlets.Keys)
                        {
                            SessionStateCmdletEntry otaCmdlet = new SessionStateCmdletEntry(key, Cmdlets[key], string.Empty);
                            iss.Commands.Add(otaCmdlet);
                        }

                        return iss;
                    });

            Logger.LogDebug("Powershell Cmdlets Registered");

            builder.RegisterType<SaitPsRawUserInterface>().AsSelf().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<SaitPsHostUserInterface>().AsSelf().SingleInstance();
            builder.RegisterType<SaitPsHost>().AsSelf().SingleInstance();
            builder.Register(
                c =>
                    {
                        var iss = c.Resolve<InitialSessionState>();
                        var host = c.Resolve<SaitPsHost>();
                        return RunspaceFactory.CreateRunspace(host, iss);
                    });
            builder.RegisterType<SaitPsListener>().AsSelf().AsImplementedInterfaces().SingleInstance();

            Logger.LogDebug("Powershell Objects Registered");

            return builder.Build();
        }
    }
}
