﻿using System;

namespace Sait.Console
{
    public class ExitCodeEventArgs : EventArgs
    {
        public ExitCodeEventArgs(int exitCode)
        {
            ExitCode = exitCode;
        }

        public int ExitCode { get; }
    }
}