﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Byte Right Solutions">
//   Copyright 2018 Byte Right Solutions. All rights reserved.
// </copyright>
// <summary>
//   Speech Analysis and Injection Tool
//   This application is used to load, play, record and stream audio
//   to/from files and other endpoints
//   Author: Brian Chapman
//   DateTime: 20180521 13:46
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Autofac;

namespace Sait.Console
{
    using System;

    /// <summary>
    /// The main entry point for the SAIT application
    /// </summary>
    public class Program
    {
        private static readonly ILogger Logger = new Log4NetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args">
        /// command line arguments passed to the application
        /// </param>
        public static void Main(string[] args)
        {
            Logger.LogDebug("Main(string[])");
            Console.Title = "Speech Analysis and Injection Tool";
            Console.ForegroundColor = ConsoleColor.Green;
            Console.BackgroundColor = ConsoleColor.Black;

            var builder = new ProgramBuilder();
            var container = builder.Build();

            var scope = container.BeginLifetimeScope();
            var listener = scope.Resolve<SaitPsListener>();

            listener.Run();
        }
    }
}
