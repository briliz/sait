﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Container.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the Container type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using log4net;

namespace Sait.Console
{
    public class Log4NetLogger : ILogger
    {
        private readonly string _tag;
        private readonly ILog _logger;

        public Log4NetLogger(Type type, string tag = "")
        {
            _tag = tag;
            _logger = LogManager.GetLogger(type);
        }

        public string LogDebug(string message)
        {
            if (_logger.IsDebugEnabled)
            {
                LogicalThreadContext.Properties["tag"] = _tag;
                _logger.Debug(message);
            }

            return message;
        }

        public string LogInfo(string message)
        {
            if (_logger.IsInfoEnabled)
            {
                LogicalThreadContext.Properties["tag"] = _tag;
                _logger.Info(message);
            }

            return message;
        }

        public string LogWarn(string message)
        {
            if (_logger.IsWarnEnabled)
            {
                LogicalThreadContext.Properties["tag"] = _tag;
                _logger.Warn(message);
            }

            return message;
        }

        public string LogError(string message)
        {
            if (_logger.IsErrorEnabled)
            {
                LogicalThreadContext.Properties["tag"] = _tag;
                _logger.Error(message);
            }

            return message;
        }

        public string LogFatal(string message, Exception ex)
        {
            if (_logger.IsFatalEnabled)
            {
                LogicalThreadContext.Properties["tag"] = _tag;
                _logger.Fatal(message, ex);
            }

            return message;
        }

        public string LogException(string message, Exception ex)
        {
            if (_logger.IsErrorEnabled)
            {
                LogicalThreadContext.Properties["tag"] = _tag;
                _logger.Error(message, ex);
            }

            return message;
        }
    }

    public interface ILogger
    {
        string LogDebug(string message);

        string LogInfo(string message);

        string LogWarn(string message);

        string LogError(string message);

        string LogException(string message, Exception ex);
        string LogFatal(string message, Exception ex);
    }
}