﻿using System;
using System.Collections.ObjectModel;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Text;

namespace Sait.Console
{
    /// <summary>
    /// Controls the console, taking commands entered, executing them and then outputting the result.
    /// </summary>
    public class SaitPsListener
    {
        private readonly SaitPsHost _host;

        /// <summary>
        /// Holds the run-space for this interpreter.
        /// </summary>
        private readonly Runspace _runSpace;

        /// <summary>
        /// Used to serialize access to instance data.
        /// </summary>
        private readonly object _instanceLock = new object();

        /// <summary>
        /// Holds a reference to the currently executing pipeline so it can be
        /// stopped by the control-C handler.
        /// </summary>
        private PowerShell _currentPowerShell;

        /// <summary>
        /// Initializes a new instance of the <see cref="SaitPsListener"/> class. 
        /// </summary>
        public SaitPsListener(SaitPsHost host, Runspace rs)
        {
            // Create the host and runspace instances for this interpreter. 
            // Note that this application does not support console files so 
            // only the default snapins will be available.
            _host = host;
            host.ShouldExit += HandleShouldExit;

            _runSpace = rs;
            _runSpace.Open();
        }

        /// <summary>
        /// Gets or sets a value indicating whether the host application
        /// should exit.
        /// </summary>
        public bool ShouldExit { get; set; }

        /// <summary>
        /// Gets or sets the exit code that the host application will use 
        /// when exiting.
        /// </summary>
        public int ExitCode { get; set; }

        /// <summary>
        /// Implements the basic listener loop. It sets up the ctrl-C handler, then
        /// reads a command from the user, executes it and repeats until the ShouldExit
        /// flag is set.
        /// </summary>
        public void Run()
        {
            // Set up the control-C handler.
            System.Console.CancelKeyPress += HandleControlC;
            System.Console.TreatControlCAsInput = false;

            // Read commands to execute until ShouldExit is set by
            // the user calling "exit".
            while (!ShouldExit)
            {
                _host.UI.Write(ConsoleColor.Cyan, ConsoleColor.Black, "\nSAIT> ");
                string cmd = System.Console.ReadLine();
                Execute(cmd);
            }

            // Exit with the desired exit code that was set by exit command.
            // This is set in the host by the MyHost.SetShouldExit() implementation.
            Environment.Exit(ExitCode);
        }

        /// <summary>
        /// A helper class that builds and executes a pipeline that writes to the
        /// default output path. Any exceptions that are thrown are just passed to
        /// the caller. Since all output goes to the default, this method()
        /// won't return anything.
        /// parameter name="cmd"; The script to run.
        /// parameter name="input";Any input arguments to pass to the script. If null
        /// then nothing is passed in.
        /// </summary>
        private void ExecuteHelper(string cmd, object input)
        {
            // Ignore empty command lines.
            if (string.IsNullOrEmpty(cmd))
            {
                return;
            }

            // Create the pipeline object and make it available
            // to the ctrl-C handle through the currentPowerShell instance
            // variable
            lock (_instanceLock)
            {
                _currentPowerShell = PowerShell.Create();
            }

            _currentPowerShell.Runspace = _runSpace;

            // Create a pipeline for this execution. Place the result in the 
            // currentPowerShell instance variable so that it is available 
            // to be stopped.
            try
            {
                var exePolicy = new StringBuilder("Set-ExecutionPolicy -Scope Process -ExecutionPolicy Unrestricted;");
                _currentPowerShell.AddCommand(exePolicy.ToString());
                _currentPowerShell.AddStatement().AddScript(cmd);

                // Now add the default outputter to the end of the pipe and indicate
                // that it should handle both output and errors from the previous
                // commands. This will result in the output being written using the PSHost
                // and PSHostUserInterface classes instead of returning objects to the hosting
                // application.
                _currentPowerShell.AddCommand("out-default");
                _currentPowerShell.Commands.Commands[0].MergeMyResults(PipelineResultTypes.Error, PipelineResultTypes.Output);

                // If there was any input specified, pass it in, otherwise just
                // execute the pipeline.
                if (input != null)
                {
                    _currentPowerShell.Invoke(new[] { input });
                }
                else
                {
                    _currentPowerShell.Invoke();
                }
            }
            finally
            {
                // Dispose of the pipeline line and set it to null, locked because 
                // currentPowerShell may be accessed by the ctrl-C handler.
                lock (_instanceLock)
                {
                    _currentPowerShell.Dispose();
                    _currentPowerShell = null;
                }
            }
        }

        /// <summary>
        /// An exception occurred that we want to display
        /// using the display formatter. To do this we run
        /// a second pipeline passing in the error record.
        /// The runtime will bind this to the $input variable
        /// which is why $input is being piped to out-string.
        /// We then call WriteErrorLine to make sure the error
        /// gets displayed in the correct error color.
        /// parameter name="e"; The exception to display.
        /// </summary>
        private void ReportException(Exception e)
        {
            if (e == null)
            {
                return;
            }

            IContainsErrorRecord icer = e as IContainsErrorRecord;
            ErrorRecord error = icer != null ? icer.ErrorRecord : new ErrorRecord(e, "Host.ReportException", ErrorCategory.NotSpecified, null);

            lock (_instanceLock)
            {
                _currentPowerShell = PowerShell.Create();
            }

            _currentPowerShell.Runspace = _runSpace;

            try
            {
                _currentPowerShell.AddScript("$input").AddCommand("out-string");

                // Do not merge errors, this function will swallow errors.
                PSDataCollection<object> inputCollection = new PSDataCollection<object> { error };
                inputCollection.Complete();
                Collection<PSObject> result = _currentPowerShell.Invoke(inputCollection);

                if (result.Count > 0)
                {
                    string str = result[0].BaseObject as string;
                    if (!string.IsNullOrEmpty(str))
                    {
                        // Remve \r\n that is added by Out-string.
                        _host.UI.WriteErrorLine(str.Substring(0, str.Length - 2));
                    }
                }
            }
            finally
            {
                // Dispose of the pipeline line and set it to null, locked because currentPowerShell
                // may be accessed by the ctrl-C handler.
                lock (_instanceLock)
                {
                    _currentPowerShell.Dispose();
                    _currentPowerShell = null;
                }
            }
        }

        /// <summary>
        /// Basic script execution routine - any runtime exceptions are
        /// caught and passed back into the engine to display.
        /// parameter name="cmd"; The parameter is not used.
        /// </summary>
        private void Execute(string cmd)
        {
            try
            {
                // Execute the command with no input.
                ExecuteHelper(cmd, null);
            }
            catch (RuntimeException rte)
            {
                ReportException(rte);
            }
        }

        /// <summary>
        /// Method used to handle control-C's from the user. It calls the
        /// pipeline Stop() method to stop execution. If any exceptions occur
        /// they are printed to the console but otherwise ignored.
        /// parameter name="sender"; See sender property of ConsoleCancelEventHandler documentation.
        /// parameter name="e"; See e property of ConsoleCancelEventHandler documentation.
        /// </summary>
        private void HandleControlC(object sender, ConsoleCancelEventArgs e)
        {
            try
            {
                lock (_instanceLock)
                {
                    if (_currentPowerShell != null && _currentPowerShell.InvocationStateInfo.State == PSInvocationState.Running)
                    {
                        _currentPowerShell.Stop();
                    }
                }

                e.Cancel = true;
            }
            catch (Exception exception)
            {
                _host.UI.WriteErrorLine(exception.ToString());
            }
        }

        private void HandleShouldExit(object sender, ExitCodeEventArgs e)
        {
            ShouldExit = true;
            ExitCode = e.ExitCode;
        }
    }
}
