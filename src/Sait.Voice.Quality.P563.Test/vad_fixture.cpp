#include <fstream>

#include "gtest/gtest.h"
#include "vad_fixture.h"

namespace SaitTest
{
	using namespace std;

	void VadFixture::SetUp()
	{
		// Load the audio file into a vector of signed 16 bit PCM samples

		std::ifstream audio_file("ae1f5a02.out", ios::in | ios::binary);
		if (audio_file.is_open())
		{
			// Stop eating new lines in binary mode!!!
			audio_file.unsetf(std::ios::skipws);

			int16_t sample_buffer[1024];
			while(!audio_file.eof())
			{
				audio_file.read(reinterpret_cast<char*>(&sample_buffer), sizeof(sample_buffer));

				for (auto i : sample_buffer)
				{
					input_signal.push_back(i);
				}
			}

			audio_file.close();

			std::vector<float> clean(input_signal.size() / VAD_FRAME_LEN);

			vad_signal.swap(clean);
		}
	}

	void VadFixture::TearDown()
	{
		std::ofstream pcm_file("ae1f5a02.vad", ios::binary | ios::trunc);

		if (pcm_file.is_open())
		{
			for (auto f : vad_signal)
			{
				const int16_t pcm_value = f > 0.0f ? 1: 0;
				for (int i = 0; i < VAD_FRAME_LEN; i++)
				{
					pcm_file << pcm_value;
				}
			}

			pcm_file.close();
		}
	}
}

