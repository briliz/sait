#include "gtest/gtest.h"
#include "vad_fixture.h"
#include "vad.h"

namespace SaitTest
{
	using namespace Sait;

	TEST_F(VadFixture, DetectsVoiceInAudio) 
	{
		std::vector<float> vad_signal(input_signal.size());
		float level_threshold = 0;
		float level_speech = 0;
		float level_noise = 0;
		float activity = 0;

		VoiceActivityDetector vad;

		const bool overall_vad_success = vad.detect_voice(
			input_signal, VAD_FRAME_LEN , vad_signal, level_threshold, level_speech, level_noise, activity);

		std::cout << "Threshold Level: " << level_threshold << std::endl;
		std::cout << "Speech Level: " << level_speech << std::endl;
		std::cout << "Noise Level: " << level_noise << std::endl;
		std::cout << "Activity: " << activity << std::endl;

		ASSERT_TRUE(overall_vad_success);
	}
}