#include <string>
#include <cppunit\TestCase.h>

class VadTests : public CppUnit::TestCase {
public:
	VadTests(std::string name) : CppUnit::TestCase(name) {}

	void runTest() override
	{
		CPPUNIT_ASSERT(Complex(10, 1) == Complex(10, 1));
		CPPUNIT_ASSERT(!(Complex(1, 1) == Complex(2, 2)));
	}
};
