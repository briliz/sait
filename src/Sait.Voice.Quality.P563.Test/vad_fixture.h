#pragma once
#include <vector>
#include <gtest/gtest.h>
#include "defines.h"

namespace SaitTest
{
	class VadFixture : public ::testing::Test
	{
	protected:
		std::vector<int16_t> input_signal;
		std::vector<float> vad_signal;

		void SetUp() override;

		void TearDown() override;
	};
}

