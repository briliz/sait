#include <cmath>
#include <vector>

#include "defines.h"
#include "vad.h"

namespace Sait
{
	std::vector<float> init_average_signal_power(const std::vector<int16_t>& samples, int window_size) noexcept
	{
		// Group the samples into windows with down_sample samples in each
		const auto num_windows = samples.size() / window_size;

		std::vector<float> signal_power(num_windows);

		// Calculate average power for each sample window
		for (auto w = 0U; w < num_windows; w++)
		{
			signal_power[w] = 0.0f;

			// For each sample within sample window
			for (auto i = 0; i < window_size; i++)
			{
				// Get the sample value
				const float g = samples[w * window_size + i];

				// Calculate sample power and add to cumulative total for window
				signal_power[w] += g * g;
			}

			// Average cumulative power for sample window
			signal_power[w] /= window_size;
		}

		return signal_power;
	}

	double init_zero_level_threshold(const std::vector<float>& vad) noexcept
	{
		const auto num_windows = vad.size();

		double zero_level_threshold = 0.0f;

		for (auto w = 0U; w < num_windows; w++)
		{
			zero_level_threshold += vad[w];
		}

		return zero_level_threshold /= num_windows;
	}

	double get_zero_level_min(const std::vector<float>& vad) noexcept
	{
		const auto num_windows = vad.size();

		double zero_level_min = 0.0f;

		for (auto w = 0U; w < num_windows; w++)
		{
			if (vad[w] > zero_level_min)
			{
				zero_level_min = vad[w];
			}
		}

		if (zero_level_min > 0.0f)
		{
			zero_level_min *= 1.0e-4f;
		}
		else
		{
			zero_level_min = 1.0f;
		}

		return zero_level_min;
	}

	double refine_zero_level_threshold(const std::vector<float>& vad, double zero_level_threshold) noexcept
	{
		const auto refinement_iterations = 12U;

		const auto num_windows = vad.size();

		for (auto i = 0U; i < refinement_iterations; i++)
		{
			double zero_level_noise = 0.0f;
			auto length = 0U;

			// find the cumulative noise power across entire signal
			for (auto w = 0U; w < num_windows; w++)
			{
				if (vad[w] <= zero_level_threshold)
				{
					zero_level_noise += vad[w];
					length++;
				}
			}

			double standard_deviation = 0.0f;

			if (length > 0L)
			{
				// Average the noise power
				zero_level_noise /= length;

				double noise_power = 0.0f;

				// Refine the average noise power standard deviation for each sample window
				for (auto w = 0U; w < num_windows; w++)
				{
					if (vad[w] <= zero_level_threshold)
					{
						const auto g = vad[w] - zero_level_noise;
						noise_power += g * g;
					}
				}

				standard_deviation = static_cast<double>(sqrt(noise_power / length));
			}

			// Modify zero level threshold by average noise power plus two times standard deviation
			zero_level_threshold = 1.001f * (zero_level_noise + 2.0f * standard_deviation);
		}

		return zero_level_threshold;
	}

	void get_zero_level_signal_and_noise(
		const std::vector<float>& vad, 
		double zero_level_threshold,
		double& zero_level_signal,
		double& zero_level_noise) noexcept
	{
		const auto num_windows = vad.size();
		
		auto length = 0U;
		zero_level_signal = 0.0f;
		zero_level_noise = 0.0f;

		for (auto w : vad)
		{
			if (w > zero_level_threshold)
			{
				zero_level_signal += w;
				length++;
			}
			else
			{
				zero_level_noise += w;
			}
		}

		if (length > 0U)
		{
			zero_level_signal /= length;
		}

		if (length < num_windows)
		{
			zero_level_noise /= num_windows - length;
		}
		else
		{
			zero_level_noise = 1.0f;
		}
	}

	VoiceActivityDetector::VoiceActivityDetector()
		= default;

	VoiceActivityDetector::~VoiceActivityDetector()
		= default;

	VoiceActivityDetector::VoiceActivityDetector(const VoiceActivityDetector& source) 
		= default;

	VoiceActivityDetector::VoiceActivityDetector(VoiceActivityDetector&&) noexcept
		= default;

	VoiceActivityDetector& VoiceActivityDetector::operator=(const VoiceActivityDetector&)
		= default;

	/*********************
	*
	*	FUNCTION: vad_process
	*
	*	DESCRIPTION:
	*		This function generates the vad profile based on an adaptative
	*		power threshold, using an iterative approach.
	*
	***********************/
	bool VoiceActivityDetector::detect_voice(
		const std::vector<int16_t>& samples,
		int window_size,
		std::vector<float>& vad_signal,
		float& level_threshold,
		float& level_speech,
		float& level_noise,
		float& activity)
	{
		const auto num_windows = samples.size() / window_size;

		// To proceed VAD needs a minimum number of windows
		if (num_windows < min_windows)
		{
			return false;
		}

		// Set the initial vad values to the average signal power across all samples int he window.
		// O(n)
		vad_signal = init_average_signal_power(samples, window_size);

		// Set the initial zero level power threshold value
		// O(w)
		auto zero_level_threshold = init_zero_level_threshold(vad_signal);

		// Determine the zero level power minimum
		// O(w)
		const auto zero_level_min = get_zero_level_min(vad_signal);

		// Set any low vad values to the minimum
		// O(w)
		for (auto w = 0U; w < num_windows; w++)
		{
			if (vad_signal[w] < zero_level_min)
			{
				vad_signal[w] = zero_level_min;
			}
		}

		// For a set number of iterations refine the noise level threshold
		// to the mean plus two standard deviations of the noise power
		// O(w)
		zero_level_threshold = refine_zero_level_threshold(vad_signal, zero_level_threshold);

		// From the VAD signal and the zero_level_threshold derive the signal zero level and the noise zero level
		// O(w)
		double zero_level_noise = 0.0f;
		double zero_level_signal = 0.0f;
		get_zero_level_signal_and_noise(vad_signal, zero_level_threshold, zero_level_signal, zero_level_noise);

		if (zero_level_signal == 0.0f)
		{
			zero_level_threshold = -1.0f;
		}

		// Reflect vad values that are less than zero level threshold
		// O(w)
		for (auto w = 0U; w < num_windows; w++)
		{
			if (vad_signal[w] <= zero_level_threshold)
			{
				vad_signal[w] = -vad_signal[w];
			}
		}

		vad_signal[0] = -zero_level_min;
		vad_signal[num_windows - 1] = -zero_level_min;

		// If the duration of signal is less than VAD_MIN_SPEECH_SECTION_LEN
		// then class it as noise and update values accordingly.
		INT32 start = 0L;
		INT32 finish;
		for (auto w = 1U; w < num_windows; w++)
		{
			if (vad_signal[w] > 0.0f && vad_signal[w - 1] <= 0.0f)
			{
				start = w;
			}

			if (vad_signal[w] <= 0.0f && vad_signal[w - 1] > 0.0f)
			{
				finish = w;
				if (finish - start <= VAD_MIN_SPEECH_SECTION_LEN)
				{
					for (auto i = start; i < finish; i++)
					{
						vad_signal[i] = -vad_signal[i];
					}
				}
			}
		}

		// If the signal level is 1000 times greater than noise level
		// and signal power is less then 3 x signal threshold then class
		// the signal as noise
		if (zero_level_signal >= zero_level_noise * 1000.0f)
		{
			for (auto w = 1U; w < num_windows; w++)
			{
				if (vad_signal[w] > 0.0f && vad_signal[w - 1] <= 0.0f)
				{
					start = w;
				}

				if (vad_signal[w] <= 0.0f && vad_signal[w - 1] > 0.0f)
				{
					finish = w;
					double g = 0.0f;
					for (auto i = start; i < finish; i++)
					{
						g += vad_signal[i];
					}

					if (g < 3.0f * zero_level_threshold * (finish - start))
					{
						for (auto i = start; i < finish; i++)
						{
							vad_signal[i] = -vad_signal[i];
						}
					}
				}
			}
		}

		// If noise between signal has small enough duration, then merge
		// the signal sections by making vad +ve and equal to min signal level
		start = 0L;
		finish = 0L;
		for (auto w = 1U; w < num_windows; w++)
		{
			if (vad_signal[w] > 0.0f && vad_signal[w - 1] <= 0.0f)
			{
				start = w;
				if (finish > 0L && start - finish <= VAD_JOIN_SPEECH_SECTION_LEN)
				{
					for (auto i = finish; i < start; i++)
					{
						vad_signal[i] = zero_level_min;
					}
				}
			}

			if (vad_signal[w] <= 0.0f && vad_signal[w - 1] > 0.0f)
			{
				finish = w;
			}
		}

		// If there are no zero level crossings then convert entire signal to +ve values
		if (start == 0L)
		{
			for (auto w = 0U; w < num_windows; w++)
			{
				vad_signal[w] = static_cast<FLOAT>(fabs(vad_signal[w]));
			}
			vad_signal[0] = -zero_level_min;
			vad_signal[num_windows - 1] = -zero_level_min;
		}

		// TODO: Work out why this change of values around zero crossings
		auto w = 2U;
		while (w < num_windows - 2)
		{
			if (vad_signal[w] > 0.0f && vad_signal[w - 2] <= 0.0f)
			{
				vad_signal[w - 2] = static_cast<FLOAT>(vad_signal[w] * 0.1);
				vad_signal[w - 1] = static_cast<FLOAT>(vad_signal[w] * 0.3);
				w++;
			}
			if (vad_signal[w] <= 0.0f && vad_signal[w - 1] > 0.0f)
			{
				vad_signal[w] = static_cast<FLOAT>(vad_signal[w - 1] * 0.3);
				vad_signal[w + 1] = static_cast<FLOAT>(vad_signal[w - 1] * 0.1);
				w += 3;
			}
			w++;
		}

		// Remove all VAD classed as noise
		for (w = 0; w < num_windows; w++)
		{
			if (vad_signal[w] < 0.0f)
			{
				vad_signal[w] = 0.0f;
			}
		}

		// Re-calculate zero_level_signal using original samples without the noise
		int length = 0;
		zero_level_signal = 0.0f;
		for (w = 0; w < num_windows; w++) 
		{
			if (vad_signal[w] > 0.0f) 
			{
				for (auto i = 0; i < window_size; i++)
				{
					const double g = samples[w * window_size + i];
					zero_level_signal += g * g;
				}

				length++;
			}
		}

		if (length > 0)
		{
			zero_level_signal /= length * window_size;
		}

		// Re-calculate zero_level_noise using original samples without the signal
		int length_noise = 0;
		zero_level_noise = 0.0f;
		start = 0;
		int is_speech = 0;
		for (w = 0; w < num_windows; w++) 
		{
			if (!is_speech && (vad_signal[w] > 0.0f || w == num_windows - 1))
			{
				finish = w;

				if (finish - start > 3 * VAD_MIN_SPEECH_SECTION_LEN) 
				{
					start += VAD_MIN_SPEECH_SECTION_LEN;
					finish -= VAD_MIN_SPEECH_SECTION_LEN;
					int k = (finish - start) / 10;
					if (k > VAD_JOIN_SPEECH_SECTION_LEN / 2)
					{
						k = VAD_JOIN_SPEECH_SECTION_LEN / 2;
					}
					start += k * 2;
					finish -= k;

					length_noise += finish - start;
					for (auto i = start * window_size; i < finish * window_size; i++)
					{
						const double g = samples[i];
						zero_level_noise += g * g;
					}
				}
			}

			if (is_speech && vad_signal[w] <= 0.0f)
			{
				start = w;
			}

			is_speech = vad_signal[w] > 0.0f;
		}

		// If no noise found, run through original samples and try to recalculate again
		if (length_noise == 0)
		{
			zero_level_noise = 0.0f;
			for (w = 0; w < num_windows; w++)
			{
				if (vad_signal[w] <= 0.0f)
				{
					for (auto i = 0; i < window_size; i++)
					{
						const double g = samples[w * window_size + i];
						zero_level_noise += g * g;
					}
					length_noise++;
				}
			}
		}

		if (length_noise > 0)
		{
			zero_level_noise /= length_noise * window_size;
		}

		activity = static_cast<float>(length) / static_cast<float>(num_windows);
		level_threshold = zero_level_threshold;
		level_speech = zero_level_signal;
		level_noise = zero_level_noise;

		return true;
	}
}