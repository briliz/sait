#pragma once
#include <vector>
#include "defines.h"

namespace Sait
{
	/**
	* \brief Class for detecting voice activity within an audio stream
	*/
	class DLLEXPORT VoiceActivityDetector
	{
	public:
		/**
		* \brief Minimum number of sample windows needed to perform voice activity detection
		*/
		static const int min_windows = 10;

		static const int num_iterations = 12;

		VoiceActivityDetector();

		virtual ~VoiceActivityDetector();

		VoiceActivityDetector(const VoiceActivityDetector&);

		VoiceActivityDetector(VoiceActivityDetector&&) noexcept;

		VoiceActivityDetector& operator=(const VoiceActivityDetector&);

		bool detect_voice(
			const std::vector<int16_t>& samples,
			int window_size,
			std::vector<float>& vad_signal,
			float& level_threshold,
			float& level_speech,
			float& level_noise,
			float& activity);
	};
}
