﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Container.cs" company="Byte Right Solutions">
//   Copyright 2018 Byte Right Solutions. All rights reserved.
// </copyright>
// <summary>
//   Speech Analysis and Injection Tool
//   This application is used to load, play, record and stream audio
//   to/from files and other endpoints
//   Author: Brian Chapman
//   DateTime: 20180521 13:46
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Sait.Container
{
    public class Container
    {
    }
}
