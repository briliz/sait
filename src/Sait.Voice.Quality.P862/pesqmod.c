/***********************************************************
*
*  PESQ C library implementation 
*
*  pesqmod.c      High-level perceptual model processing
*
*  PESQ-ITU ANSI C source code
*  Copyright (C) British Telecommunications plc, 2000.
*  Copyright (C) Royal KPN NV, 2000.
*
*  Additional modifications
*  Copyright (C) Psytechnics Limited, 2001-2006.
*  All rights reserved.
*
*  For more information
*  - visit our websites www.psytechnics.com or www.pesq.net
*
*  - write to us at
*      Psytechnics Limited, Fraser House
*      23 Museum Street, Ipswich IP1 1HN, United Kingdom
*
*  - e-mail us at  info@psytechnics.com
*
************************************************************
*
*  Functions contained:
*    fix_power_level
*    pesq_measure
*    input_filter
*    calc_VAD
*    id_searchwindows
*    id_utterances
*    utterance_split
*    utterance_locate
*    short_term_fft
*    freq_warping
*    total_audible
*    time_avg_audible_of
*    freq_resp_compensation
*    intensity_warping_of
*    pseudo_Lp
*    multiply_with_asymmetry_factor
*    pow_of
*    compute_delay
*    Lpq_weight
*    pesq_psychoacoustic_model
*
************************************************************
*
*  Version control
*   $Id: pesqmod.c,v 1.24 2006/03/14 11:01:45 intra+barrettp Exp $
*
*	31/3/2006		Release 2.2
*
*	30/10/2004		Release 2.1
*	  New ITU-T P.862.1 pesq to MOS mapping.
*	  PESQ file handling improvement.
*	  PESQ paramter memory assignment division.
*	  Internal comment C2.2.2
*
*	04/10/2002		Release 2.0
*     Minor updates to consolidate PESQ and PESQ Tools
*     Moved level and spectrum diagnostics to pesqdiag.c.
*     Included calls to new PESQ tools functions in pesqdiag.c.  
*     Bug-fix in utterance_split() to trap incorrect delay
*     estimates that could result in array bound violation.
*
*   14/11/2001		Release 1.4
*     Implemented API extensions for level and spectra,
*     in pesq_measure and new functions pesq_level_spectra
*     and calc_level_spectra.
*     Modified processing in pesq_psychoacoustic_model for
*     new model 0 to address issues with silent references;
*     old version 1.0-1.3 code still available as model -1.
*     Calculate new PESQ_LQ in pesq_psychoacoustic_model.
*     Corrected formula for mnb in pesq_psychoacoustic_model.
*
*   21/05/2001		Release 1.3
*     Improved commenting of pesq_psychoacoustic_model.
*     Tidied up, removing some redundant code in
*     pesq_psychoacoustic_model.
*
*     Implemented new API extensions in
*     pesq_psychoacoustic_model and made return data use
*     err_info->rtn_bands instead of assuming 42 bands.
*
*   07/03/2001		Release 1.2
*
***********************************************************/
#define _CRT_SECURE_NO_WARNINGS

#include <math.h>
#include <stdio.h>
#define PESQ_GLOBAL_VARIABLES
#include "pesq.h"
#include "pesqpar.h"
#include "dsp.h"
#include "pesqerr.h"



#ifdef PESQ_TOOLS
#include "pesqdiag.h"
#endif

/************************************************************
*   fix_power_level
*
*   Syntax        void fix_power_level (SIGNAL_INFO *info, 
*                                long maxNsamples)
*
*   Description
*   Level align the specified signal to TARGET_AVG_POWER
*
*   Inputs        (*info).Nsamples    length of speech in samples
*                (*info).data        pointer to speech signal
*                name                string identify signal (ref/deg)
*                maxNsamples            
*
*   Modifies    (*info).data
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL void fix_power_level (PESQ_t *inst, SIGNAL_INFO *info, long maxNsamples) 
{
    long Fs = inst->Fs;
    long   n = info-> Nsamples;
    long   i;
    float *align_filtered = (float *) safe_malloc ((n + DATAPADDING_MSECS  * (Fs / 1000)) * sizeof (float));    
    float  global_scale;
    float  power_above_300Hz;
    long Downsample = inst->Downsample;

    for (i = 0; i < n + DATAPADDING_MSECS  * (Fs / 1000); i++) {
        align_filtered [i] = info-> data [i];
    }
    apply_fft_filter (inst, align_filtered, info-> Nsamples, 26, align_filter_dB);

    power_above_300Hz = (float) pow_of (align_filtered, 
                                        SEARCHBUFFER * Downsample, 
                                        n - SEARCHBUFFER * Downsample + DATAPADDING_MSECS  * (Fs / 1000),
                                        maxNsamples - 2 * SEARCHBUFFER * Downsample + DATAPADDING_MSECS  * (Fs / 1000));

    info->level_align_power = power_above_300Hz;
    global_scale = (float) sqrt (TARGET_AVG_POWER / (1e-20f + power_above_300Hz)); 

    for (i = 0; i < n; i++) {
        info-> data [i] *= global_scale;    
    }

    safe_free (align_filtered);
}


/************************************************************
*   pesq_measure
*
*   Syntax        void pesq_measure (SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
*                ERROR_INFO * err_info, long * Error_Flag, char ** Error_Type)
*
*   Description
*   Top-level PESQ model implementation.
*    - reads in the speech data if Nsamples is <= 0 else assumes signals passed
*     - applies level alignment, filtering and computes time alignment
*     - runs the PESQ psychoacoustic model.
*
*   Inputs        ref_info        
*                deg_info
*                err_info
*                Error_Flag        error flag non-zero if error has occurred
*                Error_Type        pointer to string to contain error description
*
*   Modifies    ref_info, deg_info, err_info, Error_Flag, Error_Type
*
*   Returns        None
*
*   History
*   Updated        24/09/2002 - AR - Minor update to PESQ tools
*                               computations to support single
*                               source code base.
*                  22/02/2002 - AR - New PESQ tools functions
*                               New FrameDelay member of err_info.
*                  14/11/2001 - AR - New processing in release 1.4.
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL void pesq_measure (PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
    ERROR_INFO * err_info, long * Error_Flag, char ** Error_Type)
{
    long count;
    float * read_ptr;
    long Fs = inst->Fs;
    long Downsample = inst->Downsample;

    /* Initialisation */
    err_info-> FrameDelay = NULL;

    ref_info-> data = NULL;
    ref_info-> orig_data = NULL;
    ref_info-> VAD = NULL;
    ref_info-> logVAD = NULL;
    
    deg_info-> data = NULL;
    deg_info-> orig_data = NULL;
    deg_info-> VAD = NULL;
    deg_info-> logVAD = NULL;

    /* Read in the speech data.
       If LV_xxx_Nsamples is <= 0, LV_xxx_data is taken to point to the name
       of a file containing 16-bit "short" integers, from which data is read.
       Otherwise LV_xxx_Nsamples of data is copied from LV_xxx_data, with
       padding added at start and end as required by the model.
    */
    if ((*Error_Flag) == 0)
    {
        //printf ("Reading reference file %s...", ref_info-> path_name);

        /* Read ref data */
        if( err_info->LV_ref_Nsamples <= 0 )
        {
            /* Load data from file */
            if(err_info->LV_ref_data == NULL)
			{
				*Error_Flag = PESQ_ERR_FILE_OPEN_READ;
				*Error_Type = "Could not open reference source file";
				return;
			}

            wcscpy(ref_info->file_name, (wchar_t *)err_info->LV_ref_data);
            load_src_from_alaw_wav (inst, Error_Flag, Error_Type, ref_info);

#ifdef PESQ_TOOLS
            /* Make copy of data for use in pesq_level_spectra().
               For applications in which memory is short, this can be omitted;
               pesq_level_spectra() will simply skip this processing if
               orig_data is NULL. */

            if( ((err_info->LV_Params != NULL) || (err_info->LV_Level_Spectra != NULL))
                && ((*Error_Flag) == 0) ) {
                ref_info->orig_data = 
                    (float *) safe_malloc( (ref_info-> Nsamples - 2 * SEARCHBUFFER * Downsample) * sizeof(float) );
                if( ref_info->orig_data != NULL )
                    for( count = 0; count < (ref_info-> Nsamples - 2 * SEARCHBUFFER * Downsample); count++ )
                        ref_info->orig_data[count] = ref_info->data[count + SEARCHBUFFER * Downsample];
            }
#endif
        }
        else
        {
            /* Allocate space for data and VADs (which is otherwise done by load_src) */
            ref_info-> Nsamples = err_info->LV_ref_Nsamples + 2 * SEARCHBUFFER * Downsample;
            ref_info-> data =
                (float *) safe_malloc( (ref_info-> Nsamples + DATAPADDING_MSECS  * (Fs / 1000)) * sizeof(float) );
            ref_info-> VAD = (float *) safe_malloc( ref_info-> Nsamples * sizeof(float) / Downsample );
            ref_info-> logVAD = (float *) safe_malloc( ref_info-> Nsamples * sizeof(float) / Downsample );

            if( (ref_info-> data == NULL) || (ref_info-> VAD == NULL) || (ref_info-> logVAD == NULL) )
            {
                *Error_Flag = PESQ_ERR_OUT_OF_MEMORY;
                *Error_Type = "Failed to allocate memory for source file";
                safe_free( ref_info-> data ); safe_free( ref_info-> VAD ); safe_free( ref_info-> logVAD );
                return;
            }

            /* Make copy of data for processing in the model */
            read_ptr = ref_info-> data;
            for( count = SEARCHBUFFER*Downsample; count > 0; count-- )
                *(read_ptr++) = 0.0f;
            for( count = 0; count < err_info->LV_ref_Nsamples; count++ )
                read_ptr[count] = err_info->LV_ref_data[count];
            read_ptr += err_info->LV_ref_Nsamples;
            for( count = SEARCHBUFFER*Downsample + DATAPADDING_MSECS  * (Fs / 1000); count > 0; count-- )
                *(read_ptr++) = 0.0f;

#ifdef PESQ_TOOLS
            /* Original copy of the data will be used by pesq_level_spectra() */
            ref_info->orig_data = err_info->LV_ref_data;
#endif
        }
    }
    if ((*Error_Flag) == 0)
    {
        //printf ("Reading degraded file %s...", deg_info-> path_name);

        /* Read deg data */
        if( err_info->LV_deg_Nsamples <= 0 )
        {
            /* Load data from file */
            if(err_info->LV_deg_data == NULL)
			{
				*Error_Flag = PESQ_ERR_FILE_OPEN_READ;
				*Error_Type = "Could not open degraded source file";
				
				/*Free memory allocated for reference data*/
				safe_free( ref_info->data ); safe_free( ref_info->VAD ); safe_free( ref_info->logVAD );
#ifdef PESQ_TOOLS
				if( (ref_info->orig_data != err_info->LV_ref_data)
				&& (ref_info->orig_data != NULL) )
				safe_free ( ref_info->orig_data );
#endif
			return;
			}

			wcscpy(deg_info->file_name, (wchar_t *)err_info->LV_deg_data);
            load_src_from_alaw_wav (inst, Error_Flag, Error_Type, deg_info);

			/*Free memory allocated for reference data*/
			if((*Error_Flag) != 0)
			{
				safe_free( ref_info->data ); safe_free( ref_info->VAD ); safe_free( ref_info->logVAD );
#ifdef PESQ_TOOLS
				if( (ref_info->orig_data != err_info->LV_ref_data)
				&& (ref_info->orig_data != NULL) )
					safe_free ( ref_info->orig_data );
#endif
				return;
			}


#ifdef PESQ_TOOLS
            /* Make copy of data for use in pesq_level_spectra().
               For applications in which memory is short, this can be omitted;
               pesq_level_spectra() will simply skip this processing if
               orig_data is NULL. */

            if( ((err_info->LV_Params != NULL) || (err_info->LV_Level_Spectra != NULL))
                && ((*Error_Flag) == 0) ) {
                deg_info->orig_data = 
                    (float *) safe_malloc( (deg_info-> Nsamples - 2 * SEARCHBUFFER * Downsample) * sizeof(float) );
                if( deg_info->orig_data != NULL )
                    for( count = 0; count < (deg_info-> Nsamples - 2 * SEARCHBUFFER * Downsample); count++ )
                        deg_info->orig_data[count] = deg_info->data[count + SEARCHBUFFER * Downsample];
                else
				{
					/*Free all allocated memory*/
					safe_free( ref_info->data ); safe_free( ref_info->VAD ); safe_free( ref_info->logVAD );
					if( (ref_info->orig_data != err_info->LV_ref_data)
					&& (ref_info->orig_data != NULL) )
						safe_free ( ref_info->orig_data );
					if( (deg_info->orig_data != err_info->LV_deg_data)
		            && (deg_info->orig_data != NULL) )
						safe_free ( deg_info->orig_data );
					return;
				}
            }
#endif
        }
        else
        {
            /* Allocate space for data and VADs (which is otherwise done by load_src) */
            deg_info-> Nsamples = err_info->LV_deg_Nsamples + 2 * SEARCHBUFFER * Downsample;
            deg_info-> data =
                (float *) safe_malloc( (deg_info-> Nsamples + DATAPADDING_MSECS  * (Fs / 1000)) * sizeof(float) );
            deg_info-> VAD = (float *) safe_malloc( deg_info-> Nsamples * sizeof(float) / Downsample );
            deg_info-> logVAD = (float *) safe_malloc( deg_info-> Nsamples * sizeof(float) / Downsample );

            if( (deg_info-> data == NULL) || (deg_info-> VAD == NULL) || (deg_info-> logVAD == NULL) )
            {
                *Error_Flag = PESQ_ERR_OUT_OF_MEMORY;
                *Error_Type = "Failed to allocate memory for source file";
    
				/*Free all allocated memory*/
				safe_free(ref_info->data ); safe_free(ref_info->VAD ); safe_free(ref_info->logVAD );
				safe_free( deg_info-> data );safe_free( deg_info-> VAD );safe_free( deg_info-> logVAD );				 
#ifdef PESQ_TOOLS
				if( (ref_info->orig_data != err_info->LV_ref_data)
				&& (ref_info->orig_data != NULL) )
					safe_free (ref_info->orig_data);
#endif
	            return;
            }
			/* Make copy of data for processing in the model */
            read_ptr = deg_info-> data;
            for( count = SEARCHBUFFER*Downsample; count > 0; count-- )
                *(read_ptr++) = 0.0f;
            for( count = 0; count < err_info->LV_deg_Nsamples; count++ )
                read_ptr[count] = err_info->LV_deg_data[count];
            read_ptr += err_info->LV_deg_Nsamples;
            for( count = SEARCHBUFFER*Downsample + DATAPADDING_MSECS  * (Fs / 1000); count > 0; count-- )
                *(read_ptr++) = 0.0f;

#ifdef PESQ_TOOLS
            /* Original copy of the data will be used by pesq_level_spectra() */
            deg_info->orig_data = err_info->LV_deg_data;
#endif
        }
    }

    pesq_measure_core (inst, ref_info, deg_info, err_info, Error_Flag, Error_Type);

    return;
}

void pesq_measure_core (PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
    ERROR_INFO * err_info, long * Error_Flag, char ** Error_Type)
{
    float * ftmp = NULL;
    long Downsample = inst->Downsample;
    long Fs = inst->Fs;

    if ((((ref_info-> Nsamples - 2 * SEARCHBUFFER * Downsample) < (Fs / 4)) ||
         ((deg_info-> Nsamples - 2 * SEARCHBUFFER * Downsample) < (Fs / 4))) &&
        ((*Error_Flag) == 0))
    {
        (*Error_Flag) = PESQ_ERR_INSUFFICIENT_DATA;
        (*Error_Type) = "Reference or Degraded below 1/4 second - processing stopped ";

		/*Free all allocated memory*/
		safe_free( ref_info->data); 
		safe_free( ref_info->VAD); 
		safe_free( ref_info->logVAD);
		safe_free( deg_info-> data );
		safe_free( deg_info-> VAD );
		safe_free( deg_info-> logVAD );				 
#ifdef PESQ_TOOLS
		if( (ref_info->orig_data != err_info->LV_ref_data)
			&& (ref_info->orig_data != NULL) )		
				safe_free ( ref_info->orig_data);
		if( (deg_info->orig_data != err_info->LV_deg_data)
            && (deg_info->orig_data != NULL) )
				safe_free (deg_info->orig_data);		
#endif
		return;
	}

    if ((*Error_Flag) == 0)
    {
        /* Initialise frame delay array */
        err_info->Nframes =
            (ref_info-> Nsamples - 2 * SEARCHBUFFER * Downsample) /
            ((16*Fs)/1000) - 1;
        err_info->FrameDelay = (long *) safe_malloc( err_info->Nframes * sizeof(long) );
        if( err_info->FrameDelay == NULL )
        {
            *Error_Flag = PESQ_ERR_OUT_OF_MEMORY;
            *Error_Type = "Failed to allocate memory for frame delay buffer";

			/*Free allocated memory*/
			safe_free( ref_info->data);
			safe_free( ref_info->VAD); 
			safe_free( ref_info->logVAD);
			safe_free( deg_info-> data );
			safe_free( deg_info-> VAD );
			safe_free( deg_info-> logVAD );
			safe_free( err_info->FrameDelay);				 
#ifdef PESQ_TOOLS
			if( (ref_info->orig_data != err_info->LV_ref_data)
			&& (ref_info->orig_data != NULL) )	
				safe_free ( ref_info->orig_data);
			if( (deg_info->orig_data != err_info->LV_deg_data)
            && (deg_info->orig_data != NULL) )
				safe_free ( deg_info->orig_data);
#endif			
            return;
		}
    }
    if ((*Error_Flag) == 0)
    {
        int frame;
        /* Initialise the delay to ref_Nsamples as a flag to show when
           no delay value exists for a frame. */
        for( frame = 0; frame < err_info->Nframes; frame++ )
            err_info->FrameDelay[frame] = ref_info->Nsamples;

        /* Allocate other data */
        alloc_other (inst, ref_info, deg_info, Error_Flag, Error_Type, &ftmp);
		
		if((*Error_Flag) != 0)
		{
			/*Free allocated memory*/
			safe_free( ref_info->data );
			safe_free( ref_info->VAD ); 
			safe_free( ref_info->logVAD );
			safe_free( deg_info-> data );
			safe_free( deg_info-> VAD );
			safe_free( deg_info-> logVAD );		
			safe_free ( err_info-> FrameDelay );
			safe_free ( ftmp );
#ifdef PESQ_TOOLS
			if( (ref_info->orig_data != err_info->LV_ref_data)
			&& (ref_info->orig_data != NULL) )
				safe_free (ref_info->orig_data);
			if( (deg_info->orig_data != err_info->LV_deg_data)
			&& (deg_info->orig_data != NULL) )
				safe_free (deg_info->orig_data);
#endif		
			return;
		}

    }

    if ((*Error_Flag) == 0)
    {   
        int     maxNsamples = max (ref_info-> Nsamples, deg_info-> Nsamples);
        float * model_ref; 
        float * model_deg; 
        long    i;

        /* Level normalization */
        fix_power_level (inst, ref_info, maxNsamples);
        if( ref_info->level_align_power < 1e-20f )
        {
            *Error_Flag = PESQ_ERR_INSUFFICIENT_DATA;
            *Error_Type = "Reference signal contains no energy";

			/*Free allocated memory*/
			safe_free( ref_info->data);
			safe_free( ref_info->VAD); 
			safe_free( ref_info->logVAD);
			safe_free( deg_info->data );
			safe_free( deg_info->VAD );
			safe_free( deg_info->logVAD );				 
			safe_free( err_info->FrameDelay );
			safe_free( ftmp );
			FFTFree(inst->pfft);
#ifdef PESQ_TOOLS
			if( (ref_info->orig_data != err_info->LV_ref_data)
			&& (ref_info->orig_data != NULL) )	
				safe_free ( ref_info->orig_data );
			if( (deg_info->orig_data != err_info->LV_deg_data)
			&& (deg_info->orig_data != NULL) )
				safe_free ( deg_info->orig_data );
#endif			
            return;
        }
        fix_power_level (inst, deg_info, maxNsamples);

        /* IRS/Wideband receive filtering of the signals for
           application to the whole model */

        /* Select appropriate wideband filter */
        if( Fs == 16000 ) {
            inst->WB_InIIR_Nsos = WB_InIIR_Nsos_16k;
            inst->WB_InIIR_Hsos = WB_InIIR_Hsos_16k;
            inst->HATS_InIIR_Nsos = HATS_InIIR_Nsos_16k;
            inst->HATS_InIIR_Hsos = HATS_InIIR_Hsos_16k;
        } else {
            inst->WB_InIIR_Nsos = WB_InIIR_Nsos_8k;
            inst->WB_InIIR_Hsos = WB_InIIR_Hsos_8k;
            inst->HATS_InIIR_Nsos = HATS_InIIR_Nsos_8k;
            inst->HATS_InIIR_Hsos = HATS_InIIR_Hsos_8k;
        }

        /* Filter reference */
        if( (err_info->model == -1) || (err_info->model == 0) || 
            (err_info->model == 1) ) {
            /* P.862 PESQ input filter for IRS telephone or HATS ear applications */
            apply_fft_filter (inst, ref_info-> data, ref_info-> Nsamples, 26, standard_IRS_filter_dB);
        } else {
            /* Wideband filter for headphone listening */
            for( i = 0; i < 16; i++ ) {
                ref_info->data[SEARCHBUFFER * Downsample + i - 1]
                    *= (float)i / 16.0f;
                ref_info->data[ref_info->Nsamples - SEARCHBUFFER * Downsample - i]
                    *= (float)i / 16.0f;
            }
            IIRFilt( inst->WB_InIIR_Hsos, inst->WB_InIIR_Nsos, NULL,
                 ref_info->data + SEARCHBUFFER * Downsample,
                 ref_info->Nsamples - 2 * SEARCHBUFFER * Downsample, NULL );
        }
        /* Filter degraded */
        if( (err_info->model == -1) || (err_info->model == 0) ) {
            /* P.862 PESQ input filter for IRS telephone */
            apply_fft_filter (inst, deg_info-> data, deg_info-> Nsamples, 26, standard_IRS_filter_dB);
        } else {
            /* Wideband filter for headphone listening or for HATS ear applications */
            for( i = 0; i < 16; i++ ) {
                deg_info->data[SEARCHBUFFER * Downsample + i - 1]
                    *= (float)i / 16.0f;
                deg_info->data[deg_info->Nsamples - SEARCHBUFFER * Downsample - i]
                    *= (float)i / 16.0f;
            }
            if( err_info->model == 1 ) {
                IIRFilt( inst->HATS_InIIR_Hsos, inst->HATS_InIIR_Nsos, NULL,
                     deg_info->data + SEARCHBUFFER * Downsample,
                     deg_info->Nsamples - 2 * SEARCHBUFFER * Downsample, NULL );
            } else {
            IIRFilt( inst->WB_InIIR_Hsos, inst->WB_InIIR_Nsos, NULL,
                     deg_info->data + SEARCHBUFFER * Downsample,
                     deg_info->Nsamples - 2 * SEARCHBUFFER * Downsample, NULL );
            }
        }

        /* Make an additional copy of the signals, then apply a further filter
           for use in time alignment */
        model_ref = (float *) safe_malloc ((ref_info-> Nsamples + DATAPADDING_MSECS  * (Fs / 1000)) * sizeof (float));
		model_deg = (float *) safe_malloc ((deg_info-> Nsamples + DATAPADDING_MSECS  * (Fs / 1000)) * sizeof (float));
		if( (model_ref == NULL)||(model_deg == NULL) )
        {
            *Error_Flag = PESQ_ERR_OUT_OF_MEMORY;
            *Error_Type = "Failed to allocate memory for ref and deg signal copy";

			/*Free allocated memory*/
			safe_free( ref_info->data );
			safe_free( ref_info->VAD ); 
			safe_free( ref_info->logVAD );
			safe_free( deg_info-> data );
			safe_free( deg_info-> VAD );
			safe_free( deg_info-> logVAD );	
			safe_free ( err_info-> FrameDelay );			
			safe_free( ftmp );
			safe_free ( model_ref );
			safe_free ( model_deg );
            FFTFree(inst->pfft);
#ifdef PESQ_TOOLS
			if( (ref_info->orig_data != err_info->LV_ref_data)
            && (ref_info->orig_data != NULL) )
				safe_free (ref_info->orig_data);;
			if( (deg_info->orig_data != err_info->LV_deg_data)
            && (deg_info->orig_data != NULL) )
				safe_free (deg_info->orig_data);
#endif			
            return;
        }
        
        for (i = 0; i < ref_info-> Nsamples + DATAPADDING_MSECS  * (Fs / 1000); i++) {
            model_ref [i] = ref_info-> data [i];
        }
        for (i = 0; i < deg_info-> Nsamples + DATAPADDING_MSECS  * (Fs / 1000); i++) {
            model_deg [i] = deg_info-> data [i];
        }
        input_filter(inst, ref_info, deg_info );

        /* Time alignment */

        /* Compute VAD and envelopes */
        calc_VAD (inst, ref_info);
        calc_VAD (inst, deg_info);
        
        /* Apply crude time alignment */
        crude_align (inst, ref_info, deg_info, err_info, WHOLE_SIGNAL, ftmp);

        /* Fine time alignment of utterances and utterance splitting */
        utterance_locate (inst, ref_info, deg_info, err_info, ftmp);

        /* Restore the signals to the state after the first IRS filter for use in
           the rest of the model */
        for (i = 0; i < ref_info-> Nsamples + DATAPADDING_MSECS  * (Fs / 1000); i++) {
            ref_info-> data [i] = model_ref [i];
        }
        for (i = 0; i < deg_info-> Nsamples + DATAPADDING_MSECS  * (Fs / 1000); i++) {
            deg_info-> data [i] = model_deg [i];
        }
        safe_free (model_ref);
        safe_free (model_deg); 

        /* Pad out the shorter of the reference and degraded signals with
           zeros to make them exactly the same length. */
        if ((*Error_Flag) == 0) {
            if (ref_info-> Nsamples < deg_info-> Nsamples) {
                float *new_ref = (float *) safe_malloc((deg_info-> Nsamples + DATAPADDING_MSECS  * (Fs / 1000)) * sizeof(float));
		        long  i;

				if( new_ref == NULL )
				{
					*Error_Flag = PESQ_ERR_OUT_OF_MEMORY;
					*Error_Type = "Failed to allocate memory for ref data padding";

					/*Free allocated memory*/
					safe_free( ref_info->data );
					safe_free( ref_info->VAD ); 
					safe_free( ref_info->logVAD );
					safe_free( deg_info-> data );
					safe_free( deg_info-> VAD );
					safe_free( deg_info-> logVAD );	
					safe_free (err_info-> FrameDelay );		
					safe_free(ftmp);
					FFTFree(inst->pfft);
					safe_free (new_ref);
#ifdef PESQ_TOOLS
					if( (ref_info->orig_data != err_info->LV_ref_data)
		            && (ref_info->orig_data != NULL) )
						safe_free (ref_info->orig_data);;
					if( (deg_info->orig_data != err_info->LV_deg_data)
		            && (deg_info->orig_data != NULL) )
						safe_free (deg_info->orig_data);
#endif			
					return;
				}

                for (i = 0; i < ref_info-> Nsamples + DATAPADDING_MSECS  * (Fs / 1000); i++) {
                    new_ref [i] = ref_info-> data [i];
                }
                for (i = ref_info-> Nsamples + DATAPADDING_MSECS  * (Fs / 1000); 
                     i < deg_info-> Nsamples + DATAPADDING_MSECS  * (Fs / 1000); i++) {
                    new_ref [i] = 0.0f;
                }
                safe_free (ref_info-> data);
                ref_info-> data = new_ref;
                new_ref = NULL;
            } else {
                if (ref_info-> Nsamples > deg_info-> Nsamples) {
                    float *new_deg = (float *) safe_malloc((ref_info-> Nsamples + DATAPADDING_MSECS  * (Fs / 1000)) * sizeof(float));
                    long  i;

					if( new_deg == NULL )
					{
						*Error_Flag = PESQ_ERR_OUT_OF_MEMORY;
						*Error_Type = "Failed to allocate memory for deg data padding";

						/*Free allocated memory*/
						safe_free( ref_info->data);
						safe_free( ref_info->VAD); 
						safe_free( ref_info->logVAD);
						safe_free( deg_info-> data );
						safe_free( deg_info-> VAD );
						safe_free( deg_info-> logVAD );	
						safe_free (err_info-> FrameDelay);		
						safe_free(ftmp);
						FFTFree(inst->pfft);
						safe_free(new_deg);
#ifdef PESQ_TOOLS
						if( (ref_info->orig_data != err_info->LV_ref_data)
			            && (ref_info->orig_data != NULL) )
							safe_free (ref_info->orig_data);;
						if( (deg_info->orig_data != err_info->LV_deg_data)
			            && (deg_info->orig_data != NULL) )
							safe_free (deg_info->orig_data);
#endif			
						return;
					}
                    
					for (i = 0; i < deg_info-> Nsamples + DATAPADDING_MSECS  * (Fs / 1000); i++) {
                        new_deg [i] = deg_info-> data [i];
                    }
                    for (i = deg_info-> Nsamples + DATAPADDING_MSECS  * (Fs / 1000); 
                         i < ref_info-> Nsamples + DATAPADDING_MSECS  * (Fs / 1000); i++) {
                        new_deg [i] = 0.0f;
                    }
                    safe_free (deg_info-> data);
                    deg_info-> data = new_deg;
                    new_deg = NULL;
                }
            }
        }        

        /* Acoustic model processing */
        pesq_psychoacoustic_model (inst, ref_info, deg_info, err_info);


#ifdef PESQ_TOOLS
        if( err_info->tools == 1 ) {
            /* Level and spectrum processing */
            pesq_level_spectra (ref_info, deg_info, err_info);

            /* Other PESQ tools processing */
            pesq_delay_stats( err_info );
            pesq_clip_stats( ref_info, deg_info, err_info );
        }
#endif

        /* Processing complete - free memory */
        safe_free (err_info-> FrameDelay);
        safe_free (ref_info-> data);
        safe_free (ref_info-> VAD);
        safe_free (ref_info-> logVAD);
        safe_free (deg_info-> data);
        safe_free (deg_info-> VAD);
        safe_free (deg_info-> logVAD);
        if( (ref_info->orig_data != err_info->LV_ref_data)
            && (ref_info->orig_data != NULL) )
            safe_free (ref_info->orig_data);
        if( (deg_info->orig_data != err_info->LV_deg_data)
            && (deg_info->orig_data != NULL) )
            safe_free (deg_info->orig_data);
        safe_free (ftmp);
    }

    return;
}

/************************************************************
*   input_filter
*
*   Syntax        void input_filter( SIGNAL_INFO * ref_info,
*                    SIGNAL_INFO * deg_info )
*
*   Description
*   Applies internal telephony input filters to speech data, which is
*    modified in situ.
*
*   Inputs        (*ref_info).data        reference signal
*                (*ref_info).Nsamples    length of reference signal in samples
*                (*deg_info).data        degraded signal
*                (*deg_info).Nsamples    length of degraded signal in samples
*
*   Modifies    (*ref_info).data, (*deg_info).data
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL void input_filter(PESQ_t *inst, 
    SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info )
{
    DC_block(inst, (*ref_info).data, (*ref_info).Nsamples );
    DC_block(inst, (*deg_info).data, (*deg_info).Nsamples );

    apply_iir_filter(inst, (*ref_info).data, (*ref_info).Nsamples );
    apply_iir_filter(inst, (*deg_info).data, (*deg_info).Nsamples );
}

/************************************************************
*   calc_VAD
*
*   Syntax        void calc_VAD( SIGNAL_INFO * sinfo )
*
*   Description
*   Calculates voice activity detection on speech data and places
*    decision in (*sinfo).VAD and the log of the VAD over the threshold
*    in (*sinfo).logVAD. The VAD array contains 0.0f if a frame is thought to
*    contain no speech and a positive power measure if speech is present.
*
*   Inputs        (*sinfo).Nsamples    length of speech in samples
*                (*sinfo).data        pointer to speech signal
*
*   Modifies    (*sinfo).VAD, (*sinfo).logVAD
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL void calc_VAD(PESQ_t *inst, SIGNAL_INFO * sinfo )
{
    apply_VAD(inst, sinfo, sinfo-> data, sinfo-> VAD, sinfo-> logVAD );
}


/************************************************************
*   id_searchwindows
*
*   Syntax        int id_searchwindows( SIGNAL_INFO * ref_info, 
*                    SIGNAL_INFO * deg_info, ERROR_INFO * err_info )
*
*   Description
*   Operates on the reference VAD to identify search windows for the finer time
*    alignment of each utterance.  The funciton fills two arrays in the err_info
*    with the beginning and ends of each utterance search window.
*
*   Inputs        (*ref_info).Nsamples            length of refernce signal in samples
*                (*ref_info).VAD                    VAD for reference signal
*                (*deg_info).Nsamples            length of degraded signal in samples
*                (*err_info).Crude_DelayEst        crude alignment offset estimate
*
*   Modifies    (*err_info).UttSearch_Start, (*err_info).UttSearch_End, (*err_info).Nutterances
*
*   Returns        Number of utterances identified
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL int id_searchwindows(PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
    ERROR_INFO * err_info )
{
    long  Utt_num = 0;
    long  count, VAD_length;
    long  this_start = 0;
    int   speech_flag = 0;
    float VAD_value;
    long  del_deg_start;
    long  del_deg_end;
    long Downsample = inst->Downsample;

    VAD_length = ref_info-> Nsamples / Downsample;

    del_deg_start = MINUTTLENGTH - err_info-> Crude_DelayEst / Downsample;
    del_deg_end =
        ((*deg_info).Nsamples - err_info-> Crude_DelayEst) / Downsample -
        MINUTTLENGTH;

    for (count = 0; count < VAD_length; count++)
    {
        VAD_value = ref_info-> VAD [count];

        if( (VAD_value > 0.0f) && (speech_flag == 0) ) 
        {
            speech_flag = 1;
            this_start = count;
            if( Utt_num >= MAXNUTTERANCES )
                Utt_num = MAXNUTTERANCES - 1;
            else
            {
                (*err_info).UttSearch_Start[Utt_num] = count - SEARCHBUFFER;
                if( (*err_info).UttSearch_Start[Utt_num] < 0 )
                    (*err_info).UttSearch_Start[Utt_num] = 0;
            }
        }

        if( ((VAD_value == 0.0f) || (count == (VAD_length-1))) &&
            (speech_flag == 1) ) 
        {
            speech_flag = 0;
            err_info-> UttSearch_End [Utt_num] = count + SEARCHBUFFER;
            if( err_info-> UttSearch_End [Utt_num] > VAD_length - 1 )
                err_info-> UttSearch_End [Utt_num] = VAD_length -1;

            if( ((count - this_start) >= MINUTTLENGTH) &&
                (this_start < del_deg_end) &&
                (count > del_deg_start) )
                Utt_num++;            
        }
    }

    err_info-> Nutterances = Utt_num;
    return Utt_num;
} 


/************************************************************
*   id_utterances
*
*   Syntax        void id_utterances( SIGNAL_INFO * ref_info, 
*                    SIGNAL_INFO * deg_info, ERROR_INFO * err_info )
*
*   Description
*   Once the delay between the reference and degraded files has been identified for
*    each utterance, id_utterances assigns start and end markers to ensure that:
*        - utterances do not overlap
*        - as much of the data is processed as possible
*        - later functions will not process beyond the ends of the signal
*
*   Inputs        (*ref_info).Nsamples            length of refernce signal in samples
*                (*ref_info).VAD                    VAD for reference signal
*                (*deg_info).Nsamples            length of degraded signal in samples
*                (*err_info).Crude_DelayEst        crude alignment offset estimate
*                (*err_info).Nutterances            number of utterances in reference
*                (*err_info).Utt_Delay[x]        offset between utterance x in reference and degraded
*
*   Modifies    (*err_info).Utt_Start[], (*err_info).Utt_End[], (*err_info).Largest_uttsize
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL void id_utterances(PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
    ERROR_INFO * err_info )
{
    long  Utt_num = 0;
    long  Largest_uttsize = 0;
    long  count, VAD_length;
    int   speech_flag = 0;
    float VAD_value;
    long  this_start = 0;
    long  last_end;
    long  del_deg_start;
    long  del_deg_end;
    long Downsample = inst->Downsample;

    VAD_length = ref_info-> Nsamples / Downsample;

    del_deg_start = MINUTTLENGTH - err_info-> Crude_DelayEst / Downsample;
    del_deg_end =
        ((*deg_info).Nsamples - err_info-> Crude_DelayEst) / Downsample -
        MINUTTLENGTH;

    for (count = 0; count < VAD_length ; count++)
    {
        VAD_value = ref_info-> VAD [count];
        if( (VAD_value > 0.0f) && (speech_flag == 0) ) 
        {
            speech_flag = 1;
            this_start = count;
            if( Utt_num >= MAXNUTTERANCES )
                Utt_num = MAXNUTTERANCES - 1;
            else
                (*err_info).Utt_Start[Utt_num] = count;
        }

        if( ((VAD_value == 0.0f) || (count == (VAD_length-1))) &&
            (speech_flag == 1) ) 
        {
            speech_flag = 0;
            err_info-> Utt_End [Utt_num] = count;

            if( ((count - this_start) >= MINUTTLENGTH) &&
                (this_start < del_deg_end) &&
                (count > del_deg_start) )
                Utt_num++;            
        }
    }

    err_info-> Utt_Start [0] = SEARCHBUFFER;
    err_info-> Utt_End [err_info-> Nutterances-1] = (VAD_length - SEARCHBUFFER);
    
    for (Utt_num = 1; Utt_num < err_info-> Nutterances; Utt_num++ )
    {
        this_start = err_info-> Utt_Start [Utt_num];
        last_end = err_info-> Utt_End [Utt_num - 1];
        count = (this_start + last_end) / 2;
        err_info-> Utt_Start [Utt_num] = count;
        err_info-> Utt_End [Utt_num - 1] = count;
    }

    this_start = (err_info-> Utt_Start [0] * Downsample) + err_info-> Utt_Delay [0];
    if( this_start < (SEARCHBUFFER * Downsample) )
    {
        count = SEARCHBUFFER +
                (Downsample - 1 - err_info-> Utt_Delay [0]) / Downsample;
        err_info-> Utt_Start [0] = count;
    }
    last_end = (err_info-> Utt_End [err_info-> Nutterances-1] * Downsample) +
               err_info-> Utt_Delay [err_info-> Nutterances-1];
    if( last_end > ((*deg_info).Nsamples - SEARCHBUFFER * Downsample) )
    {
        count = ( (*deg_info).Nsamples -
                  err_info-> Utt_Delay [err_info-> Nutterances-1] ) / Downsample -
                SEARCHBUFFER;
        err_info-> Utt_End [err_info-> Nutterances-1] = count;
    }

    for (Utt_num = 1; Utt_num < err_info-> Nutterances; Utt_num++ )
    {
        this_start =
            (err_info-> Utt_Start [Utt_num] * Downsample) +
            err_info-> Utt_Delay [Utt_num];
        last_end =
            (err_info-> Utt_End [Utt_num - 1] * Downsample) +
            err_info-> Utt_Delay [Utt_num - 1];
        if( this_start < last_end )
        {
            count = (this_start + last_end) / 2;
            this_start =
                (Downsample - 1 + count - err_info-> Utt_Delay [Utt_num]) / Downsample;
            last_end =
               (count - err_info-> Utt_Delay [Utt_num - 1]) / Downsample;
            err_info-> Utt_Start [Utt_num] = this_start;
            err_info-> Utt_End [Utt_num - 1] = last_end;
        }
    }

    for (Utt_num = 0; Utt_num < err_info-> Nutterances; Utt_num++ )
        if( (err_info-> Utt_End [Utt_num] - err_info-> Utt_Start [Utt_num])
             > Largest_uttsize )
            Largest_uttsize = 
                err_info-> Utt_End [Utt_num] - err_info-> Utt_Start [Utt_num];

    err_info-> Largest_uttsize = Largest_uttsize;
}


/************************************************************
*   utterance_split
*
*   Syntax        void utterance_split( SIGNAL_INFO * ref_info, 
*                        SIGNAL_INFO * deg_info, ERROR_INFO * err_info, float * ftmp )
*
*   Description
*   This function identifies splits in utterances that are required to take account
*    of delay variations during speech.
*
*   Inputs        (*ref_info).data                reference speech signal
*                (*ref_info).Nsamples            length of refernce signal in samples
*                (*ref_info).VAD                    VAD for reference signal
*                (*ref_info).logVAD                log of VAD for reference signal
*                (*deg_info).data                degraded speech signal    
*                (*deg_info).Nsamples            length of degraded signal in samples
*                (*deg_info).VAD                    VAD for degraded signal
*                (*deg_info).logVAD                log of VAD for degraded signal
*                (*deg_info).Nsamples            length of degraded signal in samples
*                (*err_info).Utt_DelayEst[]        array of utterance delay estimates
*                (*err_info).Utt_Delay[]            array of utterance delays
*                (*err_info).Utt_DelayConf[]        array of utterance delay confidences
*                (*err_info).Utt_Start[]            array of utterance start locations
*                (*err_info).Utt_End[]            array of utterance end locations
*
*   Modifies    (*err_info).UttSearch_Start[], (*err_info).UttSearch_End[],
*                (*err_info).Nutterances, (*err_info).Utt_Start[], (*err_info).Utt_End[],
*                (*err_info).Largest_uttsize, ftmp, (*err_info).Crude_DelayEst,
*                (*err_info).Utt_DelayEst[], (*err_info).Utt_Delay[]
*                (*err_info).Utt_DelayConf[], (*err_info).Utt_Level[]
*                
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL void utterance_split(PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
    ERROR_INFO * err_info, float * ftmp )
{
    long Utt_id;
    long Utt_DelayEst;
    long Utt_Delay;
    float Utt_DelayConf;
    long Utt_Start;
    long Utt_End;
    long Utt_SpeechStart;
    long Utt_SpeechEnd;
    long Utt_Len;
    long step;
    long Best_ED1, Best_ED2;
    long Best_D1, Best_D2;
    float Best_DC1, Best_DC2;
    long Best_BP;
    long Largest_uttsize = 0;
    long Downsample = inst->Downsample;

    Utt_id = 0;
    while( (Utt_id < err_info-> Nutterances) &&
           (err_info-> Nutterances < MAXNUTTERANCES) )
    {
        Utt_DelayEst = err_info-> Utt_DelayEst [Utt_id];
        Utt_Delay = err_info-> Utt_Delay [Utt_id];
        Utt_DelayConf = err_info-> Utt_DelayConf [Utt_id];
        Utt_Start = err_info-> Utt_Start [Utt_id];
        Utt_End = err_info-> Utt_End [Utt_id];

        Utt_SpeechStart = Utt_Start;
        while( (Utt_SpeechStart < Utt_End) && (ref_info-> VAD [Utt_SpeechStart] <= 0.0f) )
            Utt_SpeechStart++;
        Utt_SpeechEnd = Utt_End;
        while( (Utt_SpeechEnd > Utt_Start) && (ref_info-> VAD [Utt_SpeechEnd] <= 0.0f) )
            Utt_SpeechEnd--;
        Utt_SpeechEnd++;
        Utt_Len = Utt_SpeechEnd - Utt_SpeechStart;

        if( Utt_Len >= 200 )
        {
            split_align(inst, ref_info, deg_info, err_info, ftmp,
                Utt_Start, Utt_SpeechStart, Utt_SpeechEnd, Utt_End,
                Utt_DelayEst, Utt_DelayConf,
                &Best_ED1, &Best_D1, &Best_DC1,
                &Best_ED2, &Best_D2, &Best_DC2,
                &Best_BP );

            if( (Best_DC1 > Utt_DelayConf) && (Best_DC2 > Utt_DelayConf) )
            {
                for (step = err_info-> Nutterances-1; step > Utt_id; step-- )
                {
                    err_info-> Utt_DelayEst [step +1] = err_info-> Utt_DelayEst [step];
                    err_info-> Utt_Delay [step +1] = err_info-> Utt_Delay [step];
                    err_info-> Utt_DelayConf [step +1] = err_info-> Utt_DelayConf [step];
                    err_info-> Utt_Start [step +1] = err_info-> Utt_Start [step];
                    err_info-> Utt_End [step +1] = err_info-> Utt_End [step];
                    err_info-> UttSearch_Start [step +1] = err_info-> Utt_Start [step];
                    err_info-> UttSearch_End [step +1] = err_info-> Utt_End [step];
                }
                err_info-> Nutterances++;

                err_info-> Utt_DelayEst [Utt_id] = Best_ED1;
                err_info-> Utt_Delay [Utt_id] = Best_D1;
                err_info-> Utt_DelayConf [Utt_id] = Best_DC1;

                err_info-> Utt_DelayEst [Utt_id +1] = Best_ED2;
                err_info-> Utt_Delay [Utt_id +1] = Best_D2;
                err_info-> Utt_DelayConf [Utt_id +1] = Best_DC2;

                err_info-> UttSearch_Start [Utt_id +1] = err_info-> UttSearch_Start [Utt_id];
                err_info-> UttSearch_End [Utt_id +1] = err_info-> UttSearch_End [Utt_id];

                if( Best_D2 < Best_D1 )
                {
                    err_info-> Utt_Start [Utt_id] = Utt_Start;
                    err_info-> Utt_End [Utt_id] = Best_BP;
                    err_info-> Utt_Start [Utt_id +1] = Best_BP;
                    err_info-> Utt_End [Utt_id +1] = Utt_End;
                }
                else
                {
                    err_info-> Utt_Start [Utt_id] = Utt_Start;
                    err_info-> Utt_End [Utt_id] =
                        min( Best_BP + (Best_D2 - Best_D1) / (2 * Downsample),
                            (Best_BP + Utt_End)/2 );
                    err_info-> Utt_Start [Utt_id +1] =
                        max( Best_BP - (Best_D2 - Best_D1) / (2 * Downsample),
                            (Best_BP + Utt_Start)/2 );
                    err_info-> Utt_End [Utt_id +1] = Utt_End;
                }

                if( (err_info-> Utt_Start [Utt_id] - SEARCHBUFFER) * Downsample + Best_D1 < 0 )
                    err_info-> Utt_Start [Utt_id] =
                        SEARCHBUFFER + (Downsample - 1 - Best_D1) / Downsample;

                if( (err_info-> Utt_End [Utt_id +1] * Downsample + Best_D2) >
                    ((*deg_info).Nsamples - SEARCHBUFFER * Downsample) )
                    err_info-> Utt_End [Utt_id +1] =
                        ((*deg_info).Nsamples - Best_D2) / Downsample - SEARCHBUFFER;

            }
            else Utt_id++;
        }
        else Utt_id++;
    }

    for (Utt_id = 0; Utt_id < err_info-> Nutterances; Utt_id++ )
        if( (err_info-> Utt_End [Utt_id] - err_info-> Utt_Start [Utt_id])
             > Largest_uttsize )
            Largest_uttsize = 
                err_info-> Utt_End [Utt_id] - err_info-> Utt_Start [Utt_id];

    err_info-> Largest_uttsize = Largest_uttsize;
}


/************************************************************
*   utterance_locate
*
*   Syntax        void utterance_locate( SIGNAL_INFO * ref_info, 
*                    SIGNAL_INFO * deg_info, ERROR_INFO * err_info, float * ftmp )
*
*   Description
*   This function identifies utterance search area, locates the delay between
*    referenece and degraded for each utterance and then assigns the utterance
*    start and end markers in err_info
*
*   Inputs        (*ref_info).data                reference speech signal
*                (*ref_info).Nsamples            length of refernce signal in samples
*                (*ref_info).VAD                    VAD for reference signal
*                (*ref_info).logVAD                log of VAD for reference signal
*                (*deg_info).data                degraded speech signal    
*                (*deg_info).Nsamples            length of degraded signal in samples
*                (*deg_info).VAD                    VAD for degraded signal
*                (*deg_info).logVAD                log of VAD for degraded signal
*                (*deg_info).Nsamples            length of degraded signal in samples
*                (*err_info).Utt_Start[]            array of utterance start locations
*                (*err_info).Utt_End[]            array of utterance end locations
*
*   Modifies    (*err_info).UttSearch_Start[], (*err_info).UttSearch_End[],
*                (*err_info).Nutterances, (*err_info).Utt_Start[], (*err_info).Utt_End[],
*                (*err_info).Largest_uttsize, ftmp, (*err_info).Crude_DelayEst,
*                (*err_info).Utt_DelayEst[], (*err_info).Utt_Delay[]
*                (*err_info).Utt_DelayConf[], (*err_info).Utt_Level[]
*                
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL void utterance_locate(PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
    ERROR_INFO * err_info, float * ftmp )
{    
    long Utt_id;
    
    id_searchwindows(inst, ref_info, deg_info, err_info );

    for (Utt_id = 0; Utt_id < err_info-> Nutterances; Utt_id++)
    {
        crude_align(inst, ref_info, deg_info, err_info, Utt_id, ftmp);
        time_align(inst, ref_info, deg_info, err_info, Utt_id, ftmp );
    }

    id_utterances(inst, ref_info, deg_info, err_info );

    utterance_split(inst, ref_info, deg_info, err_info, ftmp );   
}


/************************************************************
*   short_term_fft
*
*   Syntax        void short_term_fft (int Nf, SIGNAL_INFO *info, float *window, 
*                    long start_sample, float *hz_spectrum, float *fft_tmp)
*
*   Description
*   Wrapper function to apply windowed FFT on segment of signal and 
*    return in hz_spectrum
*
*   Inputs        (*info).data        pointer to speech signal
*                Nf                    size in samples of fft to perform 
*                window                array containing window coefficients
*                start_sample        start index in (*info).data for fft
*                fft_tmp                tempory storage area for fft processing        
*
*   Modifies    fft_tmp, hz_spectrum
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL void short_term_fft (FFT_t *pfft, int Nf, SIGNAL_INFO *info, float *window, long start_sample, 
                     float *hz_spectrum, float *fft_tmp) 
{
    int n, k;        

    for (n = 0; n < Nf; n++ )
    {
        fft_tmp [n] = info-> data [start_sample + n] * window [n];
    }
    RealFFT(pfft, fft_tmp, Nf);

    for (k = 0; k < Nf / 2; k++ ) 
    {
        hz_spectrum [k] = fft_tmp [k << 1] * fft_tmp [k << 1] + fft_tmp [1 + (k << 1)] * fft_tmp [1 + (k << 1)];
    }    

    hz_spectrum [0] = 0;
}

/************************************************************
*   freq_warping
*
*   Syntax        void freq_warping (float *hz_spectrum, int Nb,
*               float *pitch_pow_dens, long frame) 
*
*   Description
*   Performs frequency to pitch warping  of auditory transform (Herz to Bark)
*    and places the results in pitch_pow_dens
*
*   Inputs        hz_spectrum                array containing frequency bins
*                Nb                        number of Bark bands
*                frame                    current frame number
*
*   Modifies    pitch_pow_dens
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL void freq_warping (PESQ_t *inst, float *hz_spectrum, int Nb, float *pitch_pow_dens, long frame) 
{

    int        hz_band = 0;
    int        bark_band;
    double    sum;
    float Sp = inst->Sp;

    for (bark_band = 0; bark_band < Nb; bark_band++) {
        int n = inst->nr_of_hz_bands_per_bark_band [bark_band];
        int i;

        sum = 0;
        for (i = 0; i < n; i++) {
            sum += hz_spectrum [hz_band++];
        }
        
        sum *= inst->pow_dens_correction_factor [bark_band];
        sum *= Sp;
        pitch_pow_dens [frame * Nb + bark_band] = (float) sum;
    }
}


/************************************************************
*   total_audible
*
*   Syntax        float total_audible (int frame, float *pitch_pow_dens, 
*                            float factor) 
*
*   Description
*   Calculates the sum of pitch powers for a particular frame, but only for
*    pitch powers greater than a pitch dependent threshold. 
*
*   Inputs        frame                    current frame to process
*                picth_pow_dens            array containing picth representation of signal
*                factor                    thresholding value
*                
*   Modifies    None
*
*   Returns        sum of pitch powers above a pitch dependent threshold
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL float total_audible (PESQ_t *inst, int frame, float *pitch_pow_dens, float factor) 
{
    int     band;
    float   h, threshold;
    double    result;
    int Nb = inst->Nb;
    
    result = 0.;
    for (band= 1; band< Nb; band++) {
        h = pitch_pow_dens [frame * Nb + band];
        threshold = (float) (factor * inst->abs_thresh_power [band]);
        if (h > threshold) {
            result += h;
        }
    }
    return (float) result;
}


/************************************************************
*   time_avg_audible_of
*
*   Syntax        void time_avg_audible_of (int number_of_frames, int *silent, float *pitch_pow_dens, 
*                          float *avg_pitch_pow_dens, int total_number_of_frames) 
*
*   Description
*    Calculates mean power spectrum across specified active speech frames
*
*   Inputs        number_of_frames        length in frames of signal to process
*                silent                    array containing speech/non-speech decision
*                pitch_pow_dens            array containing pitch representation of signal
*                total_number_of_frames    averaging factor
*                
*   Modifies    avg_pitch_pow_dens
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL void time_avg_audible_of (PESQ_t *inst, int number_of_frames, int *silent, float *pitch_pow_dens, 
                          float *avg_pitch_pow_dens, int total_number_of_frames) 
{
    int    frame;
    int    band;
    int Nb = inst->Nb;

    for (band = 0; band < Nb; band++) {
        double result = 0;
        for (frame = 0; frame < number_of_frames; frame++) {
            if (!silent [frame]) {
                float h = pitch_pow_dens [frame * Nb + band];
                if (h > 100 * inst->abs_thresh_power [band]) {
                    result += h;
                }
            }
        }

        avg_pitch_pow_dens [band] = (float) (result / total_number_of_frames);
    }
}            


/************************************************************
*   freq_resp_compensation
*
*   Syntax        void freq_resp_compensation (int number_of_frames, float *pitch_pow_dens_ref, 
*                    float *avg_pitch_pow_dens_ref, float *avg_pitch_pow_dens_deg, float constant)
*
*   Description
*    Computes transfer function estimate from reference and degraded average pitch 
*    power profile. Then applies compensation to the reference pitch signal.
*
*   Inputs        number_of_frames        length in frames of signal to process
*                pitch_pow_dens_ref        array containing reference pitch signal
*                avg_pitch_pow_dens_ref    array containing av. pitch profile of reference
*                avg_pitch_pow_dens_deg    array containing av. pitch profile of degraded
*                constant                regularisation constant
*                
*   Modifies    apitch_pow_dens_ref
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL void freq_resp_compensation (PESQ_t *inst, int number_of_frames, float *pitch_pow_dens_ref, float *avg_pitch_pow_dens_ref,
                             float *avg_pitch_pow_dens_deg, float constant)
{
    int band;
    int Nb = inst->Nb;

    for (band = 0; band < Nb; band++) {
        float    x = (avg_pitch_pow_dens_deg [band] + constant) / (avg_pitch_pow_dens_ref [band] + constant);
        int        frame;

        if (x > (float) 100.0) {x = (float) 100.0;} 
        if (x < (float) 0.01) {x = (float) 0.01;}   

        for (frame = 0; frame < number_of_frames; frame++) {        
            pitch_pow_dens_ref [frame * Nb + band] *= x;
        }        
    }
}


/************************************************************
*   intensity_warping_of
*
*   Syntax        void intensity_warping_of (float *loudness_dens, int frame, 
*                    float *pitch_pow_dens)
*
*   Description
*    Performs the PESQ loudness warping  in the auditory transform
*
*   Inputs        frame                frame to process
*                pitch_pow_dens        array containing reference pitch signal
*                            
*   Modifies    loudness_dens
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL void intensity_warping_of (PESQ_t *inst, float *loudness_dens, int frame, float *pitch_pow_dens)
{
    int      band;
    float    h;
    double   modified_zwicker_power;
    int Nb = inst->Nb;
    float Sl = inst->Sl;

    for (band = 0; band < Nb; band++) {
        float threshold = (float) inst->abs_thresh_power [band];
        float input = pitch_pow_dens [frame * Nb + band];

        if (inst->centre_of_band_bark [band] < (float) 4) {
            h =  (float) 6 / ((float) inst->centre_of_band_bark [band] + (float) 2);
        } else {
            h = (float) 1;
        }
        if (h > (float) 2) {h = (float) 2;}
        h = (float) pow (h, (float) 0.15); 
        modified_zwicker_power = ZWICKER_POWER * h;

        if (input > threshold) {
            loudness_dens [band] = (float) (pow (threshold / 0.5, modified_zwicker_power)
                                                    * (pow (0.5 + 0.5 * input / threshold, modified_zwicker_power) - 1));
        } else {
            loudness_dens [band] = 0;
        }

        loudness_dens [band] *= (float) Sl;
    }    
}


/************************************************************
*   pseudo_Lp
*
*   Syntax        float pseudo_Lp (float *x, float p)
*
*   Description
*    Provides a non-linear weigthed average (Lp) of the array x, where as p 
*    increases more the result is dominated by the larger values of the array.
*
*   Inputs        x        array to average
*                p        power of averaging function
*                            
*   Modifies    None
*
*   Returns        weighted non-linear average
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL float pseudo_Lp (PESQ_t *inst, float *x, float p) 
{   
    double totalWeight = 0;
    double result = 0;
    int    band;
    int Nb = inst->Nb;

    for (band = 1; band < Nb; band++) {
        float h = (float) fabs (x [band]);        
        float w = (float) inst->width_of_band_bark [band];
        float prod = h * w;

        result += pow (prod, p);
        totalWeight += w;
    }

    result /= totalWeight;
    result = pow (result, 1/p);
    result *= totalWeight;
    
    return (float) result;
}  


/************************************************************
*   multiply_with_asymmetry_factor
*
*   Syntax        void multiply_with_asymmetry_factor (float *disturbance_dens, int frame, 
*                  const float * const pitch_pow_dens_ref, const float * const pitch_pow_dens_deg)
*
*   Description
*    Calculated asymetry factor from reference and degraded pitch signals and 
*    multiplies the error surface by factor 
*
*   Inputs        disturbance_dens        error surface
*                frame                    frame to process
*                pitch_pow_dens_ref        reference pitch signal
*                pitch_pow_dens_deg        degraded pitch signal
*                            
*   Modifies    disturbance_dens
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL void multiply_with_asymmetry_factor (PESQ_t *inst, float *disturbance_dens, int frame, 
                   const float * const pitch_pow_dens_ref, const float * const pitch_pow_dens_deg) 
{
    int   i;
    float ratio, h;
    int Nb = inst->Nb;

    for (i = 0; i < Nb; i++) {
        ratio = (pitch_pow_dens_deg [frame * Nb + i] + (float) 50)
                  / (pitch_pow_dens_ref [frame * Nb + i] + (float) 50);

        h = (float) pow (ratio, (float) 1.2);    
        if (h > (float) 12) {h = (float) 12;}
        if (h < (float) 3) {h = (float) 0.0;}

        disturbance_dens [i] *= h;
    }
}


/************************************************************
*   pow_of
*
*   Syntax        double pow_of (const float * const x, long start_sample, 
*                            long stop_sample, long divisor)
*
*   Description
*    Calculates the power sum of an array between the start_sample and 
*    stop_sample, the returns this divided by divisor 
*
*   Inputs        x                        array containig signal to process
*                start_sample            start index for processing
*                stop_sample                end index for processing
*                divisor                    normalisation factor
*                            
*   Modifies    None
*
*   Returns        normalised power summation
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL double pow_of (const float * const x, long start_sample, long stop_sample, long divisor) 
{
    long    i;
    double  power = 0;

    if (start_sample < 0) {
        exit (1);
    }

    if (start_sample > stop_sample) {
        exit (1);
    }

    for (i = start_sample; i < stop_sample; i++) {
        float h = x [i];
        power += h * h;        
    }
    
    power /= divisor;
    return power;
}


/************************************************************
*   compute_delay
*
*   Syntax        int compute_delay (long start_sample, long stop_sample, long search_range, 
*                   float *time_series1, float *time_series2, float *max_correlation)
*
*   Description
*    Computes time offset between two time series arrays using cross correlation
*
*   Inputs        start_sample            start index for processing
*                stop_sample                end index for processing
*                search_range            limits magnetude of delays to search for
*                time_series1            first time series
*                time_series2            second time series
*
*                            
*   Modifies    max_correlation
*
*   Returns        index of best delay
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL int compute_delay (FFT_t *pfft, long start_sample, long stop_sample, long search_range, 
                   float *time_series1, float *time_series2, float *max_correlation)
{

    double            power1, power2, normalization;
    long            i;
    float           *x1, *x2, *y;
    double            h;
    long            n = stop_sample - start_sample;   
    long            power_of_2 = nextpow2 (2 * n);
    long            best_delay;

    power1 = pow_of (time_series1, start_sample, stop_sample, stop_sample - start_sample) * (double) n/(double) power_of_2;
    power2 = pow_of (time_series2, start_sample, stop_sample, stop_sample - start_sample) * (double) n/(double) power_of_2;
    normalization = sqrt (power1 * power2);

    if ((power1 <= 1E-6) || (power2 <= 1E-6)) {
        *max_correlation = 0;
        return 0;
    }

    x1 = (float *) safe_malloc ((power_of_2 + 2) * sizeof (float));;
    x2 = (float *) safe_malloc ((power_of_2 + 2) * sizeof (float));;
    y = (float *) safe_malloc ((power_of_2 + 2) * sizeof (float));;
    
    for (i = 0; i < power_of_2 + 2; i++) {
        x1 [i] = 0.;
        x2 [i] = 0.;
        y [i] = 0.;
    }

    for (i = 0; i < n; i++) {
        x1 [i] = (float) fabs (time_series1 [i + start_sample]);
        x2 [i] = (float) fabs (time_series2 [i + start_sample]);
    }

    RealFFT (pfft, x1, power_of_2);
    RealFFT (pfft, x2, power_of_2);

    for (i = 0; i <= power_of_2 / 2; i++) { 
        x1 [2 * i] /= power_of_2;
        x1 [2 * i + 1] /= power_of_2;                
    }

    for (i = 0; i <= power_of_2 / 2; i++) { 
        y [2*i] = x1 [2*i] * x2 [2*i] + x1 [2*i + 1] * x2 [2*i + 1];
        y [2*i + 1] = -x1 [2*i + 1] * x2 [2*i] + x1 [2*i] * x2 [2*i + 1];
    }    
  
    RealIFFT (pfft, y, power_of_2);

    best_delay = 0;
    *max_correlation = 0;

    for (i = -search_range; i <= -1; i++) {
        h = (float) fabs (y [(i + power_of_2)]) / normalization;
        if (fabs (h) > (double) *max_correlation) {
            *max_correlation = (float) fabs (h);
            best_delay= i;
        }
    }

    for (i = 0; i < search_range; i++) {
        h = (float) fabs (y [i]) / normalization;
        if (fabs (h) > (double) *max_correlation) {
            *max_correlation = (float) fabs (h);
            best_delay= i;
        }
    }

    safe_free (x1);
    safe_free (x2);
    safe_free (y);
    
    return best_delay;
}


/************************************************************
*   Lpq_weight
*
*   Syntax        float Lpq_weight (int start_frame, int stop_frame, float power_syllable,
*                 float power_time, float *frame_disturbance, float *time_weight) 
*
*   Description
*    Calculates weigthed non-linear averages (Lp) over split second 
*    intervals (syllables) and then over the whole signal length.  
*    This function also includes a frame-by-frame weighting
*
*   Inputs        start_frame                start index for processing
*                stop_frame                end index for processing
*                power_syllable            value of p for syllables
*                power_time                value of p for whole signal
*                frame_disturbance        array of signal for processing
*                time_weight                frame-by-frame weighting
*                            
*   Modifies    None
*
*   Returns        single value of average distortion
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL float Lpq_weight (int start_frame, int stop_frame, float power_syllable,
                  float power_time, float *frame_disturbance, float *time_weight) 
{

    double    result_time= 0;
    double  total_time_weight_time = 0;
    int        start_frame_of_syllable;
    
    for (start_frame_of_syllable = start_frame; 
         start_frame_of_syllable <= stop_frame; 
         start_frame_of_syllable += NUMBER_OF_FRAMES_PER_SYLLABLE/2) {

        double  result_syllable = 0;
        int     count_syllable = 0;
        int     frame;

        for (frame = start_frame_of_syllable;
             frame < start_frame_of_syllable + NUMBER_OF_FRAMES_PER_SYLLABLE;
             frame++) {
            if (frame <= stop_frame) {
                float h = frame_disturbance [frame];
                result_syllable +=  pow (h, power_syllable); 
            }
            count_syllable++;                
        }

        result_syllable /= count_syllable;
        result_syllable = pow (result_syllable, (double) 1/power_syllable);        
    
        result_time+=  pow (time_weight [start_frame_of_syllable - start_frame] * result_syllable, power_time); 
        total_time_weight_time += pow (time_weight [start_frame_of_syllable - start_frame], power_time);
    }

    result_time /= total_time_weight_time;
    result_time= pow (result_time, (float) 1 / power_time);

    return (float) result_time;
}

void set_to_sine (SIGNAL_INFO *info, float amplitude, float omega) {
    long i;

    for (i = 0; i < info-> Nsamples; i++) {
        info-> data [i] = amplitude * (float) sin (omega * i);
    }
}

float maximum_of (float *x, long start, long stop) {
    long i;
    float result = -1E20f;

    for (i = start; i < stop; i++) {
        if (result < x [i]) {
            result = x [i];
        }
    }

    return result;
}

float integral_of (PESQ_t *inst, float *x, long frames_after_start) {
    double result = 0;
    int    band;
    int Nb = inst->Nb;

    for (band = 1; band < Nb; band++) {
        result += x [frames_after_start * Nb + band] * inst->width_of_band_bark [band];        
    }
    return (float) result;    
}

#define DEBUG_FR    0

/************************************************************
*   pesq_psychoacoustic_model
*
*   Syntax        void pesq_psychoacoustic_model(SIGNAL_INFO * ref_info, 
*                    SIGNAL_INFO * deg_info, ERROR_INFO * err_info)
*
*   Description
*    Performs core PESQ psychoacoustic model and calculates PESQ quality measure
*
*   Inputs        (ref_info).data            reference signal
*                (ref_info).Nsamples        length of reference in samples
*                (deg_info).data            degraded signal
*                (deg_info).Nsamples        length of degraded in samples
*                
*                            
*   Modifies    (ref_info).VAD, (ref_info).logVAD, (deg_info).VAD, (deg_info).logVAD
*                err_info
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*   Documented    06/03/2001 - RR
*   Updated       14/11/2001 - AR
*      Implemented new mapping to PESQ-LQ.
*      Implemented new noise addition to deal with silent references.
*      Corrected bug in mnb (number of bands to return).
*   Updated        22/02/2002 - AR
*      New FrameDelay member of err_info.
*   Updated        24/09/2002 - AR
*      New Ie (P.834) calculation; slight changes to order
*      of LV_Params.
************************************************************/
PESQ_DECL void pesq_psychoacoustic_model(PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
                               ERROR_INFO * err_info)
{
    long    maxNsamples = max (ref_info-> Nsamples, deg_info-> Nsamples);
    long Fs = inst->Fs;
    long Downsample = inst->Downsample;
    long    Nf = Downsample * 8L;
    long    start_frame, stop_frame;
    long    samples_to_skip_at_start, samples_to_skip_at_end;
    float   sum_of_5_samples;
    long    n, i;
    long    frame, band;
    float   *fft_tmp;
    float    *hz_spectrum_ref, *hz_spectrum_deg;
    float   *pitch_pow_dens_ref, *pitch_pow_dens_deg;
    float    *loudness_dens_ref, *loudness_dens_deg;
    float   *avg_pitch_pow_dens_ref, *avg_pitch_pow_dens_deg;
    float    *deadzone;
    float   *disturbance_dens, *disturbance_dens_asym_add;
    float     total_audible_pow_ref, total_audible_pow_deg;
    int        *silent;
    float    oldScale, scale;
    int     *frame_was_skipped;
    float   *frame_disturbance;
    float   *frame_disturbance_asym_add;
    float   *total_power_ref;
    int         utt;
    int        *frame_is_bad; 
    int        *smeared_frame_is_bad; 
    int         start_frame_of_bad_interval [MAX_NUMBER_OF_BAD_INTERVALS];    
    int         stop_frame_of_bad_interval [MAX_NUMBER_OF_BAD_INTERVALS];    
    int         start_sample_of_bad_interval [MAX_NUMBER_OF_BAD_INTERVALS];    
    int         stop_sample_of_bad_interval [MAX_NUMBER_OF_BAD_INTERVALS];   
    int         number_of_samples_in_bad_interval [MAX_NUMBER_OF_BAD_INTERVALS];    
    int         delay_in_samples_in_bad_interval  [MAX_NUMBER_OF_BAD_INTERVALS];    
    int         number_of_bad_intervals= 0;
    int         search_range_in_samples;
    int         bad_interval;
    float *untweaked_deg = NULL;
    float    *tweaked_deg = NULL;
    float *doubly_tweaked_deg = NULL;
    int         there_is_a_bad_frame = FALSE;
    float    *time_weight;
    float    d_indicator = 0.0f;
    float    a_indicator = 0.0f;
    int      nn;
    int mnb;
    int Nb;

    /* Define Hann window function */
    float Whanning [Nfmax];

    for (n = 0L; n < Nf; n++ ) {
        Whanning [n] = (float)(0.5 * (1.0 - cos((TWOPI * n) / Nf)));
    }

    switch (Fs) {
    case 8000:
        inst->Nb = 42;
        inst->Sl = (float) Sl_8k;
        inst->Sp = (float) Sp_8k;
        inst->nr_of_hz_bands_per_bark_band = nr_of_hz_bands_per_bark_band_8k;
        inst->centre_of_band_bark = centre_of_band_bark_8k;
        inst->centre_of_band_hz = centre_of_band_hz_8k;
        inst->width_of_band_bark = width_of_band_bark_8k;
        inst->width_of_band_hz = width_of_band_hz_8k;
        inst->pow_dens_correction_factor = pow_dens_correction_factor_8k;
        inst->abs_thresh_power = abs_thresh_power_8k;
        break;
    case 16000:
        inst->Nb = 49;
        inst->Sl = (float) Sl_16k;
        inst->Sp = (float) Sp_16k;
        inst->nr_of_hz_bands_per_bark_band = nr_of_hz_bands_per_bark_band_16k;
        inst->centre_of_band_bark = centre_of_band_bark_16k;
        inst->centre_of_band_hz = centre_of_band_hz_16k;
        inst->width_of_band_bark = width_of_band_bark_16k;
        inst->width_of_band_hz = width_of_band_hz_16k;
        inst->pow_dens_correction_factor = pow_dens_correction_factor_16k;
        inst->abs_thresh_power = abs_thresh_power_16k;
        break;
    default:
        printf ("Invalid sample frequency!\n");
        return;
    }

    /* Number of perceptual bands of data to return to the calling code. */
    Nb = inst->Nb;
    mnb = Nb;
    if( mnb > err_info->rtn_bands ) mnb = err_info->rtn_bands;

    /* Identify silence at start and end of the reference */
    samples_to_skip_at_start = 0;
    do {
        sum_of_5_samples= (float) 0;
        for (i = 0; i < 5; i++) {
            sum_of_5_samples += (float) fabs (ref_info-> data [SEARCHBUFFER * Downsample + samples_to_skip_at_start + i]);
        }
        if (sum_of_5_samples< CRITERIUM_FOR_SILENCE_OF_5_SAMPLES) {
            samples_to_skip_at_start++;         
        }        
    } while ((sum_of_5_samples< CRITERIUM_FOR_SILENCE_OF_5_SAMPLES) 
            && (samples_to_skip_at_start < maxNsamples / 2));
    
    samples_to_skip_at_end = 0;
    do {
        sum_of_5_samples= (float) 0;
        for (i = 0; i < 5; i++) {
            sum_of_5_samples += (float) fabs (ref_info-> data [maxNsamples - SEARCHBUFFER * Downsample + DATAPADDING_MSECS  * (Fs / 1000) - 1 - samples_to_skip_at_end - i]);
        }
        if (sum_of_5_samples< CRITERIUM_FOR_SILENCE_OF_5_SAMPLES) {
            samples_to_skip_at_end++;         
        }        
    } while ((sum_of_5_samples< CRITERIUM_FOR_SILENCE_OF_5_SAMPLES) 
        && (samples_to_skip_at_end < maxNsamples / 2));
       
    start_frame = samples_to_skip_at_start / (Nf /2);
    stop_frame = (maxNsamples - 2 * SEARCHBUFFER * Downsample + DATAPADDING_MSECS  * (Fs / 1000) - samples_to_skip_at_end) / (Nf /2) - 1; 

    
    /* Allocate memory for psychoacoustic processing */
    fft_tmp                = (float *) safe_malloc ((Nf + 2) * sizeof (float));
    hz_spectrum_ref        = (float *) safe_malloc ((Nf / 2) * sizeof (float));
    hz_spectrum_deg        = (float *) safe_malloc ((Nf / 2) * sizeof (float));
    
    frame_is_bad        = (int *) safe_malloc ((stop_frame + 1) * sizeof (int)); 
    smeared_frame_is_bad=(int *) safe_malloc ((stop_frame + 1) * sizeof (int)); 
    
    silent                = (int *) safe_malloc ((stop_frame + 1) * sizeof (int));

    pitch_pow_dens_ref    = (float *) safe_malloc ((stop_frame + 1) * Nb * sizeof (float));
    pitch_pow_dens_deg    = (float *) safe_malloc ((stop_frame + 1) * Nb * sizeof (float));
    
    frame_was_skipped    = (int *) safe_malloc ((stop_frame + 1) * sizeof (int));

    frame_disturbance    = (float *) safe_malloc ((stop_frame + 1) * sizeof (float));
    frame_disturbance_asym_add    = (float *) safe_malloc ((stop_frame + 1) * sizeof (float));

    avg_pitch_pow_dens_ref = (float *) safe_malloc (Nb * sizeof (float));
    avg_pitch_pow_dens_deg = (float *) safe_malloc (Nb * sizeof (float));
    loudness_dens_ref    = (float *) safe_malloc (Nb * sizeof (float));
    loudness_dens_deg    = (float *) safe_malloc (Nb * sizeof (float));;
    deadzone                = (float *) safe_malloc (Nb * sizeof (float));;
    disturbance_dens    = (float *) safe_malloc (Nb * sizeof (float));
    disturbance_dens_asym_add = (float *) safe_malloc (Nb * sizeof (float));    

    time_weight            = (float *) safe_malloc ((stop_frame + 1) * sizeof (float));
    total_power_ref     = (float *) safe_malloc ((stop_frame + 1) * sizeof (float));


    /* Calculate frame-by-frame pitch power density for both
       reference and degraded signals */
    for (frame = 0; frame <= stop_frame; frame++) {
        int start_sample_ref = SEARCHBUFFER * Downsample + frame * Nf / 2;
        int start_sample_deg;
        int delay;    

        /* Spectrum of reference */
        short_term_fft (inst->pfft, Nf, ref_info, Whanning, start_sample_ref, hz_spectrum_ref, fft_tmp);
        
        if (err_info-> Nutterances < 1) {
            printf ("Processing error!\n");
            return;
        }

        /* Compute delay for this frame */
        utt = err_info-> Nutterances - 1;
        while ((utt >= 0) && (err_info-> Utt_Start [utt] * Downsample > start_sample_ref)) {
            utt--;
        }
        if (utt >= 0) {
            delay = err_info-> Utt_Delay [utt];
        } else {
            delay = err_info-> Utt_Delay [0];        
        }
        start_sample_deg = start_sample_ref + delay;         

        /* Spectrum of degraded */
        if ((start_sample_deg > 0) && (start_sample_deg + Nf < maxNsamples + DATAPADDING_MSECS  * (Fs / 1000))) {
            short_term_fft (inst->pfft, Nf, deg_info, Whanning, start_sample_deg, hz_spectrum_deg, fft_tmp);            
        } else {
            for (i = 0; i < Nf / 2; i++) {
                hz_spectrum_deg [i] = 0;
            }
        }

        /* Warp both reference and degraded to the Bark-like pitch scale */
        freq_warping (inst, hz_spectrum_ref, Nb, pitch_pow_dens_ref, frame);
        freq_warping (inst, hz_spectrum_deg, Nb, pitch_pow_dens_deg, frame);

        /* Compute power of both signals and make activity decision */
        total_audible_pow_ref = total_audible (inst, frame, pitch_pow_dens_ref, 1E2);
        total_audible_pow_deg = total_audible (inst, frame, pitch_pow_dens_deg, 1E2);        
        silent [frame] = (total_audible_pow_ref < 1E7);     

        /* Store frame delay */
        if( frame < err_info->Nframes )
            err_info->FrameDelay[frame] = delay;

        /* Write pre-equalised surfaces (before transfer function and time-varying gain
           have been cancelled out) */
        if( (err_info->LV_ref_surf_preeq != NULL) && (frame < err_info->LV_Nframes) ) {
            intensity_warping_of (inst, loudness_dens_ref, frame, pitch_pow_dens_ref); 
            for( band = 0; band < mnb; band++ )
                err_info->LV_ref_surf_preeq[frame*err_info->rtn_bands + band]
                    = loudness_dens_ref[band];
        }
        if( (err_info->LV_deg_surf_preeq != NULL) && (frame < err_info->LV_Nframes) ) {
            intensity_warping_of (inst, loudness_dens_deg, frame, pitch_pow_dens_deg);
            for( band = 0; band < mnb; band++ )
                err_info->LV_deg_surf_preeq[frame*err_info->rtn_bands + band]
                    = loudness_dens_deg[band];
        }
    }

    /* Compute weighted power spectra of reference and degraded */
    time_avg_audible_of (inst, stop_frame + 1, silent, pitch_pow_dens_ref, avg_pitch_pow_dens_ref, (maxNsamples - 2 * SEARCHBUFFER * Downsample + DATAPADDING_MSECS  * (Fs / 1000)) / (Nf / 2) - 1);
    time_avg_audible_of (inst, stop_frame + 1, silent, pitch_pow_dens_deg, avg_pitch_pow_dens_deg, (maxNsamples - 2 * SEARCHBUFFER * Downsample + DATAPADDING_MSECS  * (Fs / 1000)) / (Nf / 2) - 1);
    
    /* Perform frequency response equalisation using the spectral difference - this equalises the
       reference to the degraded using the estimated transfer function. */
    freq_resp_compensation (inst, stop_frame + 1, pitch_pow_dens_ref, avg_pitch_pow_dens_ref, avg_pitch_pow_dens_deg, 1000);

    /* Frame-by-frame processing to compute Sone loudness and perceived errors */    oldScale = 1;
    oldScale = 1;
    for (frame = 0; frame <= stop_frame; frame++) {
        /* Gain equalisation of the degraded to the reference to cancel time-varying gain. */
        total_audible_pow_ref = total_audible (inst, frame, pitch_pow_dens_ref, 1);
        total_audible_pow_deg = total_audible (inst, frame, pitch_pow_dens_deg, 1);        
        total_power_ref [frame] = total_audible_pow_ref;

        scale = (total_audible_pow_ref + (float) 5E3) / (total_audible_pow_deg + (float) 5E3);
                
        if (frame > 0) {
            scale = (float) 0.2 * oldScale + (float) 0.8*scale;
        }
        oldScale = scale;

        if (scale > (float) MAX_SCALE) scale = (float) MAX_SCALE;

        if (scale < (float) MIN_SCALE) {
            scale = (float) MIN_SCALE;            
        }

        for (band = 0; band < Nb; band++) {
            pitch_pow_dens_deg [frame * Nb + band] *= scale;
        }

        /* Control minimum of pitch_pow_dens (new in release 1.4;
           not applied if the model is -1) */
        if( (total_power_ref[frame] < POWTHRESH) && (err_info->model != -1) ) {
            float pow_target =
                POWTARGET * (POWTHRESH - total_power_ref[frame]) / POWTHRESH;
            for (band = 0; band < 42; band++)  {
                pitch_pow_dens_ref [frame * Nb + band] = max(pitch_pow_dens_ref [frame * Nb + band], pow_target);
                pitch_pow_dens_deg [frame * Nb + band] = max(pitch_pow_dens_deg [frame * Nb + band], pow_target);
            }
        }

        /* Map both signals to Sone loudness */
        intensity_warping_of (inst, loudness_dens_ref, frame, pitch_pow_dens_ref); 
        intensity_warping_of (inst, loudness_dens_deg, frame, pitch_pow_dens_deg); 

        /* Write surfaces */
        if( (err_info->LV_ref_surf != NULL) && (frame < err_info->LV_Nframes) )
            for( band = 0; band < mnb; band++ )
                err_info->LV_ref_surf[frame*err_info->rtn_bands + band]
                    = loudness_dens_ref[band];
        if( (err_info->LV_deg_surf != NULL) && (frame < err_info->LV_Nframes) )
            for( band = 0; band < mnb; band++ )
                err_info->LV_deg_surf[frame*err_info->rtn_bands + band]
                    = loudness_dens_deg[band];

        /* Compute disturbance density and apply deadzone masking */
        for (band = 0; band < Nb; band++) {
            disturbance_dens [band] = loudness_dens_deg [band] - loudness_dens_ref [band];
        }
        
        for (band = 0; band < Nb; band++) {
            deadzone [band] = min (loudness_dens_deg [band], loudness_dens_ref [band]);    
            deadzone [band] *= 0.25;
        }
        
        for (band = 0; band < Nb; band++) {
            float d = disturbance_dens [band];
            float m = deadzone [band];
                
            if (d > m) {
                disturbance_dens [band] -= m;
            } else {
                if (d < -m) {
                    disturbance_dens [band] += m;
                } else {
                    disturbance_dens [band] = 0;
                }
            }
        }    

        /* Compute symmetric disturbance */
        frame_disturbance [frame] = pseudo_Lp (inst, disturbance_dens, D_POW_F);    

        /* Note if there has been a bad frame */
        if (frame_disturbance [frame] > THRESHOLD_BAD_FRAMES) 
        {
            there_is_a_bad_frame = TRUE;
        }

        /* Compute asymmetric disturbance */
        multiply_with_asymmetry_factor (inst, disturbance_dens, frame, pitch_pow_dens_ref, pitch_pow_dens_deg);
        frame_disturbance_asym_add [frame] = pseudo_Lp (inst, disturbance_dens, A_POW_F);
    }

    /* Check for delay changes that require a frame of the degraded signal to be skipped
       because it was not heard */
    for (frame = 0; frame <= stop_frame; frame++) {
        frame_was_skipped [frame] = FALSE;
    }

    for (utt = 1; utt < err_info-> Nutterances; utt++) {
        int frame1 = (int) floor (((err_info-> Utt_Start [utt] - SEARCHBUFFER ) * Downsample + err_info-> Utt_Delay [utt]) / (Nf / 2));
        int j = (int) floor ((err_info-> Utt_End [utt-1] - SEARCHBUFFER) * Downsample + err_info-> Utt_Delay [utt-1]) / (Nf / 2);
        int delay_jump = err_info-> Utt_Delay [utt] - err_info-> Utt_Delay [utt-1];

        if (frame1 > j) {
            frame1 = j;
        }
        
        if (frame1 < 0) {
            frame1 = 0;
        }
            
        if (delay_jump < -(int) (Nf / 2)) {

            int frame2 = (int) ((err_info-> Utt_Start [utt] - SEARCHBUFFER) * Downsample + max (0, fabs (delay_jump))) / (Nf / 2) + 1; 
            
            for (frame = frame1; frame <= frame2; frame++)  {
                if (frame < stop_frame) {
                    frame_was_skipped [frame] = TRUE;

                    frame_disturbance [frame] = 0;
                    frame_disturbance_asym_add [frame] = 0;
                }
            } 
        }    
    }

    /* Process bad frames to correct for mis-aligned sections */

    /* Make copy of the degraded signal with time alignment applied */
    nn = DATAPADDING_MSECS  * (Fs / 1000) + maxNsamples;
    tweaked_deg = (float *) safe_malloc (nn * sizeof (float));
    for (i = 0; i < nn; i++) {
        tweaked_deg [i] = 0;
    } 
    for (i = SEARCHBUFFER * Downsample; i < nn - SEARCHBUFFER * Downsample; i++) {
        int  utt = err_info-> Nutterances - 1;
        long delay, j;

        while ((utt >= 0) && (err_info-> Utt_Start [utt] * Downsample > i)) {
            utt--;
        }
        if (utt >= 0) {
            delay = err_info-> Utt_Delay [utt];
        } else {
            delay = err_info-> Utt_Delay [0];        
        }
            
        j = i + delay;
        if (j < SEARCHBUFFER * Downsample) {
            j = SEARCHBUFFER * Downsample;
        }
        if (j >= nn - SEARCHBUFFER * Downsample) {
            j = nn - SEARCHBUFFER * Downsample - 1;
        }
        tweaked_deg [i] = deg_info-> data [j];
    }

    /* Re-align bad intervals */
    if (there_is_a_bad_frame) {        
        /* Identify sections of bad frames (bad intervals) */
        for (frame = 0; frame <= stop_frame; frame++) 
        {  
            frame_is_bad [frame] = (frame_disturbance [frame] > THRESHOLD_BAD_FRAMES);       

            smeared_frame_is_bad [frame] = FALSE;
        }
        frame_is_bad [0] = FALSE;

        for (frame = SMEAR_RANGE; frame < stop_frame - SMEAR_RANGE; frame++) {    
            long max_itself_and_left = frame_is_bad [frame];
            long max_itself_and_right = frame_is_bad [frame];
            long mini, i;

            for (i = -SMEAR_RANGE; i <= 0; i++) {
                if (max_itself_and_left < frame_is_bad [frame  + i]) {
                    max_itself_and_left = frame_is_bad [frame  + i];
                }
            }
        
            for (i = 0; i <= SMEAR_RANGE; i++) {
                if (max_itself_and_right < frame_is_bad [frame + i]) {
                    max_itself_and_right = frame_is_bad [frame + i];
                }
            }

            mini = max_itself_and_left;
            if (mini > max_itself_and_right) {
                mini = max_itself_and_right;
            }

            smeared_frame_is_bad [frame] = mini;
        }

        /* Count the number of bad intervals */
        number_of_bad_intervals = 0;    
        frame = 0; 
        while (frame <= stop_frame) {

            while ((frame <= stop_frame) && (!smeared_frame_is_bad [frame])) {
                frame++; 
            }

            if (frame <= stop_frame) { 
                start_frame_of_bad_interval [number_of_bad_intervals] = frame;

                while ((frame <= stop_frame) && (smeared_frame_is_bad [frame])) {
                    frame++; 
                }
            
                if (frame <= stop_frame) {
                    stop_frame_of_bad_interval [number_of_bad_intervals] = frame; 

                    if (stop_frame_of_bad_interval [number_of_bad_intervals] - start_frame_of_bad_interval [number_of_bad_intervals] >= MINIMUM_NUMBER_OF_BAD_FRAMES_IN_BAD_INTERVAL) {
                        number_of_bad_intervals++; 
                    }
                }
            }
        }

        /* Locate the bad intervals */
        for (bad_interval = 0; bad_interval < number_of_bad_intervals; bad_interval++) {
            start_sample_of_bad_interval [bad_interval] =  start_frame_of_bad_interval [bad_interval] * (Nf / 2) + SEARCHBUFFER * Downsample;
            stop_sample_of_bad_interval [bad_interval] =  stop_frame_of_bad_interval [bad_interval] * (Nf / 2) + Nf + SEARCHBUFFER* Downsample;
            if (stop_frame_of_bad_interval [bad_interval] > stop_frame) {
                stop_frame_of_bad_interval [bad_interval] = stop_frame; 
            }

            number_of_samples_in_bad_interval [bad_interval] =  stop_sample_of_bad_interval [bad_interval] - start_sample_of_bad_interval [bad_interval];
        }

        search_range_in_samples= SEARCH_RANGE_IN_TRANSFORM_LENGTH * Nf;

        /* Re-align each bad interval */
        for (bad_interval= 0; bad_interval< number_of_bad_intervals; bad_interval++) {
            float  *ref = (float *) safe_malloc ( (2 * search_range_in_samples + number_of_samples_in_bad_interval [bad_interval]) * sizeof (float));
            float  *deg = (float *) safe_malloc ( (2 * search_range_in_samples + number_of_samples_in_bad_interval [bad_interval]) * sizeof (float));
            int        i;
            float    best_correlation;
            int        delay_in_samples;

            for (i = 0; i < search_range_in_samples; i++) {
                ref[i] = 0.0f;
            }
            for (i = 0; i < number_of_samples_in_bad_interval [bad_interval]; i++) {
                ref [search_range_in_samples + i] = ref_info-> data [start_sample_of_bad_interval [bad_interval] + i];
            }
            for (i = 0; i < search_range_in_samples; i++) {
                ref [search_range_in_samples + number_of_samples_in_bad_interval [bad_interval] + i] = 0.0f;
            }
        
            for (i = 0; 
                 i < 2 * search_range_in_samples + number_of_samples_in_bad_interval [bad_interval];
                 i++) {
                
                int j = start_sample_of_bad_interval [bad_interval] - search_range_in_samples + i;
                int nn = maxNsamples - SEARCHBUFFER * Downsample + DATAPADDING_MSECS  * (Fs / 1000);

                if (j < SEARCHBUFFER * Downsample) {
                    j = SEARCHBUFFER * Downsample;
                }
                if (j >= nn) {
                    j = nn - 1;
                }
                deg [i] = tweaked_deg [j]; 
            }

            delay_in_samples= compute_delay (inst->pfft, 0, 
                                             2 * search_range_in_samples + number_of_samples_in_bad_interval [bad_interval], 
                                             search_range_in_samples,
                                             ref, 
                                             deg,
                                             &best_correlation);

            delay_in_samples_in_bad_interval [bad_interval] =  delay_in_samples;

            if (best_correlation < 0.5) {
                delay_in_samples_in_bad_interval  [bad_interval] = 0;
            } 

            safe_free (ref);
            safe_free (deg);
        }

        if (number_of_bad_intervals > 0) {
            /* Re-align degraded file with new time alignment data */
            doubly_tweaked_deg = (float *) safe_malloc ((maxNsamples + DATAPADDING_MSECS  * (Fs / 1000)) * sizeof (float));

            for (i = 0; i < maxNsamples + DATAPADDING_MSECS  * (Fs / 1000); i++) {
                doubly_tweaked_deg [i] = tweaked_deg [i];
            }
        
            for (bad_interval= 0; bad_interval< number_of_bad_intervals; bad_interval++) {
                int delay = delay_in_samples_in_bad_interval  [bad_interval];
                int i;

                for (i = start_sample_of_bad_interval [bad_interval]; i < stop_sample_of_bad_interval [bad_interval]; i++) {
                    float h;
                    int j = i + delay;
                    if (j < 0) {
                        j = 0;
                    }
                    if (j >= maxNsamples) {
                        j = maxNsamples - 1;

                    }
                    doubly_tweaked_deg [i] = h = tweaked_deg [j];        
                }
            }

            untweaked_deg = deg_info-> data;
            deg_info-> data = doubly_tweaked_deg;

            for (bad_interval= 0; bad_interval < number_of_bad_intervals; bad_interval++) {
                /* Re-process each bad interval - re-apply the psychoacoustic model */
                for (frame = start_frame_of_bad_interval [bad_interval]; 
                     frame < stop_frame_of_bad_interval [bad_interval]; 
                     frame++) {

                     int start_sample_ref = SEARCHBUFFER * Downsample + frame * Nf / 2;
                    int start_sample_deg = start_sample_ref;

                    short_term_fft (inst->pfft, Nf, deg_info, Whanning, start_sample_deg, hz_spectrum_deg, fft_tmp);
                    freq_warping (inst, hz_spectrum_deg, Nb, pitch_pow_dens_deg, frame);
                }    

                oldScale = 1;
                for (frame = start_frame_of_bad_interval [bad_interval];
                     frame < stop_frame_of_bad_interval [bad_interval];
                     frame++) {
                    total_audible_pow_ref = total_audible (inst, frame, pitch_pow_dens_ref, 1);
                    total_audible_pow_deg = total_audible (inst, frame, pitch_pow_dens_deg, 1);        

                    scale = (total_audible_pow_ref + (float) 5E3) / (total_audible_pow_deg + (float) 5E3);
                
                    if (frame > 0) {
                        scale = (float) 0.2 * oldScale + (float) 0.8*scale;
                    }
                    oldScale = scale;

                    if (scale > (float) MAX_SCALE) scale = (float) MAX_SCALE;

                    if (scale < (float) MIN_SCALE) {
                        scale = (float) MIN_SCALE;            
                    }

                    for (band = 0; band < Nb; band++) {
                        pitch_pow_dens_deg [frame * Nb + band] *= scale;
                    }

                    /* Control minimum of pitch_pow_dens (new in release 1.4;
                       not applied if the model is -1) */
                    if( (total_power_ref[frame] < POWTHRESH) && (err_info->model != -1) ) {
                        float pow_target =
                            POWTARGET * (POWTHRESH - total_power_ref[frame]) / POWTHRESH;
                        for( band = 0; band < 42; band++ )  {
                            pitch_pow_dens_deg [frame * Nb + band] =
                                max(pitch_pow_dens_deg [frame * Nb + band], pow_target);
                        }
                    }

                    intensity_warping_of (inst, loudness_dens_ref, frame, pitch_pow_dens_ref); 
                    intensity_warping_of (inst, loudness_dens_deg, frame, pitch_pow_dens_deg); 
    
                    for (band = 0; band < Nb; band++) {
                        disturbance_dens [band] = loudness_dens_deg [band] - loudness_dens_ref [band];
                    }

                    for (band = 0; band < Nb; band++) {
                        deadzone [band] = min (loudness_dens_deg [band], loudness_dens_ref [band]);    
                        deadzone [band] *= 0.25;
                    }
                
                    for (band = 0; band < Nb; band++) {
                        float d = disturbance_dens [band];
                        float m = deadzone [band];
                
                        if (d > m) {
                            disturbance_dens [band] -= m;
                        } else {
                            if (d < -m) {
                                disturbance_dens [band] += m;
                            } else {
                                disturbance_dens [band] = 0;
                            }
                        }
                    }    
    
                    frame_disturbance [frame] = min (frame_disturbance [frame] , pseudo_Lp (inst, disturbance_dens, D_POW_F));    

                    /* Update frame delay */
                    if( (frame_disturbance [frame] > pseudo_Lp (inst, disturbance_dens, D_POW_F)) &&
                        (frame < err_info->Nframes) )
                        err_info->FrameDelay[frame] +=
                            delay_in_samples_in_bad_interval[bad_interval];

                    multiply_with_asymmetry_factor (inst, disturbance_dens, frame, pitch_pow_dens_ref, pitch_pow_dens_deg);
    
                    frame_disturbance_asym_add [frame] = min (frame_disturbance_asym_add [frame], pseudo_Lp (inst, disturbance_dens, A_POW_F));    
                }
            }

            safe_free (doubly_tweaked_deg);
            deg_info->data = untweaked_deg;
        }    
    }
    /* End of bad interval processing */    

    /* Apply time-varying weight to take account of forgetting of errors that
       occurred more than 16s ago (only applies to files longer than 16s) */
    for (frame = 0; frame <= stop_frame; frame++) {
        float h = 1;
        
        if (stop_frame + 1 > 1000) {
            long n = (maxNsamples - 2 * SEARCHBUFFER * Downsample) / (Nf / 2) - 1;
            double timeWeightFactor = (n - (float) 1000) / (float) 5500;
            if (timeWeightFactor > (float) 0.5) timeWeightFactor = (float) 0.5;
            h = (float) (((float) 1.0 - timeWeightFactor) + timeWeightFactor * (float) frame / (float) n);
        }

        time_weight [frame] = h;
    }

    /* Apply weighting based on loudness of signals */
    for (frame = 0; frame <= stop_frame; frame++) {

        float h = (float) pow ((total_power_ref [frame] + 1E5) / 1E7, 0.04); 

        frame_disturbance [frame] /= h;
        frame_disturbance_asym_add [frame] /= h;

        if (frame_disturbance [frame] > 45) {
            frame_disturbance [frame] = 45;
        }
        if (frame_disturbance_asym_add [frame] > 45) {
            frame_disturbance_asym_add [frame] = 45;
        }            
    }

    /* Compute average disturbance and quality score */

	/* PESQ score */
	d_indicator = Lpq_weight (start_frame, stop_frame, D_POW_S, D_POW_T, frame_disturbance, time_weight);    
	a_indicator = Lpq_weight (start_frame, stop_frame, A_POW_S, A_POW_T, frame_disturbance_asym_add, time_weight);       

	err_info-> pesq_score = (float) (4.5 - D_WEIGHT * d_indicator - A_WEIGHT * a_indicator);

	/* Produce mapped narrowband outputs for if computing according to P.862 */
	if ( err_info->model == 0 || err_info->model == -1 )
	{
		/* Compute PESQ-LQ score */
		if( err_info->pesq_score < 1.7f )
			err_info->pesq_lq = 1.0f;
		else
			err_info->pesq_lq =
				-0.157268f * err_info->pesq_score * err_info->pesq_score * err_info->pesq_score
				+ 1.386609f * err_info->pesq_score * err_info->pesq_score
				- 2.504699f * err_info->pesq_score + 2.023345f;

		/* Compute P.862.1 mapped score */
		err_info->pesq_P862_1 =
				(float) (0.999f + (4.000f / ( 1.000f + (exp ((-1.4945f * err_info->pesq_score) + 4.6607f)))));

		/* Compute Ie value
		   Reference: "Methodology for the derivation of equipment impairment
		   factors from instrumental models"  ITU-T Recommendation P.834
		   (July 2002) */
		if( err_info->pesq_score <= 1.0)
			err_info->pesq_ie = 139.3988f;
		else if( err_info->pesq_score > 4.49402822f )
			err_info->pesq_ie = -39.5144f;
		else
			err_info->pesq_ie =
				(87.2f - 6.66666667f * (float)(8.0-15.033296*cos((
				atan2( 15.0*sqrt(-903522 +1113960*err_info->pesq_score -
				202500 * err_info->pesq_score*err_info->pesq_score),
				18566.0-6750.0 * err_info->pesq_score ) + 3.14159265)/3.0))
				- 7.8502f) / 0.5226f;

		/* As suggested in P.834, make sure no value is below 0.0 */
		if( err_info->pesq_ie < 0.0f ) err_info->pesq_ie = 0.0f;
	}
	else
	{
			err_info->pesq_lq = 0.0f;
			err_info->pesq_P862_1 = 0.0f;
			err_info->pesq_ie = 0.0f;
	}

	/* Produce mapped wideand output for if computing according to P.862.2 */
	if ( err_info->model == 3 )
	{
		/* Compute P.862.2 mapped score */
		err_info->pesq_P862_2 =
				(float) (0.999f + (4.000f / ( 1.000f + (exp ((-1.3669f * err_info->pesq_score) + 3.8224f)))));
	}
	else
	{
		err_info->pesq_P862_2 = 0.0f;
	}


    /* Write other outputs */
    if (err_info->LV_Params != NULL) {
        err_info->LV_Params[0] = err_info->pesq_score;
        err_info->LV_Params[1] = err_info->pesq_lq;
        err_info->LV_Params[2] = err_info->pesq_ie;
        err_info->LV_Params[4] = (float)ref_info->Nsamples - 2 * SEARCHBUFFER * Downsample;
        err_info->LV_Params[5] = ref_info->Activity;
        err_info->LV_Params[6] = (float)deg_info->Nsamples - 2 * SEARCHBUFFER * Downsample;
        err_info->LV_Params[7] = deg_info->Activity;
        err_info->LV_Params[8] = (float)err_info->model;
        err_info->LV_Params[9] = (float)Fs;
        err_info->LV_Params[11] = d_indicator;
        err_info->LV_Params[12] = a_indicator;
		err_info->LV_Params[13] = err_info->pesq_P862_1;
		err_info->LV_Params[14] = err_info->pesq_P862_2;
    }

	if( err_info->LV_Frame_score != NULL )
		for (frame = start_frame; frame <= min(stop_frame, err_info->LV_Nframes-1); frame++)
			err_info->LV_Frame_score[frame] =
				(float)(4.5 - D_WEIGHT * frame_disturbance[frame] -
					A_WEIGHT * frame_disturbance_asym_add[frame]);
	if( err_info->LV_Sframe_err != NULL )
		for (frame = start_frame; frame <= min(stop_frame, err_info->LV_Nframes-1); frame++)
			err_info->LV_Sframe_err[frame] = frame_disturbance[frame];
	if( err_info->LV_Aframe_err != NULL )
		for (frame = start_frame; frame <= min(stop_frame, err_info->LV_Nframes-1); frame++)
			err_info->LV_Aframe_err[frame] = frame_disturbance_asym_add[frame];
	if( err_info->LV_PESQ_score != NULL )
		*(err_info->LV_PESQ_score) = err_info->pesq_score;
	if( err_info->LV_PESQ_LQ != NULL )
		*(err_info->LV_PESQ_LQ) = err_info->pesq_lq;
	if( err_info->LV_PESQ_P862_1 != NULL )
		*(err_info->LV_PESQ_P862_1) = err_info->pesq_P862_1;
	if( err_info->LV_PESQ_P862_2 != NULL )
		*(err_info->LV_PESQ_P862_2) = err_info->pesq_P862_2;
	if( err_info->LV_PESQ_Ie != NULL )
		*(err_info->LV_PESQ_Ie) = err_info->pesq_ie;
	if( err_info->LV_Params != NULL ) {
	}
	if( err_info->LV_TFE != NULL ) {
		for( band = 0; band < mnb; band++ )
			err_info->LV_TFE[band] = (float)(10.0 * log10(
				(avg_pitch_pow_dens_deg[band] + 1000.0) / (avg_pitch_pow_dens_ref[band] + 1000.0)));
	}

	/* Fix frame delay estimates for unprocessed sections at start and end */
	frame = 0;
	while( (err_info->FrameDelay[frame] >= ref_info->Nsamples)
		&& (frame < stop_frame) && (frame < (err_info->Nframes-1)) )
		frame++;
	if( err_info->FrameDelay[frame] >= ref_info->Nsamples )
		err_info->FrameDelay[frame] = 0;
	while( frame > 0 ) {
		frame--;
		err_info->FrameDelay[frame] = err_info->FrameDelay[frame+1];
	}
	frame = err_info->Nframes-1;
	while( (err_info->FrameDelay[frame] >= ref_info->Nsamples)
		&& (frame > start_frame) ) frame--;
	if( err_info->FrameDelay[frame] >= ref_info->Nsamples )
		err_info->FrameDelay[frame] = 0;
	while( frame < (err_info->Nframes - 1) ) {
		frame++;
		err_info->FrameDelay[frame] = err_info->FrameDelay[frame-1];
	}

	/* Copy out to the calling code if required */
	if( err_info->LV_FrameDelay != NULL )
		for( frame = 0; frame < err_info->LV_Nframes; frame++ )
			err_info->LV_FrameDelay[frame] = err_info->FrameDelay[frame];


    /* Free up memory */
    FFTFree(inst->pfft);
    safe_free (fft_tmp);
    safe_free (hz_spectrum_ref);
    safe_free (hz_spectrum_deg);
    safe_free (silent);
    safe_free (pitch_pow_dens_ref);
    safe_free (pitch_pow_dens_deg);
    safe_free (frame_was_skipped);
    safe_free (avg_pitch_pow_dens_ref);
    safe_free (avg_pitch_pow_dens_deg);
    safe_free (loudness_dens_ref);
    safe_free (loudness_dens_deg);
    safe_free (deadzone);
    safe_free (disturbance_dens);
    safe_free (disturbance_dens_asym_add);
    safe_free (total_power_ref);

    safe_free (frame_is_bad);
    safe_free (smeared_frame_is_bad);
    
    safe_free (time_weight);
    safe_free (frame_disturbance);
    safe_free (frame_disturbance_asym_add);
    safe_free (tweaked_deg);

    return;
}

/* END OF FILE */
