#ifndef PESQINSTANCE_INCLUDED_H
#define PESQINSTANCE_INCLUDED_H

#include "fftinstance.h"

typedef struct {
    /* Sample rate */
    long Fs;

    /* Decimation factor used for VAD and time alignment */
    long Downsample;

    /* Time alignment filter */
    long InIIR_Nsos;
    const float * InIIR_Hsos;

    /* Wideband IIR input filters, defined at 8kHz and 16kHz */
    long WB_InIIR_Nsos;
    const float * WB_InIIR_Hsos;
    
    /* Number of FFT points used for time alignment */
    long Align_Nfft;

    /* HATS IIR input filters, defined at 8kHz and 16kHz */
    long HATS_InIIR_Nsos;
    float * HATS_InIIR_Hsos;

    /* Loudness scaling factors */
    float Sp;
    float Sl;

    /* Number of Bark bands at the given sample rate */
    int Nb;

    /* PESQ psychoacoustic model definitions */
    int *nr_of_hz_bands_per_bark_band;
    double *centre_of_band_bark;
    double *centre_of_band_hz;
    double *width_of_band_bark;
    double *width_of_band_hz;
    double *pow_dens_correction_factor;
    double *abs_thresh_power;

    FFT_t *pfft;
} PESQ_t;

PESQ_t *create_new_pesq_instance();
void free_pesq_instance(PESQ_t **pinst);

#endif /* PESQINSTANCE_INCLUDED_H */

/* END OF FILE */


