/***********************************************************
*
*  PESQ C library implementation 
*
*  pesq.h      General constants and declarations
*
*  PESQ-ITU ANSI C source code
*  Copyright (C) British Telecommunications plc, 2000.
*  Copyright (C) Royal KPN NV, 2000.
*
*  Additional modifications
*  Copyright (C) Psytechnics Limited, 2001-2006.
*  All rights reserved.
*
*  For more information
*  - visit our websites www.psytechnics.com or www.pesq.net
*
*  - write to us at
*      Psytechnics Limited, Fraser House
*      23 Museum Street, Ipswich IP1 1HN, United Kingdom
*
*  - e-mail us at  info@psytechnics.com
*
************************************************************
*
*   $Revision: 1.12 $
*
*	31/3/2006		Release 2.2
*	  Structure ERROR_INFO changed to include LV_PESQ_P862_2 
*
*	30/10/2004		Release 2.1
*	  Structure ERROR_INFO changed to iclude LV_PESQ_P862_1 
*	  parm_chk function removed
*	  Internal comment C2.1.2	
*
*   04/10/2002		Release 2.0
*     New functions for PESQ tools in new source file pesqdiag.c.
*     New members in err_info to support this.
*
*   14/11/2001		Release 1.4
*     New parameters in SIGNAL_INFO and ERROR_INFO for new
*     API extension; also new function pesq_level_spectra()
*     and calc_level_spectra().
*     New constants INVALID_DB, SAT_DB, Spect_calib_16k
*     and Spect_calib_8k for use in these functions.
*     New constants POWTARGET and POWTHRESH for regulating the
*     noise level with silent reference signals.
*
*   21/05/2001		Release 1.3
*
*   07/03/2001		Release 1.2
*
***********************************************************/

#ifndef PESQ_INCLUDED_H
#define PESQ_INCLUDED_H

#include <string.h>
#include <stdlib.h>
#include "pesqinstance.h"
#include "pesq.h"

/* Log file to which results are reported */
#define PESQ_LOG_FILE "pesqlog.txt"

/* True and false */
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

/* Maximum number of utterances used in time alignment */
#define MAXNUTTERANCES 50

/* Flag for crude time alignment of whole signal, as opposed to a given utterance */
#define WHOLE_SIGNAL -1

/* Length of time alignment filter */
#define LINIIR 60

/* Silence padding added to the signals for filtering */
#define DATAPADDING_MSECS 320

/* Padding added at start and end of signals for variable delay time alignment */
#define SEARCHBUFFER 75  

/* Small value used to regularise division and logs */
#define EPS 1E-12

/* Minimum length (in 4ms VAD frames) of a speech burst */
#define MINSPEECHLGTH 4

/* Length (in 4ms VAD frames) of longest intra-utterance silenc. */
#define JOINSPEECHLGTH 50

/* Minimum length (in 4ms VAD frames) of an utterance */
#define MINUTTLENGTH 50

/* 2 * pi */
#ifndef TWOPI
  #define TWOPI 6.28318530717959
#endif

/* Maximum length of a frame in the psychoacoustic model */
#define Nfmax 512

/* Level alignment calibrated level (power) */
#define TARGET_AVG_POWER    1E7

/* FFT Power spectrum and Zwicker loudness correction factors (8kHz) */
#define Sp_8k   2.764344e-5
#define Sl_8k   1.866055e-1

/* FFT Power spectrum and Zwicker loudness correction factors (16kHz) */
#define Sp_16k  6.910853e-006
#define Sl_16k  1.866055e-001

/* Zwicker powers used in loudness calculation */
#define gamma 0.001
#define ZWICKER_POWER       0.23 

/* Threshold for determining silence at start and end of files */
#define CRITERIUM_FOR_SILENCE_OF_5_SAMPLES 500.

/* Length of a speech burst (syllable) for temporal integration */
#define NUMBER_OF_FRAMES_PER_SYLLABLE       20

/* Number of bad intervals to process */
#define MAX_NUMBER_OF_BAD_INTERVALS        1000

/* Maximum and minimum scaling factors for gain correction */
#define MAX_SCALE   5.0
#define MIN_SCALE   3E-4

/* Noise addition thresholds to deal with silent references */
#define POWTARGET 10.0f
#define POWTHRESH 350.0f

/* Symmetric error threshold for bad interval identification */
#define THRESHOLD_BAD_FRAMES   30

/* Spreading range for bad interval identification */
#define SMEAR_RANGE 2

/* Minimum length of a bad interval */
#define MINIMUM_NUMBER_OF_BAD_FRAMES_IN_BAD_INTERVAL    5

/* Maximum delay correction (in number of frames) in bad interval realignment */
#define SEARCH_RANGE_IN_TRANSFORM_LENGTH    4

/* Non-linear averaging (Lp) power, symmetric disturbance:
   F   Integration over frequency
   S   Integration over speech bursts
   T   Integration over time */
#define D_POW_F 2
#define D_POW_S 6
#define D_POW_T 2

/* Non-linear averaging (Lp) power, asymmetric disturbance:
   F   Integration over frequency
   S   Integration over speech bursts
   T   Integration over time */
#define A_POW_F 1
#define A_POW_S 6
#define A_POW_T 2

/* Weight given to symmetric and asymmetric disturbance in
   subjective quality prediction */
#define D_WEIGHT 0.1
#define A_WEIGHT 0.0309

/* Flag to note that it has been impossible to calculate a level value */
#define INVALID_DB  -999.0f

/* Correction to make +/- 32767 equivalent to 0 dBov */
#define SAT_DB  -90.309f

/* FFT Power spectrum calibration factors */
#define Spect_calib_16k 1.8966776e-014
#define Spect_calib_8k  7.5867061e-014

/* Standard min and max operators */
#ifndef min
  #define min(a,b)  (((a) < (b)) ? (a) : (b))
#endif

#ifndef max
  #define max(a,b)  (((a) > (b)) ? (a) : (b))
#endif

/* The SIGNAL_INFO type is used to store various data items
   relating to the reference and degraded speech files, including
   pointers to the data structures used in processing. */
typedef struct {
  wchar_t  file_name [512];
  long  Nsamples;
  long  apply_swap;

  float * orig_data;
  float * data;
  float * VAD;
  float * logVAD;
  float Activity;
  float level_align_power;
} SIGNAL_INFO;

/* The ERROR_INFO type is used to store various data items relating to the
   errors between reference and degraded signals. */
typedef struct {
  long model;
  long rtn_bands;
  long tools;

  long Nutterances;
  long Largest_uttsize;

  long  Crude_DelayEst;
  float Crude_DelayConf;
  long  UttSearch_Start[MAXNUTTERANCES];
  long  UttSearch_End[MAXNUTTERANCES];
  long  Utt_DelayEst[MAXNUTTERANCES];
  long  Utt_Delay[MAXNUTTERANCES];
  float Utt_DelayConf[MAXNUTTERANCES];
  long  Utt_Start[MAXNUTTERANCES];
  long  Utt_End[MAXNUTTERANCES];

  long Nframes;
  long * FrameDelay;

  float pesq_score;
  float pesq_lq;
  float pesq_P862_1;
  float pesq_P862_2;
  float pesq_ie;

  /* PESQDLL inputs and outputs */
  float * LV_ref_data;
  long LV_ref_Nsamples;
  float * LV_deg_data;
  long LV_deg_Nsamples;
  long LV_Nframes;
  float * LV_ref_surf;
  float * LV_deg_surf;
  float * LV_ref_surf_preeq;
  float * LV_deg_surf_preeq;
  float * LV_Sframe_err;
  float * LV_Aframe_err;
  float * LV_PESQ_score;
  float * LV_Params;
  float * LV_TFE;
  long * LV_FrameDelay;
  float * LV_PESQ_LQ;
  float * LV_PESQ_P862_1;
  float * LV_PESQ_P862_2;
  float * LV_PESQ_Ie;
  float * LV_Level_Spectra;
  float * LV_Frame_score;
  long * LV_DlyHistCentres;
  long * LV_DlyHistCount;
  float * LV_Class_err;
  long * LV_Diagnosis;
  float * LV_Time_err;
  short * LV_Clipping_est;
  float * LV_Lin_Spectra;
  float * LV_Lin_tfe;
  float * LV_ref_spectrogram;
  float * LV_ref_lpc_spectrogram;
  float * LV_ref_excitation;
  float * LV_ref_speech_parms;
  float * LV_deg_spectrogram;
  float * LV_deg_lpc_spectrogram;
  float * LV_deg_excitation;
  float * LV_deg_speech_parms;


} ERROR_INFO;

/* Other variables declared in pesqpar.h */
#ifndef PESQ_GLOBAL_VARIABLES
#define PESQ_GLOBAL_VARIABLES

extern char * PESQ_Copyright;

extern const long Fs_8k;
extern const long Downsample_8k;
extern const long InIIR_Nsos_8k;
extern const float InIIR_Hsos_8k[LINIIR];
extern const long WB_InIIR_Nsos_8k;
extern const float WB_InIIR_Hsos_8k[LINIIR];
extern const long Align_Nfft_8k;

extern const long Fs_16k;
extern const long Downsample_16k;
extern const long InIIR_Nsos_16k;
extern const float InIIR_Hsos_16k[LINIIR];
extern const long WB_InIIR_Nsos_16k;
extern const float WB_InIIR_Hsos_16k[LINIIR];
extern const long Align_Nfft_16k;

extern int nr_of_hz_bands_per_bark_band_8k[42];
extern double centre_of_band_bark_8k[42];
extern double centre_of_band_hz_8k[42];
extern double width_of_band_bark_8k[42];
extern double width_of_band_hz_8k[42];
extern double pow_dens_correction_factor_8k[42];
extern double abs_thresh_power_8k[42];

extern int nr_of_hz_bands_per_bark_band_16k[49];
extern double centre_of_band_bark_16k[49];
extern double centre_of_band_hz_16k[49];
extern double width_of_band_bark_16k[49];
extern double width_of_band_hz_16k[49];
extern double pow_dens_correction_factor_16k[49];
extern double abs_thresh_power_16k[49];

extern double align_filter_dB [26][2];
extern double standard_IRS_filter_dB [26][2];

#endif

#ifndef PESQ_DECL 
  #define PESQ_DECL 
#endif

/* function definitions for pesqio.c */
PESQ_DECL void  select_rate(PESQ_t *inst, long sample_rate, long * Error_Flag, char ** Error_Type );
PESQ_DECL int   file_exist( char * fname );
////PESQ_DECL void  load_src(PESQ_t *inst, long * Error_Flag, char ** Error_Type, SIGNAL_INFO * sinfo);
PESQ_DECL void  alloc_other(PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info, 
        long * Error_Flag, char ** Error_Type, float ** ftmp);
PESQ_DECL void  load_src_from_alaw_wav(PESQ_t *inst, long * Error_Flag, char ** Error_Type, SIGNAL_INFO * sinfo);
PESQ_DECL void  load_files (PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
    ERROR_INFO * err_info, long * Error_Flag, char ** Error_Type);
PESQ_DECL void read_pcm_data(PESQ_t *inst, SIGNAL_INFO * sig_info, const float* pcm16BitSamples, int numPcmSamples, ERROR_INFO * err_info,
    long * Error_Flag,
    char ** Error_Type);

/* function definitions for pesqmod.c */
PESQ_DECL void  fix_power_level(PESQ_t *inst, SIGNAL_INFO *info, long maxNsamples );
PESQ_DECL void  pesq_measure_core (PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
        ERROR_INFO * err_info, long * Error_Flag, char ** Error_Type);
void pesq_measure (PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
    ERROR_INFO * err_info, long * Error_Flag, char ** Error_Type);
PESQ_DECL void  input_filter(PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info );
PESQ_DECL void  calc_VAD(PESQ_t *inst, SIGNAL_INFO * pinfo );
PESQ_DECL int   id_searchwindows(PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
        ERROR_INFO * err_info );
PESQ_DECL void  id_utterances(PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
        ERROR_INFO * err_info );
PESQ_DECL void  utterance_split(PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
        ERROR_INFO * err_info, float * ftmp );
PESQ_DECL void  utterance_locate(PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
        ERROR_INFO * err_info, float * ftmp );
PESQ_DECL void  short_term_fft(FFT_t *pfft, int Nf, SIGNAL_INFO *info, float *window,
        long start_sample, float *hz_spectrum, float *fft_tmp);
PESQ_DECL void  freq_warping(PESQ_t *inst, float *hz_spectrum, int Nb, float *pitch_pow_dens,
        long frame );
PESQ_DECL void  time_avg_audible_of(PESQ_t *inst, int number_of_frames, int *silent,
        float *pitch_pow_dens, float *avg_pitch_pow_dens,
        int total_number_of_frames);
PESQ_DECL void  freq_resp_compensation(PESQ_t *inst, int number_of_frames, float *pitch_pow_dens_ref,
        float *avg_pitch_pow_dens_ref, float *avg_pitch_pow_dens_deg,
        float constant);
PESQ_DECL void  intensity_warping_of(PESQ_t *inst, float *loudness_dens, int frame,
        float *pitch_pow_dens);
PESQ_DECL void  multiply_with_asymmetry_factor(PESQ_t *inst, float *disturbance_dens, int frame,
        const float * const pitch_pow_dens_ref,
        const float * const pitch_pow_dens_deg);
PESQ_DECL int   compute_delay(FFT_t *pfft, long start_sample, long stop_sample, long search_range,
        float *time_series1, float *time_series2, float *max_correlation);
PESQ_DECL float total_audible(PESQ_t *inst, int frame, float *pitch_pow_dens, float factor);
PESQ_DECL float pseudo_Lp(PESQ_t *inst, float *x, float p);
PESQ_DECL float Lpq_weight( int start_frame, int stop_frame, float power_syllable,
        float power_time, float *frame_disturbance, float *time_weight);
PESQ_DECL double pow_of( const float * const x, long start_sample, long stop_sample,
        long divisor);
PESQ_DECL void pesq_psychoacoustic_model(PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
        ERROR_INFO * err_info);

/* function definitions for pesqdsp.c */
PESQ_DECL void  DC_block(PESQ_t *inst, float * data, long Nsamples );
PESQ_DECL void  apply_iir_filter(PESQ_t *inst,  float * data, long Nsamples );
PESQ_DECL float interpolate( float freq, double filter_curve_db [][2],
        int number_of_points);
PESQ_DECL void  apply_fft_filter(PESQ_t *inst, float * data, long Nsamples, int, double [][2] );
PESQ_DECL void  apply_VAD(PESQ_t *inst, SIGNAL_INFO * pinfo, float * data,
        float * VAD, float * logVAD );
PESQ_DECL void  crude_align(PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
        ERROR_INFO * err_info, long Utt_id, float * ftmp);
PESQ_DECL void time_align(PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
        ERROR_INFO * err_info, long Utt_id, float * ftmp );
PESQ_DECL void split_align(PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
       ERROR_INFO * err_info, float * ftmp,
       long Utt_Start, long Utt_SpeechStart, long Utt_SpeechEnd, long Utt_End,
       long Utt_DelayEst, float Utt_DelayConf, long * Best_ED1, long * Best_D1,
       float * Best_DC1, long * Best_ED2, long * Best_D2, float * Best_DC2,
       long * Best_BP );

#endif /* PESQ_INCLUDED_H */

/* END OF FILE */

