/***********************************************************
*
*  PESQ C library implementation 
*
*  dsp.c      Low-level general signal processing routines
*
*  PESQ-ITU ANSI C source code
*  Copyright (C) British Telecommunications plc, 2000.
*  Copyright (C) Royal KPN NV, 2000.
*
*  Additional modifications
*  Copyright (C) Psytechnics Limited, 2001-2006.
*  All rights reserved
*
*  For more information
*  - visit our websites www.psytechnics.com or www.pesq.net
*
*  - write to us at
*      Psytechnics Limited, Fraser House
*      23 Museum Street, Ipswich IP1 1HN, United Kingdom
*
*  - e-mail us at  info@psytechnics.com
*
************************************************************
*
*  Functions contained:
*    safe_malloc
*    safe_free
*    malloc_check
*    nextpow2
*    ispow2
*    intlog2
*    FFTInit
*    FFTFree
*    FFT
*    IFFT
*    RealFFT
*    RealIFFT
*    FFTNXCorr
*    IIRsos
*    IIRFilt
*    FIRFilt
*
************************************************************
*
*  Version control
*   $Revision: 1.11 $
*
*	31/3/2006		Release 2.2
*
*	30/10/2004		Release 2.1
*
*   04/10/2002		Release 2.0
*      Included new function FIRFilt().
*
*   14/11/2001		Release 1.4
*
*   21/05/2001		Release 1.3
*     Replaced certain calls to malloc() with safe_malloc(),
*     and calls to to free() with safe_free(), for consistency.
*
*   07/03/2001		Release 1.2
*
***********************************************************/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "fftinstance.h"
#include "dsp.h"

#ifdef USE_SPLIB
/* Conditional defines and include to interface to the Intel PLSuite SPLib
   signal processing routines; tested with SPLib release 4.2. */
#define nsp_UsesTransform
#define nsp_UsesIir
#include "nsp.h"
#endif

#ifndef TWOPI
  #define TWOPI   6.283185307179586f
#endif

unsigned long total_malloced = 0;
long malloc_count = 0;

/************************************************************
*   safe_malloc
*
*   Syntax        void *safe_malloc (unsigned long size)
*
*   Description
*   Allocates memory and, if MALLOC_TRACK defined, keeps track of it in malloc.txt
*
*   Inputs        size        size of memory allocation        
*
*   Modifies    None
*
*   Returns        Void pointer to allocated memory
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - JM
************************************************************/
PESQ_DECL void *safe_malloc (unsigned long size) {

#ifdef MALLOC_TRACK
    /* Memory allocation with logging */

    unsigned long *result;
    FILE * malloc_log = fopen("malloc.txt", "at");
    total_malloced += size;
    malloc_count++;
    result = (unsigned long *)malloc (4 + size);
    if (result == NULL) {
        printf ("malloc failed!\n");
    }
    fprintf(malloc_log, "MALLOC\t%d\t%d\t%x\n", total_malloced, size, (unsigned long)(result + 1));
    fclose(malloc_log);
    result[0] = size;
    return (void *)(result + 1);

#else
    /* Standard malloc with minimal logging */

    void *result;
    total_malloced += size;
    malloc_count++;
    result = malloc (size);
    if (result == NULL) {
        printf ("malloc failed!\n");
    }
    return result;

#endif
}


/************************************************************
*   safe_free
*
*   Syntax        void safe_free (void *p)
*
*   Description
*   Frees memory and, if MALLOC_TRACK defined, keeps track of it in malloc.txt.
*
*   Inputs        p        
*
*   Modifies    None
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - JM
************************************************************/
PESQ_DECL void safe_free (void *p) {
#ifdef MALLOC_TRACK
    /* Free allocated memory with logging */

    unsigned long * r = (unsigned long *)p;
    FILE * malloc_log = fopen("malloc.txt", "at");
    if( p != NULL ) {
        malloc_count--;
        total_malloced -= r[-1];
        fprintf(malloc_log, "FREE\t%d\t%d\t%x\n", total_malloced, r[-1], (unsigned long)(p));
        free ((void *)(r - 1));
    }
    fclose(malloc_log);

#else
    /* Standard free */

    if( p != NULL ) {
        free (p);
        malloc_count--;
    }

#endif
}


/************************************************************
*   malloc_check
*
*   Syntax        void malloc_check(void)
*
*   Description
*   Checks total allocated memory
*
*   Inputs        None        
*
*   Modifies    None
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - JM
************************************************************/
PESQ_DECL void malloc_check(void) {
    if( malloc_count > 0 ) printf("MEMCHK! Some memory is still allocated\n");
    else if( malloc_count < 0 ) printf("MEMCHK! Too many calls to safe_free()\n");
    printf("MEMALLOC\t%ld\n", total_malloced);
}


/************************************************************
*   nextpow2
*
*   Syntax        unsigned long nextpow2(unsigned long X) 
*
*   Description
*   Returns the next power of 2 greater than X,
*   or equal to X if X is a power of 2.
*
*   Inputs        x        X value    
*
*   Modifies    None
*
*   Returns        Next power of 2
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - JM
************************************************************/
PESQ_DECL unsigned long nextpow2(unsigned long X)
{
    unsigned long C = 1;
    while( (C < ULONG_MAX) && (C < X) )
        C <<= 1;

    return C;
}


/************************************************************
*   ispow2
*
*   Syntax        int ispow2(unsigned long X) 
*
*   Description
*    Determines if X is a power of 2
*
*   Inputs        x        X value    
*
*   Modifies    None
*
*   Returns        Non-zero if X is a power of 2, 0 otherwise.
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - JM
************************************************************/
PESQ_DECL int ispow2(unsigned long X)
{
    unsigned long C = 1;
    while( (C < ULONG_MAX) && (C < X) )
        C <<= 1;
        
    return (C == X);
}


/************************************************************
*   intlog2
*
*   Syntax        int intlog2(unsigned long X) 
*
*   Description
*    Calculates log of X to the power of 2, rounded to the 
*    nearest interger
*
*   Inputs        x        X value    
*
*   Modifies    None
*
*   Returns        Returns log of X to the power of 2, rounded 
*                to the nearest integer.
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - JM
************************************************************/
PESQ_DECL int intlog2(unsigned long X)
{
#ifdef __WATCOMC__
    return (int)floor( log2( 1.0 * X ) + 0.5 );
#else
    int L2 = 0;
    X = (unsigned long)(X * 1.414213562373);
    while( X > 1 ) {
        X >>= 1;
        L2++;
    }
    return L2;
#endif
}


/************************************************************
*   FFTInit
*
*   Syntax        void FFTInit(unsigned long N) 
*
*   Description
*    Initialises arrays FFTButter, FFTPhi and FFTBitSwap, 
*    which are global to the module dsp.c, for use by FFT.
*
*   Inputs        N        size of FFT, required to be a power of 2    
*
*   Modifies    FFTButter, FFTPhi, FFTSwapInitialised, FFTBItSwap
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - JM
************************************************************/
PESQ_DECL void FFTInit(FFT_t *pfft, unsigned long N)
{
    unsigned long   C, L, K;
    float           Theta;
    float         * PFFTPhi;
    
    /* Check and clear if space already allocated */
    if( (pfft->FFTSwapInitialised != N) && (pfft->FFTSwapInitialised != 0) )
        FFTFree(pfft);

    if( pfft->FFTSwapInitialised == N )
    {
        return;
    }
    else
    {
        /* Compute log to base 2 */
        C = N;
        for( pfft->FFTLog2N = 0; C > 1; C >>= 1 )
            pfft->FFTLog2N++;

        /* Check that N is a power of 2 */
        C = 1;
        C <<= pfft->FFTLog2N;
        if( N == C )
            pfft->FFTSwapInitialised = N;
        else return;
        /* In this case a memory exception will be raised if a call to
           any FFT routine is made. */

        /* Allocate space */
        pfft->FFTButter = (unsigned long *) safe_malloc( sizeof(unsigned long) * (N >> 1) );
        pfft->FFTBitSwap = (unsigned long *) safe_malloc( sizeof(unsigned long) * N );
        pfft->FFTPhi = (float *) safe_malloc( 2 * sizeof(float) * (N >> 1) );
    
        /* Initialise complex angles */
        PFFTPhi = pfft->FFTPhi;
        for( C = 0; C < (N >> 1); C++ )
        {
            Theta = (TWOPI * C) / N;
            (*(PFFTPhi++)) = (float)cos( Theta );
            (*(PFFTPhi++)) = (float)sin( Theta );
        }
    
        /* Initialise array containing bit-reversed indices */
        pfft->FFTButter[0] = 0;
        L = 1;
        K = N >> 2;
        while( K >= 1 )
        {
            for( C = 0; C < L; C++ )
                pfft->FFTButter[C+L] = pfft->FFTButter[C] + K;
            L <<= 1;
            K >>= 1;
        }
    }
}


/************************************************************
*   FFTFree
*
*   Syntax        void FFTFree(void) 
*
*   Description
*    Frees FFT memory and sets FFTSwapInitialised to zero.
*
*   Inputs        N        size of FFT, required to be a power of 2    
*
*   Modifies    FFTButter, FFTPhi, FFTSwapInitialised, FFTBItSwap
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - JM
************************************************************/
PESQ_DECL void FFTFree(FFT_t *pfft)
{
    if( pfft->FFTSwapInitialised != 0 )
    {
        safe_free( pfft->FFTButter );
        safe_free( pfft->FFTBitSwap );
        safe_free( pfft->FFTPhi );
        pfft->FFTSwapInitialised = 0;
    }
}


/************************************************************
*   FFT
*
*   Syntax        void FFT(float * x, unsigned long N) 
*
*   Description
*    Performs the Fast Fourier Transform of data in x, which 
*    is returned in situ in order of increasing frequency
*
*   Inputs        x        x should contain N complex pairs, real part first
*                N        size of FFT, must be a power of 2    
*
*   Modifies    x, FFTButter, FFTPhi, FFTSwapInitialised, FFTBitSwap
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - JM
************************************************************/
PESQ_DECL void FFT(FFT_t *pfft, float * x, unsigned long N)
{
#ifdef USE_SPLIB
    if( (!ispow2(N)) || (N < 2) ) return;
    nspcFft( (SCplx *)x, intlog2(N), NSP_Forw | NSP_NoBitRev );
#else
    unsigned long   Cycle, C, S, NC;
    unsigned long   Step    = N >> 1;
    unsigned long   K1, K2;
    register float  R1, I1, R2, I2;
    float           ReFFTPhi, ImFFTPhi;

    /* If N = 1 there is nothing to do and FFTInit must not be called */
    if( N > 1 )
    {
        /* Initialise the arrays */
        FFTInit(pfft, N );
    
        /* Perform FFT returning frequency points in bit-swapped
           order - they are then sorted at the end. */
        for( Cycle = 1; Cycle < N; Cycle <<= 1, Step >>= 1 )
        {
            /* K1 and K2 point to the two complex pairs that
               are processed at each step. */
            K1 = 0;
            K2 = Step << 1;
    
            for( C = 0; C < Cycle; C++ )
            {
                if( C == 0 )
                {
                    /* This special case needs no multiplying as
                       the value of Theta is exactly zero. */
                    for( S = 0; S < Step; S++ )
                    {
                        R1 = x[K1];
                        I1 = x[K1+1];
                        R2 = x[K2];
                        I2 = x[K2+1];
                        
                        x[K1++] = R1 + R2;
                        x[K1++] = I1 + I2;
                        x[K2++] = R1 - R2;
                        x[K2++] = I1 - I2;
                    }
                }
                else if( C == 1 )
                    /* This case also needs no multiplying as
                       Theta is pi/2, a rotation of 90 degrees. */
                    for( S = 0; S < Step; S++ )
                    {
                        R1 = x[K1];
                        I1 = x[K1+1];
                        R2 = x[K2];
                        I2 = x[K2+1];
                        
                        x[K1++] = R1 + I2;
                        x[K1++] = I1 - R2;
                        x[K2++] = R1 - I2;
                        x[K2++] = I1 + R2;
                    }
                else
                {
                    NC = pfft->FFTButter[C] << 1;
                    ReFFTPhi = pfft->FFTPhi[NC];
                    ImFFTPhi = pfft->FFTPhi[NC+1];
                    for( S = 0; S < Step; S++ )
                    {
                        R1 = x[K1];
                        I1 = x[K1+1];
                        R2 = x[K2];
                        I2 = x[K2+1];
                        
                        x[K1++] = R1 + ReFFTPhi * R2 + ImFFTPhi * I2;
                        x[K1++] = I1 - ImFFTPhi * R2 + ReFFTPhi * I2;
                        x[K2++] = R1 - ReFFTPhi * R2 - ImFFTPhi * I2;
                        x[K2++] = I1 + ImFFTPhi * R2 - ReFFTPhi * I2;
                    }
                }
    
                /* Update K1 and K2 for the next cycle of C. */
                K1 = K2;
                K2 = K1 + (Step << 1);
            }
        }
    
        /* Untangle bit-reversal */
        NC = N >> 1;
        for( C = 0; C < NC; C++ )
        {
            pfft->FFTBitSwap[C] = pfft->FFTButter[C] << 1;
            pfft->FFTBitSwap[C+NC] = 1 + pfft->FFTBitSwap[C];
        }
        for( C = 0; C < N; C++ )
            if( (S = pfft->FFTBitSwap[C]) != C )
            {
                pfft->FFTBitSwap[S] = S;     /* Ensures that only one swap is made */
    
                K1 = C << 1;
                K2 = S << 1;
                R1 = x[K1];
                x[K1++] = x[K2];
                x[K2++] = R1;
                R1 = x[K1];
                x[K1] = x[K2];
                x[K2] = R1;
            }
        /* FFT Completed. */
    }
#endif
}


/************************************************************
*   IFFT
*
*   Syntax        void IFFT(float * x, unsigned long N) 
*
*   Description
*    Performs the Inverse Fast Fourier Transform of data in x, which 
*    is returned in situ in order of increasing time
*
*   Inputs        x        x should contain N complex pairs, real part first
*                N        size of FFT, must be a power of 2    
*
*   Modifies    x, FFTButter, FFTPhi, FFTSwapInitialised, FFTBitSwap
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - JM
************************************************************/
PESQ_DECL void IFFT(FFT_t *pfft, float * x, unsigned long N)
{
#ifdef USE_SPLIB
    if( (!ispow2(N)) || (N < 2) ) return;
    nspcFft( (SCplx *)x, intlog2(N), NSP_Inv | NSP_NoBitRev );
#else
    unsigned long   Cycle, C, S, NC;
    unsigned long   Step    = N >> 1;
    unsigned long   K1, K2;
    register float  R1, I1, R2, I2;
    float           ReFFTPhi, ImFFTPhi;

    /* If N = 1 there is nothing to do and FFTInit must not be called */
    if( N > 1 )
    {
        /* Initialise the arrays */
        FFTInit(pfft, N );
    
        /* Perform IFFT returning frequency points in bit-swapped
           order - they are then sorted at the end. */
        for( Cycle = 1; Cycle < N; Cycle <<= 1, Step >>= 1 )
        {
            /* K1 and K2 point to the two complex pairs that
               are processed at each step. */
            K1 = 0;
            K2 = Step << 1;
    
            for( C = 0; C < Cycle; C++ )
            {
                if( C == 0 )
                {
                    /* This special case needs no multiplying as
                       the value of Theta is exactly zero. */
                    for( S = 0; S < Step; S++ )
                    {
                        R1 = x[K1];
                        I1 = x[K1+1];
                        R2 = x[K2];
                        I2 = x[K2+1];
                        
                        x[K1++] = R1 + R2;
                        x[K1++] = I1 + I2;
                        x[K2++] = R1 - R2;
                        x[K2++] = I1 - I2;
                    }
                }
                else if( C == 1 )
                    /* This case also needs no multiplying as
                       Theta is pi/2, a rotation of 90 degrees. */
                    for( S = 0; S < Step; S++ )
                    {
                        R1 = x[K1];
                        I1 = x[K1+1];
                        R2 = x[K2];
                        I2 = x[K2+1];
                        
                        x[K1++] = R1 - I2;
                        x[K1++] = I1 + R2;
                        x[K2++] = R1 + I2;
                        x[K2++] = I1 - R2;
                    }
                else
                {
                    NC = pfft->FFTButter[C] << 1;
                    ReFFTPhi = pfft->FFTPhi[NC];
                    ImFFTPhi = pfft->FFTPhi[NC+1];
                    for( S = 0; S < Step; S++ )
                    {
                        R1 = x[K1];
                        I1 = x[K1+1];
                        R2 = x[K2];
                        I2 = x[K2+1];
                        
                        x[K1++] = R1 + ReFFTPhi * R2 - ImFFTPhi * I2;
                        x[K1++] = I1 + ImFFTPhi * R2 + ReFFTPhi * I2;
                        x[K2++] = R1 - ReFFTPhi * R2 + ImFFTPhi * I2;
                        x[K2++] = I1 - ImFFTPhi * R2 - ReFFTPhi * I2;
                    }
                }
    
                /* Update K1 and K2 for the next cycle of C. */
                K1 = K2;
                K2 = K1 + (Step << 1);
            }
        }
    
        /* Untangle bit-reversal */
        NC = N >> 1;
        for( C = 0; C < NC; C++ )
        {
            pfft->FFTBitSwap[C] = pfft->FFTButter[C] << 1;
            pfft->FFTBitSwap[C+NC] = 1 + pfft->FFTBitSwap[C];
        }
        for( C = 0; C < N; C++ )
            if( (S = pfft->FFTBitSwap[C]) != C )
            {
                pfft->FFTBitSwap[S] = S;     /* Ensures that only one swap is made */
                K1 = C << 1;
                K2 = S << 1;
                R1 = x[K1];
                x[K1++] = x[K2];
                x[K2++] = R1;
                R1 = x[K1];
                x[K1] = x[K2];
                x[K2] = R1;
            }
    
        /* Divide through by N. */
        NC = N << 1;
        for( C = 0; C < NC; )
            x[C++] /= N;
    
        /* IFFT Completed. */
    }
#endif
}


/************************************************************
*   RealFFT
*
*   Syntax        void RealFFT(float * xr, unsigned long N) 
*
*   Description
*    Computes the FFT of a real signal.
*    The FFT of an N-point real signal can be fully represented
*   by N/2+1 frequency points, and takes approximately half the
*   time to calculate as an N-point complex FFT. Performs FFT on
*    xr in situ, Or calls SPLIB if so specified
*
*   Inputs            xr        array containing signal to be FFT
*                    N        length of xr in samples
*
*   Modifies        xr
*
*   Returns            None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - JM
************************************************************/
PESQ_DECL void RealFFT(FFT_t *pfft, float *xr, unsigned long N) 
{
#ifdef USE_SPLIB
//    FILE * fid = fopen("dsplog.txt", "at");
//    fprintf(fid, "RealFFT\t%d\n", N);
//    fclose(fid);
    if( (!ispow2(N)) || (N < 4) ) return;
    nspsRealFft( xr, intlog2(N), NSP_Forw | NSP_NoBitRev );
#else
    /* Real FFT algorithm is based on the complex FFT, using the
       even coefficients as real values and odd coefficients as
       imaginary values.  A correction is then applied to map to
       the "true" FFT of the data. */
    register float RHn, IHn, RHN_n, IHN_n;
    float c0 = (float)cos(6.283185307179586 / (double)N);
    float s0 = (float)sin(6.283185307179586 / (double)N);
    float c, s, sp1, sm1;
    unsigned long C;
    int iseven = 0;

    /* If N < 4 there is nothing to do and FFTInit must not be called */
    if( N < 4 ) return;

    /* Ensure that twiddle factors (exp j Phi) are initialised */
    FFTInit(pfft, N/2 );

    /* Forward FFT of the "complex" signal, to be corrected below */
    FFT(pfft, xr, N/2 );

    /* Elements for n = 0, N/2 */
    xr[N] = xr[0] - xr[1];
    xr[N+1] = 0.0f;
    xr[0] = xr[0] + xr[1];
    xr[1] = 0.0f;

    /* n = 1 .. N/4-1 and n = N/2-1 .. N/4+1 */
    for( C = 1; C <= (N/4); C++ ) {
        /* Twiddle factors (exp j Phi) */
        if( iseven ) {
            iseven = 0;
            c = pfft->FFTPhi[C];
            sp1 = 1.0f - pfft->FFTPhi[C+1];
            sm1 = -1.0f - pfft->FFTPhi[C+1];
        } else {
            iseven = 1;
            c = pfft->FFTPhi[C-1] * c0 - pfft->FFTPhi[C] * s0;
            s = pfft->FFTPhi[C-1] * s0 + pfft->FFTPhi[C] * c0;
            sp1 = 1.0f - s;
            sm1 = -1.0f - s;
        }

        /* H(n) */
        RHn = 0.5f * xr[C << 1];
        IHn = 0.5f * xr[(C << 1) + 1];

        /* H(N/2 - n) */
        RHN_n = 0.5f * xr[N - (C << 1)];
        IHN_n = 0.5f * xr[N - (C << 1) + 1];

        /* Write XR(n) */
        xr[C << 1] = RHn * sp1 - RHN_n * sm1 + IHn * c + IHN_n * c;
        xr[(C << 1) + 1] = IHn * sp1 + IHN_n * sm1 - RHn * c + RHN_n * c;

        /* Write XR(N/2-n) */
        xr[N - (C << 1)] = RHN_n * sp1 - RHn * sm1 - IHN_n * c - IHn * c;
        xr[N - (C << 1) + 1] = IHN_n * sp1 + IHn * sm1 + RHN_n * c - RHn * c;
    }
#endif
}


/************************************************************
*   RealIFFT
*
*   Syntax        void RealIFFT(float * xr, unsigned long N) 
*
*   Description
*    Computes the Inverse FFT of a real signal.
*    The FFT of an N-point real signal can be fully represented
*   by N/2+1 frequency points, and takes approximately half the
*   time to calculate as an N-point complex FFT. Performs IFFT on
*    xr in situ, Or calls SPLIB if so specified.
*
*   Inputs        xr        array containing signal to be IFFT
*                N        length of xr in samples
*
*   Modifies    xr
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - JM
************************************************************/
PESQ_DECL void RealIFFT(FFT_t *pfft, float *xr, unsigned long N)
{
#ifdef USE_SPLIB
//    FILE * fid = fopen("dsplog.txt", "at");
//    fprintf(fid, "RealIFFT\t%d\n", N);
//    fclose(fid);
    if( (!ispow2(N)) || (N < 4) ) return;
    nspsCcsFft( xr, intlog2(N), NSP_Inv | NSP_NoBitRev );
#else
    /* Maps the input into a format that can then be inverted using the
       complex FFT of N/2 complex data points. */
    register float RXn, IXn, RXN_n, IXN_n;
    float c0 = (float)cos(6.283185307179586 / (double)N);
    float s0 = (float)sin(6.283185307179586 / (double)N);
    float c, s, sp1, sm1;
    unsigned long C;
    int iseven = 0;

    /* If N < 4 there is nothing to do and FFTInit must not be called */
    if( N < 4 ) return;

    /* Ensure that twiddle factors (exp j Phi) are initialised */
    FFTInit(pfft, N/2 );

    /* Special case for n = 0 */
    xr[1] = 0.5f * (xr[0] - xr[N]);
    xr[0] = 0.5f * (xr[0] + xr[N]);

    /* n = 1 .. N/4-1 and n = N/2-1 .. N/4+1 */
    for( C = 1; C <= (N/4); C++ ) {
        /* Twiddle factors */
        if( iseven ) {
            iseven = 0;
            c = pfft->FFTPhi[C];
            sp1 = 1.0f + pfft->FFTPhi[C+1];
            sm1 = -1.0f + pfft->FFTPhi[C+1];
        } else {
            iseven = 1;
            c = pfft->FFTPhi[C-1] * c0 - pfft->FFTPhi[C] * s0;
            s = pfft->FFTPhi[C-1] * s0 + pfft->FFTPhi[C] * c0;
            sp1 = 1.0f + s;
            sm1 = -1.0f + s;
        }

        /* XR(n) */
        RXn = 0.5f * xr[C << 1];
        IXn = 0.5f * xr[(C << 1) + 1];

        /* XR(N/2 - n) */
        RXN_n = 0.5f * xr[N - (C << 1)];
        IXN_n = 0.5f * xr[N - (C << 1) + 1];

        /* Write H(n) */
        xr[C << 1] = RXN_n * sp1 - RXn * sm1 - IXN_n * c - IXn * c;
        xr[(C << 1) + 1] = RXn * c - IXN_n * sp1 - IXn * sm1 - RXN_n * c;

        /* Write H(N/2-n) */
        xr[N - (C << 1)] = RXn * sp1 - RXN_n * sm1 + IXn * c + IXN_n * c;
        xr[N - (C << 1) + 1] = RXn * c - RXN_n * c - IXn * sp1 - IXN_n * sm1 ;
    }

    /* Inverse FFT of the "complex" signal */
    IFFT(pfft, xr, N/2 );
#endif
}

/************************************************************
*   FFTNXCorr
*
*   Syntax        unsigned long FFTNXCorr( 
*                    float * x1, unsigned long n1,
*                    float * x2, unsigned long n2,
*                    float * y ) 
*
*   Description
*    Performs cross-correlation using FFT. The cross-correlation 
*    product is placed in y
*
*   Inputs        x1        vector input 1
*                x2        vector input 2
*                n1        length of x1 in samples
*                n2        length of x2 in samples
*                y        memory (allocated to store at least 
*                        n1 + n2 - 1 samples) to receive 
*                        cross-correlation product    
*
*   Modifies    y
*
*   Returns        Length of correlation product (n1 + n2 - 1)
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - JM
************************************************************/
PESQ_DECL unsigned long FFTNXCorr(FFT_t *pfft, 
  float * x1, unsigned long n1,
  float * x2, unsigned long n2,
  float * y )
{
    register float  r1, i1;
    float         * tmp1;
    float         * tmp2;
    unsigned long   C, D, Nx, Ny;

    /* Allocate temporary storage */
    Nx = nextpow2( max(n1, n2) );
    tmp1 = (float *) safe_malloc( sizeof(float) * 2 * (Nx + 1) );
    tmp2 = (float *) safe_malloc( sizeof(float) * 2 * (Nx + 1) );

    /* Copy data, pad with zeros, and perform FFTs. */
    for( C = 0; C < n1; C++ )
        tmp1[C] = x1[n1-1-C];   /* Time-reversal implemented here */
    for( C = n1; C < 2*Nx; C++ )
        tmp1[C] = 0.0;
    RealFFT(pfft, tmp1, 2*Nx );
    
    for( C = 0; C < n2; C++ )
        tmp2[C] = x2[C];
    for( C = n2; C < 2*Nx; C++ )
        tmp2[C] = 0.0;
    RealFFT(pfft, tmp2, 2*Nx );

    /* Convolve in frequency domain. */
    for( C = 0; C <= Nx; C++ )
    {
        D = C << 1; r1 = tmp1[D]; i1 = tmp1[1 + D];
        tmp1[D] = r1 * tmp2[D] - i1 * tmp2[1 + D];
        tmp1[1 + D] = r1 * tmp2[1 + D] + i1 * tmp2[D];
    }

    /* Inverse FFT and copy to position. */
    RealIFFT(pfft, tmp1, 2*Nx );
    Ny = n1 + n2 - 1;
    for( C = 0; C < Ny; C++ )
        y[C] = tmp1[C];
    
    /* Free up temporary memory. */
    safe_free( tmp1 );
    safe_free( tmp2 );

    return Ny;
}

/************************************************************
*   IIRsos
*
*   Syntax        void IIRsos(
*                    float * x, unsigned long Nx,
*                    float b0, float b1, float b2, float a1, float a2,
*                    float * tz1, float * tz2 ) 
*
*   Description
*    Applies a fast second-order section IIR filter to x. The 
*    filter applied is defined in Z-transformation notation as 
*    (b0 z^2 + b1 z + b0)/(z^2 + a1 z + a2). Filtering is performed
*    and returned in x.
*
*   Inputs        x        signal
*                Nx        number of samples in x to filter
*                b0        filter coefficient
*                b1        filter coefficient
*                b2        filter coefficient
*                a1        filter coefficient
*                a2        filter coefficient
*                tz1        IIR internal filter state
*                tz2        IIR internal filter state    
*
*   Modifies    tz1, tz2,x
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - JM
************************************************************/
PESQ_DECL void IIRsos(
    float * x, unsigned long Nx,
    float b0, float b1, float b2, float a1, float a2,
    float * tz1, float * tz2 )
{
    /* Registers */
    register float z0;
    register float z1;
    register float z2;

    /* Incorporate initial conditions */
    if( tz1 == NULL ) z1 = 0.0f; else z1 = *tz1;
    if( tz2 == NULL ) z2 = 0.0f; else z2 = *tz2;
    
    /* Implement the IIR filter in situ, optimising for redundant taps */
    if( (a1 != 0.0f) || (a2 != 0.0f) )
    {
        /* Use the a taps */
        if( (b1 != 0.0f) || (b2 != 0.0f) )
        {
            /* Implement the full filter */
            while( (Nx--) > 0 )
            {
                z0 = (*x) - a1 * z1 - a2 * z2;
                *(x++) = b0 * z0 + b1 * z1 + b2 * z2;
                z2 = z1;
                z1 = z0;
            }
        }
        else
        {
            if( b0 != 1.0f )
            {
                /* Omit taps b1 and b2 only */
                while( (Nx--) > 0 )
                {
                    z0 = (*x) - a1 * z1 - a2 * z2;
                    *(x++) = b0 * z0;
                    z2 = z1;
                    z1 = z0;
                }
            }
            else
            {
                /* Omit all b taps */
                while( (Nx--) > 0 )
                {
                    z0 = (*x) - a1 * z1 - a2 * z2;
                    *(x++) = z0;
                    z2 = z1;
                    z1 = z0;
                }
            }
        }
    }
    else
    {
        /* Omit the a taps */
        if( (b1 != 0.0f) || (b2 != 0.0f) )
        {
            /* Implement the filter without the a taps */
            while( (Nx--) > 0 )
            {
                z0 = (*x);
                *(x++) = b0 * z0 + b1 * z1 + b2 * z2;
                z2 = z1;
                z1 = z0;
            }
        }
        else
        {
            /* Omit taps a1, a2, b1 and b2 */
            if( b0 != 1.0f )
            {
                /* Implement the filter with b0 only */
                while( (Nx--) > 0 )
                {
                    *x = b0 * (*x);
                    x++;
                }
            }
            /* Otherwise a1=a2=b1=b2=0 and b0=1 so there is nothing to do! */
        }
    }

    /*  */
    if( tz1 != NULL ) (*tz1) = z1;
    if( tz2 != NULL ) (*tz2) = z2;
}

/************************************************************
*   IIRFilt
*
*   Syntax        void IIRFilt(
*                    float * h, unsigned long Nsos, float * z,
*                    float * x, unsigned long Nx, float * y )
*
*   Description
*    Performs cascaded second order sections IIR filter.
*
*   Inputs        h        array containing second order sections 
*                        in order b0, b1, b2, a1, a2    
*                Nsos    number of second order sections
*                z        initial IIR register states, two per second 
*                        order section
*                x        signal
*                Nx        number of samples in x to filter
*                y
*
*   Modifies    x, y, z
*
*   Returns    None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - JM
************************************************************/
PESQ_DECL void IIRFilt(
    const float * h, unsigned long Nsos, float * z,
    float * x, unsigned long Nx, float * y )
{
#ifdef USE_SPLIB
    unsigned long C;
    NSPIirTapState tapState;
    NSPIirDlyState dlyState;

    /* Buffer to translate taps into SPLib's format */
    float tapbuf[120];
    float * taps = tapbuf;

    /* Delay line */
    float zbuf[40];
    float * zs = zbuf;

    if( Nsos > 20 ) taps = (float *)safe_malloc( sizeof(float) * 6 * Nsos );
    if( (Nsos > 20) && (z == NULL) ) zs = (float *)safe_malloc( sizeof(float) * 2 * Nsos );
    
    /* Translate taps */
    for( C = 0; C < Nsos; C++ ) {
        taps[C*6] = h[C*5];
        taps[C*6+1] = h[C*5+1];
        taps[C*6+2] = h[C*5+2];
        taps[C*6+3] = 1.0f;
        taps[C*6+4] = h[C*5+3];
        taps[C*6+5] = h[C*5+4];
    }

    /* Initialise taps and delay line */
    nspsIirlInitBq( NSP_IirDefault, taps, Nsos, &tapState );
    if( z == NULL )
        nspsIirlInitDlyl( &tapState, zs, &dlyState );
    else
        dlyState.dlyl = z;

    /* Filter */
    if( y == NULL )
        y = x;
    nspsbIirl( &tapState, &dlyState, x, y, Nx );

    /* Free buffers if required */
    if( Nsos > 20 ) safe_free( taps );
    if( (Nsos > 20) && (z == NULL) ) safe_free( zs );
#else
    unsigned long C;

    if( y == NULL )
        y = x;
    else
    {
        /* Copy input data to output for processing in situ */
        for( C = 0; C < Nx; C++ )
            y[C] = x[C];
    }

    /* Apply second order sections */
    for( C = 0; C < Nsos; C++ )
    {
        if( z != NULL )
        {
            IIRsos( y, Nx, h[0], h[1], h[2], h[3], h[4], z, z+1 );
            z += 2;
        }
        else
            IIRsos( y, Nx, h[0], h[1], h[2], h[3], h[4], NULL, NULL );
        h += 5;
    }
#endif
}

/************************************************************
*   FIRFilt
*
*   Syntax   int FIRFilt(
*       float * h, unsigned long Nh, float * z,
*       float * x, unsigned long Nx, float * y )
*
*   Description
*   Performs simple FIR filtering.
*   z should be initially filled with zeros; if not required it
*   may be set to NULL.
*
*   Inputs
*            h        FIR impulse response
*            Nh         length of h
*           z       filter memory (also of length Nh)
*            x        input signal
*            Nx         length of x
*            y        output signal
*
*   Modifies
*            y
*
*   Returns        
*       1 if successful, or 0 if a failure occurred.
*
*   History
*   Created       26/9/2002    - PB
*   Documented    26/9/2002    - PB
*   Updated       04/11/2002 - AR
************************************************************/
PESQ_DECL int FIRFilt(
    float * h, unsigned long Nh, float * z,
    float * x, unsigned long Nx, float * y )
{
    float * mem;
    register float sum;
    unsigned long i, j;

    /* Error checks */
    if( (Nh < 1) || (Nx < 1) ) return 0;

    /* Use filter history, creating and initialising if required. */
    if( z == NULL ) {
        mem = (float *)malloc( Nh*sizeof(float) );
        if( mem == NULL ) return 0;
        for( i = 0; i < Nh; i++ )
            mem[i] = 0.0f;
    }
    else
        mem = z;

    /* perform filtering */
    for( i = 0; i < Nx; i++ ) {
        sum = h[0]*x[i];
        for( j = Nh - 1; j > 0; j-- ) {
            mem[j] = mem[j-1];
            sum += mem[j]*h[j];
        }
        mem[0] = x[i];
        y[i] = sum;
    }

    /* Free up temporary memory if it was allocated above. */
    if( z == NULL )
        free( mem );

    return 1;
}

/* END OF FILE */
