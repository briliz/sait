/***********************************************************
*
*  PESQ C library implementation 
*
*  pesqerr.h      Diagnostics and analysis functions
*
*  Copyright (C) Psytechnics Limited, 2001-2006.
*  All rights reserved.
*
*  For more information
*  - visit our websites www.psytechnics.com or www.pesq.net
*
*  - write to us at
*      Psytechnics Limited, Fraser House
*      23 Museum Street, Ipswich IP1 1HN, United Kingdom
*
*  - e-mail us at  info@psytechnics.com
*
************************************************************
*
*  Version control
*   $Revision: 1.10 $
*
*	31/3/2006		Release 2.2
*
*	30/10/2004		Release 2.1
**
*   04/10/2002		Release 2.0
*     New file containing PESQ error codes
*
***********************************************************/

/**************************************************************
 *
 * PSYTECHNICS ERROR CODE RANGE (positive 32 bit words)
 *
 * 0x0|xxx xxxx|xxxx xxxx|xxxx xxxx|xxxx xxxx|
 *    | family | product |  module |  error  |
 *
 * family	- product family id	(7 bits)
 * product	- product member id (8 bits)
 * module	- module id			(8 bits)
 * error    - error code		(8 bits)
 *
 * n.b. error code range 0x800 to 0xfff is reserved 
 *		for common, i.e. non module-specific, codes
 *
 **************************************************************/

#ifndef PESQ_ERROR_CODES_H
#define PESQ_ERROR_CODES_H

#ifndef NO_ERROR
#define NO_ERROR						0x0
#endif

/**********************************************
 *
 * PESQ MODULE ERROR CODE RANGE
 *
 **********************************************/

#define PESQ_ERR						0x01010100L	/* PESQ						*/

/**********************************************
 *
 * COMMON ERROR CODES
 *
 **********************************************/
#ifndef PSY_ERROR_CODES_H

/**********************************************
 * Memory related 
 **********************************************/

#define PE_OUT_OF_MEMORY				0x81

/**********************************************
 * File I/O related 
 **********************************************/

#define PE_FILE_OPEN_READ_ERROR			0x91		/* failure to open file for read		*/
#define PE_FILE_OPEN_WRITE_ERROR		0x92		/* failure to open file for write		*/
#define PE_FILE_OPEN_APPEND_ERROR		0x93		/* failure to open file for append		*/
#define PE_FILE_READ_ERROR				0x94		/* read from file has failed			*/
#define PE_FILE_WRITE_ERROR				0x95		/* write to file has failed				*/
#define PE_PATHNAME_TOO_LONG			0x96		/* pathname too long to handle			*/
#define PE_FILENAME_TOO_LONG			0x97		/* filename too long to handle			*/

#endif /* PSY_ERROR_CODES_H */

/**********************************************
 *
 * PESQ ERROR CODES
 *
 **********************************************/

#define PE_INSUFFICIENT_DATA			0x01		/* speech files are too short or reference contains no data	*/
#define PE_INVALID_SAMPLE_RATE			0x11		/* invalid sampling rate				*/
#define PE_INVALID_MODEL				0x12		/* invalid model flag					*/


/**********************************************
 *
 * FULL ERROR CODES RETURNED BY PESQ
 *
 **********************************************/

#define PESQ_ERR_OUT_OF_MEMORY			(PESQ_ERR | PE_OUT_OF_MEMORY)				/* failure to allocate memory			*/
#define PESQ_ERR_FILE_OPEN_READ			(PESQ_ERR | PE_FILE_OPEN_READ_ERROR)		/* failure to open file for read		*/
#define PESQ_ERR_FILE_READ				(PESQ_ERR | PE_FILE_READ_ERROR)				/* read from file has failed			*/
#define PESQ_ERR_INSUFFICIENT_DATA		(PESQ_ERR | PE_INSUFFICIENT_DATA)			/* speech files are too short			*/
#define PESQ_ERR_INVALID_SAMPLE_RATE	(PESQ_ERR | PE_INVALID_SAMPLE_RATE)			/* invalid sampling rate				*/
#define PESQ_ERR_INVALID_MODEL			(PESQ_ERR | PE_INVALID_MODEL)				/* invalid model flag					*/


#endif /* define PESQ_ERROR_CODES_H */

