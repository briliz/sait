/***********************************************************
*
*  PESQ C library implementation 
*
*  pesqmain.c      Main program interfacing to PESQ libary release.
*
*  PESQ-ITU ANSI C source code
*  Copyright (C) British Telecommunications plc, 2000.
*  Copyright (C) Royal KPN NV, 2000.
*
*  Additional modifications
*  Copyright (C) Psytechnics Limited, 2001 - 2006.
*  All rights reserved.
*
*  For more information
*  - visit our websites www.psytechnics.com or www.pesq.net
*
*  - write to us at
*      Psytechnics Limited, Fraser House
*      23 Museum Street, Ipswich IP1 1HN, United Kingdom
*
*  - e-mail us at  info@psytechnics.com
*
************************************************************
*
*  Version control
*   $Id: pesqmain.c,v 1.26 2006/04/21 09:43:54 intra+barrettp Exp $
*
*	31/3/2006		Release 2.2
*		P862.2 supported added
*		Log file format ammended 
*
*   30/10/2004		Release 2.1
*       Moved to new PESQ_core_run API call.
*		862.3 warning messages generated.
*
*   04/10/2002		Release 2.0
*       Moved to new PESQ_core_run API call.
*		Added support for PESQ tools.
*       Corrected bug in parsing parameter "+v1"
*
*   14/11/2001		Release 1.4
*       Moved to new PESQ_ext_run API call and included
*       PESQ-LQ in the log file.
*       New model option -1 for version 1 backwards-compatible
*       processing, selectable with +v1.
*       Fixed bug in logging error messages to log file.
*
*   21/05/2001		Release 1.3
*
*   07/03/2001		Release 1.2
*
************************************************************
*
*  Functions contained:
*   main
*   usage
*
***********************************************************/

#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "pesqdll.h"
#include "pesqinstance.h"
#include "pesq.h"
#include "dsp.h"
#include "pesqerr.h"
#ifdef PESQ_TOOLS
#include "pesqtool.h"
#endif

/*
    Note that this module interfaces to the PESQ algorithm
    only through the PESQDLL interface.  By default it links
    statically to PESQ, but it can also be built to use the
    dynamically linked library (DLL) interface.

    Static build:
    - Compile and link together the following C source files
      pesqmain.c pesqdll.c pesqmod.c pesqdsp.c pesqio.d dsp.c

    DLL build:
    - Build PESQDLL.DLL separately.
    - Compile pesqmain.c and link it with PESQDLL.LIB.
    - Place PESQDLL.DLL in the same directory as the executable,
      or elsewhere on the search path.

    See pesqdll.h for the appropriate compiler macro
    pre-definitions to choose the DLL build; the static build is
    the default case.
*/

/* Log file to which results are written */
#define PESQ_LOG_FILE "pesqlog.txt"

/* Pre-declare function prototypes. */
int main( int argc, char * argv[] );
void usage( char * argv[] );


/************************************************************
*   usage
*
*   Syntax      void usage( char *argv[) )
*
*   Description
*   Simple usage text for PESQ executable.
*
*   Inputs      argv   - argument list
*
*   Modifies    None
*
*   Returns     None
*
*   History
*   Created 27/02/2001 - AR
************************************************************/
void usage( char *argv[] ) {
	char *ptr, *exename;

	/* get exe name from arg[0] without path */
	ptr=argv[0];
	do {
		exename=ptr;
		ptr+=strcspn( ptr, "//\\" );
	} while ( *(ptr++) );

	/* print usage information */
    printf( "\n");
    printf( "Usage:\n");
    printf( "To run PESQ on a 8kHz sampled reference and degraded file pair, use:\n" );
    printf( "  %s +8000 [options] <ref-file> <deg-file>\n", exename );
    printf( "\n");
    printf( "To run PESQ on a 16kHz sampled reference and degraded file pair, use:\n" );
    printf( "  %s +16000 [options] <ref-file> <deg-file>\n", exename );
    printf( "\n");
    printf( "To display this information, use:\n" );
    printf( "  %s help\n", exename);
    printf( "\n");
    printf( "Options:\n");
    printf( "  +wb      - enables wideband mode of operation\n");
    printf( "  +v1      - provides PESQ version 1.0-1.3 compatibility\n");
    printf( "  +hatsear - for recordings where the degraded file is a HATS ear recording\n");
    printf( "  +q       - supresses generation of pesqlog.txt file\n");
    printf( "\n");
	printf( "File names may not begin with a + character. File names with a .wav or\n");
	printf( ".WAV suffix are assumed to contain a valid WAVE header and little-endian\n");
    printf( "16-bit PCM samples. All other file types are assumed to contain 16-bit\n");
	printf( "PCM samples in the native byte order and to have no header.\n");
}

/************************************************************
*   main
*
*   Syntax      int main( int argc, char * argv[] )
*
*   Description
*   Command-line main program for PESQ executable.
*   Writes results to PESQ_LOG_FILE (pesqlog.txt).
*
*   Inputs      Standard C command-line input arguments according to usage().
*
*   Modifies    None
*
*   Returns     0 if success, 1 if failure e.g. if unable to open a file.
*
*   History
*
*   Updated 30/10/2004 - PS - 862.2 warning messages, 
*                             New PESQ_core_run API.
*   Updated 24/09/2002 - AR - Use new PESQ_core_run API,
*                             write PESQ_Ie to file.
*   Updated 07/03/2002 - AR - Corrected "== 1" to "== 0" in
*                             test for +v1 command-line input.
*   Updated 14/11/2001 - AR - Use new PESQ_ext_run API, write
*                             PESQ-LQ to file, and fixed bug
*                             with logging error messages.
*   Created 27/02/2001 - AR
************************************************************/
int main( int argc, char * argv[] ) {
    int  arg;
    int  names = 0;
    char * ref_name = NULL;
    char * deg_name = NULL;
    long LV_Fs = -1L;
    long LV_Model = 0L;
    long LV_Error_Flag = 0L;
    float LV_PESQ_score = 0.0f;
    float LV_PESQ_LQ = 0.0f;
	float LV_PESQ_P862_1 = 0.0f;
	float LV_PESQ_P862_2 = 0.0f;
    float LV_PESQ_Ie = 0.0f;
	long version;
    FILE * log_file = NULL;
	char build[20]="PESQ Core";
	float LV_Params[100];
	short quiet = 0;

#ifndef PESQ_TOOLS
		version=PESQ_version();
#else
		version=PESQ_tool_version();
		strcpy (build, "PESQ Tools" );
#endif

    if( LV_Error_Flag == 0 ) {
        printf("PESQ - Perceptual Evaluation of Speech Quality. \n");
        fflush(stdout);
        printf("Build: %s Version %d.%d.%d\n\n", build, version>>24, (version>>16)&0xff, (version>>8)&0xff);
        printf("Copyright 2000 British Telecommunications plc.\n");
        printf("Copyright 2000 Royal KPN NV.\n");
        printf("Copyright 2001 - 2006 Psytechnics Limited.\nAll rights reserved.\n");
        printf("To use this software you must have a valid licence.\n\n");

        if (argc < 3){
            usage ( argv );
            return 0;                                                                  
        }

        /* Standard PESQ processing: obtain command-line parameters */
        names = 0;
        LV_Fs = -1;
        LV_Model = 0;
        LV_Error_Flag = 0;
        LV_PESQ_score = 0.0f;
        LV_PESQ_LQ = 0.0f;
		LV_PESQ_P862_1 = 0.0f;
		LV_PESQ_P862_2 = 0.0f;
        LV_PESQ_Ie = 0.0f;

        for (arg = 1; arg < argc; arg++) {
            if (argv [arg] [0] == '+') {
                if (strcmp (argv [arg], "+hatsear") == 0) {
                    printf("Selecting HATS ear model\n");
                    LV_Model = 1;
                } else if (strcmp (argv [arg], "+wb") == 0) {
                    printf("Selecting wideband (headphone listening) model\n");
                    LV_Model = 3;
                } else if (strcmp (argv [arg], "+v1") == 0) {
                    printf("Selecting version 1 model\n");
                    LV_Model = -1;
                } else if (strcmp(argv [arg], "+q") == 0) {
                    quiet = 1;
                } else if (strcmp(argv [arg], "+16000") == 0) {
                    LV_Fs = 16000L;
                } else if (strcmp (argv [arg], "+8000") == 0) {
                    LV_Fs = 8000L;
                } else {
                    fprintf(stderr, "Invalid parameter '%s'.\n", argv [arg]);
                    usage( argv );
                    return 1;
				}
            } else {
                switch (names) {
                    case 0: 
                        ref_name = argv[arg];
                        break;
                    case 1: 
                        deg_name = argv[arg];
                        break;
                    default:
                        fprintf (stderr, "Invalid parameter '%s'.\n", argv [arg]);
                        usage ( argv );
                        return 1;
                }
                names++;
            }
        }

        if( LV_Fs == -1 ) {
            printf ("PESQ Error. Must specify either +8000 or +16000 sample frequency option!\n");
            return 1;
        }
    }



    if( LV_Error_Flag == 0 ) {
        /* Run PESQ according to the PESQDLL API in command-line interface
           mode. Note that many of the return values are not used.  */
#ifndef PESQ_TOOLS
		PESQ_core_run( 
            (float *)ref_name, 0L, 0L, (float *)deg_name, 0L, 0L,
            LV_Fs, LV_Model, 49L, 0L,
            &LV_Error_Flag,
            &LV_PESQ_score, &LV_PESQ_LQ,  &LV_PESQ_Ie, LV_Params,
            NULL, NULL,
            NULL, NULL,
            NULL, NULL,
            NULL,
            &LV_PESQ_P862_2, &LV_PESQ_P862_1);
#else
		PESQ_tool_run( 
            (float *)ref_name, 0L, 0L, (float *)deg_name, 0L, 0L,
            LV_Fs, LV_Model, 49L, 0L,
            &LV_Error_Flag,
            &LV_PESQ_score, &LV_PESQ_LQ, &LV_PESQ_Ie, LV_Params,
            NULL, NULL,
            NULL, NULL,
            NULL, NULL,
            NULL,
            NULL, NULL,
			&LV_PESQ_P862_2, &LV_PESQ_P862_1, NULL, NULL, NULL, NULL,
			LV_UttDelay, LV_UttDConf, &N_utterances, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL
			); 
#endif
    }

    /* Test if log file exists.  If it does not, write header row. */
    if ( !quiet )
	{
		errno_t err = fopen_s(&log_file, PESQ_LOG_FILE, "rt");
		if ( err != 0 ) {
			err = fopen_s(&log_file, PESQ_LOG_FILE, "wt");
			if( log_file != NULL )
				fprintf(log_file, "Reference\tDegraded\tFsample\tModel\tRaw\tMapped\tIe\n");
		} else {
			fclose(log_file);
			err = fopen_s(&log_file, PESQ_LOG_FILE, "at");
		}

		if ( err != 0 )
			fprintf( stderr, "Error %d opening log file: %s\n", err, PESQ_LOG_FILE);
	}


    if( LV_Error_Flag == 0 ) {

        /* Write to log file */
        if( log_file != NULL ) {
			switch ( LV_Model ) {
			case 0:
			case 1:
				fprintf(log_file, "%s\t%s\t%ld\t%ld\t%.3f\t%.3f\t%.1f\n",
					ref_name, deg_name, LV_Fs, LV_Model,
					(double)LV_PESQ_score, (double)LV_PESQ_P862_1, (double)LV_PESQ_Ie);
				break;
			case 3:
				fprintf(log_file, "%s\t%s\t%ld\t%ld\t%.3f\t%.3f\t%.1f\n",
					ref_name, deg_name, LV_Fs, LV_Model,
					(double)LV_PESQ_score, (double)LV_PESQ_P862_2, (double)LV_PESQ_Ie);
				break;
			}
			fclose(log_file);
		}
   } else {
        /* An error has occurred */
        printf ("An error of type 0x%lx occurred during processing\n", LV_Error_Flag);

         if( log_file != NULL ) {
            fprintf(log_file, "%s\t%s\t%ld\t%ld\tERROR:0x%08lx\n",
                ref_name, deg_name, LV_Fs, LV_Model, LV_Error_Flag);
            fclose(log_file);
        }

        return 1;
    }

    return 0;
}

PESQ_t *create_new_pesq_instance()
{
    PESQ_t *inst = (PESQ_t *) safe_malloc(sizeof(PESQ_t));
    inst->Fs = 0;
    inst->pfft = create_new_fft_instance();
    return inst;
}

void free_pesq_instance(PESQ_t **pinst)
{
    free_fft_instance(&((*pinst)->pfft));
    safe_free(*pinst);
    *pinst = NULL;
}

__declspec(dllexport) float __cdecl CalculateMos862FromAlawWavFiles(const wchar_t *szInputAudioFileName, const wchar_t *szReferenceAudioFileName, char* buffer, int bufferSize)
{
    long sample_rate = 8000L;
    
    SIGNAL_INFO ref_info = {0};
    SIGNAL_INFO deg_info = {0};
    ERROR_INFO err_info = {0};
    int  names = 0;
	int  count;
	long	N_utterances = 0;
    long LV_Fs = -1L;
    long LV_Model = 0L;
    long LV_Error_Flag = 0L;
    float LV_PESQ_score = 0.0f;
    float LV_PESQ_LQ = 0.0f;
	float LV_PESQ_P862_1 = 0.0f;
	float LV_PESQ_P862_2 = 0.0f;
    float LV_PESQ_Ie = 0.0f;

	float mean_conf = 0, mean_delay = 0, std_delay = 0;
    float LV_UttDConf[50] = {0};
    float LV_Params[100] = {0};
    long  LV_UttDelay[50] = {0};
	long  Nsamples_ref = 0L;
	long  Nsamples_deg = 0L;

    long Error_Flag = 0;
    char * Error_Type = "Unknown error type.";
    PESQ_t *inst = create_new_pesq_instance();

    names = 0;
    LV_Fs = -1;
    LV_Model = 0;
    LV_Error_Flag = 0;
    LV_PESQ_score = 0.0f;
    LV_PESQ_LQ = 0.0f;
	LV_PESQ_P862_1 = 0.0f;
	LV_PESQ_P862_2 = 0.0f;
    LV_PESQ_Ie = 0.0f;
    LV_Fs = 8000L;

    strcpy_s(buffer, bufferSize, "");

    //OutputDebugString(szInputAudioFileName);
    //OutputDebugString(szReferenceAudioFileName);
    wcscpy_s(ref_info.file_name, DIM(ref_info.file_name), szReferenceAudioFileName); 
    wcscpy_s(deg_info.file_name, DIM(deg_info.file_name), szInputAudioFileName); 

    /* Basic initialisation */
    LV_Error_Flag = 0;
    ref_info.apply_swap = 0;
    deg_info.apply_swap = 0;
    err_info.model = LV_Model;
    err_info.Nutterances = 0;
    err_info.tools = 0;

    /* Initialise the elements in err_info that will be used to pass
       data back out to the calling code. */
    err_info.LV_ref_data = (float *)szReferenceAudioFileName;
    err_info.LV_ref_Nsamples = 0;
    err_info.LV_deg_data = (float *)szInputAudioFileName;
    err_info.LV_deg_Nsamples = 0;
    err_info.rtn_bands = 49;
    err_info.LV_Nframes = 0;
    err_info.LV_PESQ_score = &LV_PESQ_score;
    err_info.LV_PESQ_LQ = &LV_PESQ_LQ;
	err_info.LV_PESQ_P862_1 = &LV_PESQ_P862_1;
	err_info.LV_PESQ_P862_2 = &LV_PESQ_P862_2;
    err_info.LV_PESQ_Ie = &LV_PESQ_Ie;
    err_info.LV_Params = LV_Params;
    err_info.LV_ref_surf = NULL;
    err_info.LV_deg_surf = NULL;
    err_info.LV_ref_surf_preeq = NULL;
    err_info.LV_deg_surf_preeq = NULL;
    err_info.LV_FrameDelay = NULL;
    err_info.LV_Frame_score = NULL;
    err_info.LV_Sframe_err = NULL;
    err_info.LV_Aframe_err = NULL;
    err_info.LV_TFE = NULL;
    err_info.LV_Class_err = NULL;
    err_info.LV_Diagnosis = NULL;
    err_info.LV_Level_Spectra = NULL;
    err_info.LV_DlyHistCentres = NULL;
    err_info.LV_DlyHistCount = NULL;
    err_info.LV_Time_err = NULL;
    err_info.LV_Clipping_est = NULL;
    err_info.LV_Lin_Spectra = NULL;
    err_info.LV_Lin_tfe = NULL;
    err_info.LV_ref_spectrogram = NULL;
    err_info.LV_ref_lpc_spectrogram = NULL;
    err_info.LV_ref_excitation = NULL;
    err_info.LV_ref_speech_parms = NULL;
    err_info.LV_deg_spectrogram = NULL;
    err_info.LV_deg_lpc_spectrogram = NULL;
    err_info.LV_deg_excitation = NULL;
    err_info.LV_deg_speech_parms = NULL;

    /* Check sampling rate and model */
    if( (LV_Error_Flag == 0) && (LV_Fs != 8000) && (LV_Fs != 16000) ) {
        LV_Error_Flag = PESQ_ERR_INVALID_SAMPLE_RATE;
        Error_Type = "Invalid sample rate";
    }
    if( (LV_Error_Flag == 0) && (LV_Model != 0) && (LV_Model != -1) &&
        (LV_Model != 1) && (LV_Model != 3) ) {
        LV_Error_Flag = PESQ_ERR_INVALID_MODEL;
        Error_Type = "Invalid model";
    }

    if( LV_Params != NULL ) {
        /* Params receives miscellaneous internal variables */
        for( count = 0; count < 100; count++ )
            LV_Params[count] = 0.0f;
    }

    select_rate (inst, sample_rate, &Error_Flag, &Error_Type);
    load_files (inst, &ref_info, &deg_info, &err_info, &Error_Flag, &Error_Type);
    if (Error_Flag == 0)
    {
        pesq_measure_core (inst, &ref_info, &deg_info, &err_info, &Error_Flag, &Error_Type);
    }

    if (Error_Flag == 0)
    {
        Error_Type = "";

	    /* Test for P.862.2 warning messages */
	    for (count = 0; count < N_utterances; count++)
	    {
		    mean_conf += LV_UttDConf[count];
		    mean_delay += LV_UttDelay[count];
	    }
    	
	    if(N_utterances!=0)
	    {
            mean_delay = mean_delay / N_utterances;
            mean_conf = mean_conf / N_utterances;
	    }

	    std_delay = LV_Params[53];
    	
	    Nsamples_ref = (long)LV_Params[4];
	    Nsamples_deg = (long)LV_Params[6];
    	
        //printf("N_utterances = %d\n", N_utterances);
        //printf("mean_conf = %f\n", mean_conf);
        //printf("mean_delay = %f\n", mean_delay);
        //printf("std_delay = %f\n", std_delay);
        //printf("LV_PESQ_score = %f\n", LV_PESQ_score);
        //printf("Nsamples_ref = %d\n", Nsamples_ref);
        //printf("Nsamples_deg = %d\n", Nsamples_deg);
        //printf("LV_Params[5] = %f\n", LV_Params[5]);
        //printf("LV_Params[7] = %f\n", LV_Params[7]);
        //printf("LV_Params[9] = %f\n", LV_Params[9]);

	    /* Possible unrelated signals message */
	    if(  (mean_conf < 0.3) && (std_delay > 0.05) && (LV_PESQ_score > 1.5))
            Error_Type = "CAUTION: Possibly unlrelated reference and degraded files.";
    		
	    /* Possible file duration problems */
	    if( (Nsamples_ref > 960000) || (Nsamples_deg > 960000))
		    Error_Type = "CAUTION: Possible file duration problem.";

	    if ((Nsamples_ref > 560000) && (LV_Params[9] == 16000))
		    Error_Type = "CAUTION: Reference file is greater than 35 seconds.";

	    if ((Nsamples_ref > 280000) && (LV_Params[9] == 8000))
		    Error_Type = "CAUTION: Reference file is greater than 35 seconds.";

	    /* Potential level alignment problem */
	    if(( fabs(LV_Params[5] - LV_Params[7]) > 0.2) )
		    Error_Type = "CAUTION: Difference in speech activity > 20%.";

	    if( fabs( ((double)(Nsamples_ref - Nsamples_deg)) / (Nsamples_ref)) > 0.2)
		    Error_Type = "CAUTION: Difference in length > 20%.";
    	
	    /* Speech activity warning */
	    if( (LV_Params[5] <= 0.35) || (LV_Params[7] <= 0.35))
		    Error_Type = "CAUTION: Speech activity is low.";

	    if( (LV_Params[5] >= 0.85) || (LV_Params[7] >= 0.85))
		    Error_Type = "CAUTION: Speech activity is too high.";

	    /* print scores */
	    switch ( LV_Model ) {
	    case 0:
	    case 1:
		    printf ("\nRaw score = %.3f    P.862.1 = %.3f    Ie (P.834) = %.1f\n",
                (double)LV_PESQ_score, (double)LV_PESQ_P862_1, (double)LV_PESQ_Ie);
		    break;
	    case 3:
		    printf ("\nRaw score = %.3f    P.862.2 = %.3f    Ie (P.834) = %.1f\n",
                (double)LV_PESQ_score, (double)LV_PESQ_P862_2, (double)LV_PESQ_Ie);
		    break;
	    }
    }

    free_pesq_instance(&inst);

    if (Error_Type != NULL)
    {
        wprintf(L"ref='%ws' deg='%ws' %hs\n", szReferenceAudioFileName, szInputAudioFileName, Error_Type);
        strcpy_s(buffer, bufferSize, Error_Type);
    }

	return (Error_Flag == 0) ? err_info.pesq_P862_1 : 0;
}

__declspec(dllexport) float __cdecl CalculateMos862FromLinearFloatSamples(const float* degradedSamples, int numDegradedSamples, const float* originalSamples, int numOriginalSamples, char* buffer, int bufferSize)
{
    long sample_rate = 8000L;

    SIGNAL_INFO ref_info = { 0 };
    SIGNAL_INFO deg_info = { 0 };
    ERROR_INFO err_info = { 0 };
    int  names = 0;
    int  count;
    long	N_utterances = 0;
    long LV_Fs = -1L;
    long LV_Model = 0L;
    long LV_Error_Flag = 0L;
    float LV_PESQ_score = 0.0f;
    float LV_PESQ_LQ = 0.0f;
    float LV_PESQ_P862_1 = 0.0f;
    float LV_PESQ_P862_2 = 0.0f;
    float LV_PESQ_Ie = 0.0f;

    float mean_conf = 0, mean_delay = 0, std_delay = 0;
    float LV_UttDConf[50] = { 0 };
    float LV_Params[100] = { 0 };
    long  LV_UttDelay[50] = { 0 };
    long  Nsamples_ref = 0L;
    long  Nsamples_deg = 0L;

    long Error_Flag = 0;
    char * Error_Type = "Unknown error type.";
    PESQ_t *inst = create_new_pesq_instance();

    names = 0;
    LV_Fs = -1;
    LV_Model = 0;
    LV_Error_Flag = 0;
    LV_PESQ_score = 0.0f;
    LV_PESQ_LQ = 0.0f;
    LV_PESQ_P862_1 = 0.0f;
    LV_PESQ_P862_2 = 0.0f;
    LV_PESQ_Ie = 0.0f;
    LV_Fs = 8000L;

    strcpy_s(buffer, bufferSize, "");

    /* Basic initialisation */
    LV_Error_Flag = 0;
    ref_info.apply_swap = 0;
    deg_info.apply_swap = 0;
    err_info.model = LV_Model;
    err_info.Nutterances = 0;
    err_info.tools = 0;

    /* Initialise the elements in err_info that will be used to pass
    data back out to the calling code. */
    err_info.LV_ref_data = (float*)originalSamples;
    err_info.LV_ref_Nsamples = numOriginalSamples;
    err_info.LV_deg_data = (float*)degradedSamples;
    err_info.LV_deg_Nsamples = numDegradedSamples;
    err_info.rtn_bands = 49;
    err_info.LV_Nframes = 0;
    err_info.LV_PESQ_score = &LV_PESQ_score;
    err_info.LV_PESQ_LQ = &LV_PESQ_LQ;
    err_info.LV_PESQ_P862_1 = &LV_PESQ_P862_1;
    err_info.LV_PESQ_P862_2 = &LV_PESQ_P862_2;
    err_info.LV_PESQ_Ie = &LV_PESQ_Ie;
    err_info.LV_Params = LV_Params;
    err_info.LV_ref_surf = NULL;
    err_info.LV_deg_surf = NULL;
    err_info.LV_ref_surf_preeq = NULL;
    err_info.LV_deg_surf_preeq = NULL;
    err_info.LV_FrameDelay = NULL;
    err_info.LV_Frame_score = NULL;
    err_info.LV_Sframe_err = NULL;
    err_info.LV_Aframe_err = NULL;
    err_info.LV_TFE = NULL;
    err_info.LV_Class_err = NULL;
    err_info.LV_Diagnosis = NULL;
    err_info.LV_Level_Spectra = NULL;
    err_info.LV_DlyHistCentres = NULL;
    err_info.LV_DlyHistCount = NULL;
    err_info.LV_Time_err = NULL;
    err_info.LV_Clipping_est = NULL;
    err_info.LV_Lin_Spectra = NULL;
    err_info.LV_Lin_tfe = NULL;
    err_info.LV_ref_spectrogram = NULL;
    err_info.LV_ref_lpc_spectrogram = NULL;
    err_info.LV_ref_excitation = NULL;
    err_info.LV_ref_speech_parms = NULL;
    err_info.LV_deg_spectrogram = NULL;
    err_info.LV_deg_lpc_spectrogram = NULL;
    err_info.LV_deg_excitation = NULL;
    err_info.LV_deg_speech_parms = NULL;

    /* Check sampling rate and model */
    if ((LV_Error_Flag == 0) && (LV_Fs != 8000) && (LV_Fs != 16000)) {
        LV_Error_Flag = PESQ_ERR_INVALID_SAMPLE_RATE;
        Error_Type = "Invalid sample rate";
    }
    if ((LV_Error_Flag == 0) && (LV_Model != 0) && (LV_Model != -1) &&
        (LV_Model != 1) && (LV_Model != 3)) {
        LV_Error_Flag = PESQ_ERR_INVALID_MODEL;
        Error_Type = "Invalid model";
    }

    if (LV_Params != NULL) {
        /* Params receives miscellaneous internal variables */
        for (count = 0; count < 100; count++)
            LV_Params[count] = 0.0f;
    }

    select_rate(inst, sample_rate, &Error_Flag, &Error_Type);
    
    read_pcm_data(inst, &ref_info, originalSamples, numOriginalSamples, &err_info, &Error_Flag, &Error_Type);
    read_pcm_data(inst, &deg_info, degradedSamples, numDegradedSamples, &err_info, &Error_Flag, &Error_Type);
    
    if (Error_Flag == 0)
    {
        pesq_measure_core(inst, &ref_info, &deg_info, &err_info, &Error_Flag, &Error_Type);
    }

    if (Error_Flag == 0)
    {
        Error_Type = "";

        /* Test for P.862.2 warning messages */
        for (count = 0; count < N_utterances; count++)
        {
            mean_conf += LV_UttDConf[count];
            mean_delay += LV_UttDelay[count];
        }

        if (N_utterances != 0)
        {
            mean_delay = mean_delay / N_utterances;
            mean_conf = mean_conf / N_utterances;
        }

        std_delay = LV_Params[53];

        Nsamples_ref = (long)LV_Params[4];
        Nsamples_deg = (long)LV_Params[6];

        /* Possible unrelated signals message */
        if ((mean_conf < 0.3) && (std_delay > 0.05) && (LV_PESQ_score > 1.5))
            Error_Type = "CAUTION: Possibly unlrelated reference and degraded files.";

        /* Possible file duration problems */
        if ((Nsamples_ref > 960000) || (Nsamples_deg > 960000))
            Error_Type = "CAUTION: Possible file duration problem.";

        if ((Nsamples_ref > 560000) && (LV_Params[9] == 16000))
            Error_Type = "CAUTION: Reference file is greater than 35 seconds.";

        if ((Nsamples_ref > 280000) && (LV_Params[9] == 8000))
            Error_Type = "CAUTION: Reference file is greater than 35 seconds.";

        /* Potential level alignment problem */
        if ((fabs(LV_Params[5] - LV_Params[7]) > 0.2))
            Error_Type = "CAUTION: Difference in speech activity > 20%.";

        if (fabs(((double)(Nsamples_ref - Nsamples_deg)) / (Nsamples_ref)) > 0.2)
            Error_Type = "CAUTION: Difference in length > 20%.";

        /* Speech activity warning */
        if ((LV_Params[5] <= 0.35) || (LV_Params[7] <= 0.35))
            Error_Type = "CAUTION: Speech activity is low.";

        if ((LV_Params[5] >= 0.85) || (LV_Params[7] >= 0.85))
            Error_Type = "CAUTION: Speech activity is too high.";

        /* print scores */
        switch (LV_Model) {
        case 0:
        case 1:
            printf("\nRaw score = %.3f    P.862.1 = %.3f    Ie (P.834) = %.1f\n",
                (double)LV_PESQ_score, (double)LV_PESQ_P862_1, (double)LV_PESQ_Ie);
            break;
        case 3:
            printf("\nRaw score = %.3f    P.862.2 = %.3f    Ie (P.834) = %.1f\n",
                (double)LV_PESQ_score, (double)LV_PESQ_P862_2, (double)LV_PESQ_Ie);
            break;
        }
    }

    free_pesq_instance(&inst);

    if (Error_Type != NULL)
    {
       // wprintf(L"ref='%ws' deg='%ws' %hs\n", szReferenceAudioFileName, szInputAudioFileName, Error_Type);
        strcpy_s(buffer, bufferSize, Error_Type);
    }

    return (Error_Flag == 0) ? err_info.pesq_P862_1 : 0;
}

void read_pcm_data(PESQ_t *inst, 
    SIGNAL_INFO * sig_info, 
    const float* pcm16BitSamples, 
    int numPcmSamples, 
    ERROR_INFO * err_info, 
    long * Error_Flag, 
    char ** Error_Type)
{
    sig_info->data = NULL;
    sig_info->VAD = NULL;
    sig_info->logVAD = NULL;

    if ((*Error_Flag) == 0)
    {
        sig_info->Nsamples = numPcmSamples + 2 * SEARCHBUFFER * inst->Downsample;
        sig_info->data = (float *)safe_malloc((sig_info->Nsamples + DATAPADDING_MSECS  * (inst->Fs / 1000)) * sizeof(float));
        if (sig_info->data == NULL)
        {
            *Error_Flag = PESQ_ERR_OUT_OF_MEMORY;
            *Error_Type = "Failed to allocate memory for source file";
            printf("%s!\n", *Error_Type);
            return;
        }
        float *read_ptr = sig_info->data;
        long read_count;
        for (read_count = SEARCHBUFFER*inst->Downsample; read_count > 0; read_count--)
        {
            *(read_ptr++) = 0.0f;
        }
        
        for (int i = 0; i < numPcmSamples; i++)
        {
            read_ptr[i] = pcm16BitSamples[i];
        }

        read_ptr += numPcmSamples;

        for (read_count = DATAPADDING_MSECS * (inst->Fs / 1000) + SEARCHBUFFER * inst->Downsample; read_count > 0; read_count--)
        {
            *(read_ptr++) = 0.0f;
        }

        sig_info->VAD = (float *)safe_malloc(sig_info->Nsamples * sizeof(float) / inst->Downsample);
        sig_info->logVAD = (float *)safe_malloc(sig_info->Nsamples * sizeof(float) / inst->Downsample);
        if ((sig_info->VAD == NULL) || (sig_info->logVAD == NULL))
        {
            *Error_Flag = PESQ_ERR_OUT_OF_MEMORY;
            *Error_Type = "Failed to allocate memory for VAD";
            printf("%s!\n", *Error_Type);
            return;
        }
    }
}

void load_files (PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info,
    ERROR_INFO * err_info, long * Error_Flag, char ** Error_Type)
{
    ref_info-> data = NULL;
    ref_info-> VAD = NULL;
    ref_info-> logVAD = NULL;
    
    deg_info-> data = NULL;
    deg_info-> VAD = NULL;
    deg_info-> logVAD = NULL;
        
    if ((*Error_Flag) == 0)
    {
        //printf ("Reading reference file %s...", ref_info-> path_name);

       load_src_from_alaw_wav (inst, Error_Flag, Error_Type, ref_info);
       //if ((*Error_Flag) == 0)
       //    printf ("done.\n");
    }
    if ((*Error_Flag) == 0)
    {
        //printf ("Reading degraded file %s...", deg_info-> path_name);

       load_src_from_alaw_wav (inst, Error_Flag, Error_Type, deg_info);
       //if ((*Error_Flag) == 0)
       //    printf ("done.\n");
    }
}

/* END OF FILE */
