#ifndef FFTINSTANCE_INCLUDED_H
#define FFTINSTANCE_INCLUDED_H

typedef struct {
    unsigned long   FFTSwapInitialised;
    unsigned long   FFTLog2N;
    unsigned long * FFTButter;
    unsigned long * FFTBitSwap;
    float         * FFTPhi;
} FFT_t;

FFT_t* create_new_fft_instance();
void free_fft_instance(FFT_t** ppfft);



#endif /* FFTINSTANCE_INCLUDED_H */

/* END OF FILE */
