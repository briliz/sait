/***********************************************************
*
*  PESQ C implementation 
*
*  pesqdll.h      Include file for code that uses pesqdll.dll.
*
*  PESQ-ITU ANSI C source code
*  Copyright (C) British Telecommunications plc, 2000.
*  Copyright (C) Royal KPN NV, 2000.
*
*  Additional modifications
*  Copyright (C) Psytechnics Limited, 2001 - 2006.
*  All rights reserved.
*
*  For more information
*  - visit our websites www.psytechnics.com or www.pesq.net
*
*  - write to us at
*      Psytechnics Limited, Fraser House
*      23 Museum Street, Ipswich IP1 1HN, United Kingdom
*
*  - e-mail us at  info@psytechnics.com
*
************************************************************
*
*   $Revision: 1.14 $
*
*	31/3/2006		Release 2.2
*	  PESQ API calls changed to include LV_PESQ_P862_2 
*
*   30/10/2004		Release 2.1
*	  PESQ_core_run API changed to include additional 
*	  parameter for 862.1 score
*
*	04/10/2002		Release 2.0
*	  Migrated to new PESQ_core API
*	  Version format changed.
*
*   14/11/2001		Release 1.4
*     Modification to syntax of PESQ_ext_run for API extensions.
*     New PESQ model -1 for version 1.0-1.3 processing.
*
*   21/05/2001		Release 1.3
*     New API call PESQ_ext_run offering some extensions.
*
*   07/03/2001		Release 1.2
*
***********************************************************/

#ifndef PESQ_DLL_INCLUDED_H
#define PESQ_DLL_INCLUDED_H

#ifdef __cplusplus
extern "C" {
#endif

/***********************************************************
*  Export declarations.
*
*  This code can be used to build a number of different types of
*  library/executable by pre-defining the appropriate macro.
*
*  Stand-alone Windows DLL:
*     WATCOM C: pre-define WATCOMC_PESQ_DLL
*     Microsoft Visual C++: pre-define MSVC_PESQ_DLL
*
*  Static library or static linking as part of a project to
*  build executable code: pre-define empty macro PESQExport.
*
*  Dynamically linked library on other systems:
*  pre-define PESQExport as appropriate.
*
*  By default, if none of the above macros are defined,
*  PESQExport is assumed to be empty e.g. for static linking.
***********************************************************/

#ifndef PESQExport
  #ifdef WATCOMC_PESQ_DLL
    #define PESQExport __stdcall
  #endif
  #ifdef MSVC_PESQ_DLL
    #define PESQExport __stdcall
  #endif
#endif
#ifndef PESQExport
  #define PESQExport
#endif

/************************************************************
*   PESQ_version
*
*   Syntax        long PESQExport PESQ_version( void )
*
*   Description
*   When called, this returns the version number of PESQ as
*   a 4-byte integer:
*   MSB                             LSB
*       XX      XX      XX      XX
*    Release Version  Edition  Unused
*
*   Thus if the version of PESQ is x.y.z, the function returns
*   x*2^24 + y*2^16 + z*2^8.
*
*   Inputs        None
*
*   Modifies    None
*
*   Returns        PESQ version number encoded as described above.
*
*   History
*   Updated 24/09/2002 - AR - New 4-byte version codes
*   Created 27/02/2001 - AR
************************************************************/
long PESQExport PESQ_version( void );

/************************************************************
*   PESQ_core_run
*
*   Syntax
*   void PESQExport PESQ_core_run(
*     float * LV_ref_data, long LV_ref_Nsamples, long LV_Reserved_ref,
*     float * LV_deg_data, long LV_deg_Nsamples, long LV_Reserved_deg,
*     long LV_Fs, long LV_Model,
*     long LV_Rtn_Bands, long LV_Nframes,
*     long * LV_Error_Flag,
*     float * LV_PESQ_score, float * LV_PESQ_LQ, 
*     float * LV_PESQ_Ie, float LV_Params[100],
*     float * LV_ref_surf, float * LV_deg_surf,
*     long * LV_FrameDelay, float * LV_Frame_score,
*     float * LV_Sframe_err, float * LV_Aframe_err,
*     float * LV_TFE,
*     float * LV_PESQ_P862_2, void * LV_ReservedB );
*
*   Description
*   Main application programming interface (API) to PESQ.
*   Can be used for static or dynamic linking.  If used to
*   build a Windows dynamic linked library (DLL), which is
*   placed on the search path, PESQ can then be available to
*   any code.
*
*   Inputs
*   LV_ref_data array of float containing LV_ref_Nsamples
*               of reference speech recording.  If LV_ref_Nsamples
*               is equal to zero, LV_ref_data should point
*               to a null-terminated array of char giving the name
*               of the reference file to be read in.
*
*   LV_Reserved_ref Reserved - should be set to zero.
*
*   LV_deg_data array of float containing LV_deg_Nsamples
*               of degraded speech recording.  If LV_deg_Nsamples
*               is zero, contains null-terminated file name.
*
*   LV_Reserved_deg Reserved - should be set to zero.
*
*   LV_Fs       the sample rate in Hertz of the reference and
*               degraded files.  This is used to choose the
*               appropriate PESQ model.
*
*   LV_Model    choice of model:
*               0  standard PESQ (narrowband handset on ref and deg)
*               -1 PESQ version 1.0-1.3 model (narrowband handset on
*                  ref and deg)
*               1  deg is a HATS ear recording, ref is unprocessed (wideband)
*               3  wideband model (headphone listening)
*
*   LV_Rtn_Bands  number of Bark bands to return.  LV_Rtn_Bands must be
*               set to either 42 or 49.  Used for sensation surface
*               and TFE output variables.
*
*               The 8kHz model internally uses 42 bands; the 16kHz model uses
*               49 bands. If LV_Rtn_Bands is set to 42 but the model is run at
*               16kHz sample rate, the top seven bands are not returned.
*               If LV_Rtn_Bands is set to 49 but the model is run at
*               8kHz sample rate, the top seven bands are filled with zeros.
*
*   LV_Nframes  the number of frames in the arrays (ref_surf, deg_surf, etc)
*               that will receive PESQ output data.  This should be set to
*               the number of whole frames in the reference.
*               Frames are 32ms long, and overlap by 50%.
*               At 16kHz, Frame_Len = 512; at 8kHz Frame_Len = 256 (samples)
*               LV_Nframes = floor(LV_ref_Nsamples / (Frame_Len/2)) - 1
*
*   Outputs
*   LV_Error_Flag (long int *) receives 0 if processing completes OK, or a
*               non-zero error flag indicating the type of error (see
*               pesqerr.h). If errors occur no data is returned and the
*               above parameters will be undefined.
*               LV_Error_Flag must be defined.
*
*   LV_PESQ_score receives PESQ score (on -1 to 4.5 scale)
*               (set to NULL if not required).
*
*   LV_PESQ_LQ  receives PESQ score mapped to subjective MOS (on 1 to 5 scale);
*               see "PESQ score and P.800 ACR LQ MOS - Technical note"
*               published by Psytechnics Limited (set to NULL if not required).
*
*   LV_PESQ_Ie  receives E-model Ie (impairment factor) derived from PESQ
*               score. On a 0 to 140 scale - see ITU-T Recommendation P.834
*               (set to NULL if not required).
*
*   LV_Params   arrays of floats pre-allocated to receive 100 miscellaneous
*               values (set to NULL if not required).
*               Note that the definitions of LV_Params have changed slightly
*               from previous versions.
*               LV_Params[0]    PESQ score
*               LV_Params[1]    PESQ-LQ score (mapped to be closer to LQ MOS)
*               LV_Params[2]    Ie value derived from PESQ score (P.834)
*
*               LV_Params[4]    Number of samples in reference
*               LV_Params[5]    Speech activity in reference
*               LV_Params[6]    Number of samples in degraded
*               LV_Params[7]    Speech activity in degraded
*               LV_Params[8]    Model selected (LV_Model);
*               LV_Params[9]    Sample rate in Hz (LV_Fs);
*
*               LV_Params[11]   Symmetric disturbance (d_indicator)
*               LV_Params[12]   Asymmetric disturbance (a_indicator)
*				LV_Params[13]	P.862.1 mapped score (ITU universal MOS mapping)
*
*               The following parameters are set in PESQ Tools only:
*               LV_Params[20]   Mean DC level of reference signal
*               LV_Params[21]   Active speech level (ASL) of reference signal
*                               in dBov
*               LV_Params[22]   Mean noise level of reference signal, dBov
*               LV_Params[23]   RMS mean level of whole reference signal, dBov
*               LV_Params[30]   Mean DC level of degraded signal
*               LV_Params[31]   ASL of degraded signal, dBov
*               LV_Params[32]   Mean noise level of degraded signal, dBov
*               LV_Params[33]   RMS mean level of whole degraded signal, dBov
*               LV_Params[41]   Insertion gain: dB difference between ASL
*                               of reference and degraded signals.
*               LV_Params[42]   Noise gain: difference between mean noise
*                               level of reference and degraded signals.
*
*               LV_Params[50]   Mean delay, in s
*               LV_Params[51]   Minimum delay, in s
*               LV_Params[52]   Maximum delay, in s
*               LV_Params[53]   Standard deviation of delay, in s
*
*               Note that values in dBov are calculated with reference to 32767
*               as the overload value: a _square_ wave alternating between
*               +/-32767 would have a level of 0 dBov.  A value of -999.0 dBov
*               means that the parameter could not be calculated.
*
*   LV_ref_surf storage to receive reference and degraded
*   LV_deg_surf sensation surfaces, each pre-allocated to at least
*               LV_Rtn_Bands * LV_Nframes
*               floats.  The error surface is defined as
*               Degraded - Reference and is not returned.
*               (set to NULL if not required).
*
*   LF_FrameDelay storage allocated to LV_Nframes longs, which receives the
*               frame-by-frame delay estimates used in the model.
*               (set to NULL if not required).
*
*   LV_Frame_score  receives a PESQ score estimate for each frame
*               (set to NULL if not required)
*
*   LV_Sframe_err storage to receive symmetric frame disturbance, pre-allocated
*               to LV_Nframes floats.
*               (set to NULL if not required).
*
*   LV_Aframe_err storage to receive asymmetric frame disturbance, pre-allocated
*               to LV_Nframes floats.
*               (set to NULL if not required).
*
*   LV_TFE      array of LV_Rtn_Bands floats which receives the transfer
*               function estimate (dB) in each perceptual frequency band
*               (set to NULL if not required).
*
*   LV_PESQ_P862_2 receives PESQ score from a wideband measurement mapped to a 
*				wideband context subjective MOS (on 1 to 5 scale) according to 
*				ITU-T Recommendation P.862.2, "Wideband extension to Recommendation 
*				P.862 for the assessment of wideband telephone networks and 
*				speech codecs (11/2005)"
*
*	LV_PESQ_P862_1 receives PESQ score mapped to subjective MOS (on 1 to 5 scale)
*				according to ITU universal mapping function;      
*               see "Mapping function for transforming P.862 raw result scores
*               to MOS-LQO", ITU-T recommendation P.862.1, November 2003.
*
*   Modifies    All of the following, if not set to NULL:
*       LV_Error_Flag,  LV_PESQ_score, LV_PESQ_LQ, LV_PESQ_P862_1, LV_PESQ_Ie, LV_Params,
*       LV_ref_surf, LV_deg_surf, LV_FrameDelay, LV_Frame_score,
*       LV_Sframe_err, LV_Aframe_err, LV_TFE
*
*   Returns     None
*
*   History
*   Updated 24/09/2002 - AR - restructured API call
*   Updated 12/02/2002 - AR - created from pesqdll.c: PESQ_ext_run().
*   Created 21/05/2001 - AR
************************************************************/
void PESQExport PESQ_core_run(
  float * LV_ref_data, long LV_ref_Nsamples, long LV_Reserved_ref,
  float * LV_deg_data, long LV_deg_Nsamples, long LV_Reserved_deg,
  long LV_Fs, long LV_Model,
  long LV_Rtn_Bands, long LV_Nframes,
  long * LV_Error_Flag,
  float * LV_PESQ_score, float * LV_PESQ_LQ, 
  float * LV_PESQ_Ie, float LV_Params[100],
  float * LV_ref_surf, float * LV_deg_surf,
  long * LV_FrameDelay, float * LV_Frame_score,
  float * LV_Sframe_err, float * LV_Aframe_err,
  float * LV_TFE,
  float * LV_PESQ_P862_2, float * LV_PESQ_P862_1 );
  
#ifdef __cplusplus
}

#endif /* PESQ_DLL_INCLUDED_H */

#endif
