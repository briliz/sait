/***********************************************************
*
*  PESQ C library implementation 
*
*  dsp.h      Header file for DSP.C
*
*  PESQ-ITU ANSI C source code
*  Copyright (C) British Telecommunications plc, 2000.
*  Copyright (C) Royal KPN NV, 2000.
*
*  Additional modifications
*  Copyright (C) Psytechnics Limited, 2001-2006.
*  All rights reserved.
*
*  For more information
*  - visit our websites www.psytechnics.com or www.pesq.net
*
*  - write to us at
*      Psytechnics Limited, Fraser House
*      23 Museum Street, Ipswich IP1 1HN, United Kingdom
*
*  - e-mail us at  info@psytechnics.com
*
************************************************************
*
*  Version control:
*   $Revision: 1.9 $
*
*	31/3/2006		Release 2.2
*
*	30/10/2004		Release 2.1
*
*   04/10/2002		Release 2.0
*      Included new function FIRFilt().
*	
*   14/11/2001		Release 1.4
*
*   21/05/2001		Release 1.3
*
*   07/03/2001		Release 1.2
*
***********************************************************/

#ifndef DSP_INCLUDED_H
#define DSP_INCLUDED_H

#ifndef min
  #define min(a,b)  (((a) < (b)) ? (a) : (b))
#endif

#ifndef max
  #define max(a,b)  (((a) > (b)) ? (a) : (b))
#endif

#ifndef PESQ_DECL 
  #define PESQ_DECL 
#endif

#ifndef DIM
  #define DIM(x) (sizeof(x)/sizeof(x[0]))
#endif

PESQ_DECL void *safe_malloc (unsigned long);
PESQ_DECL void safe_free (void *);
PESQ_DECL void malloc_check( void );
PESQ_DECL unsigned long nextpow2(unsigned long X);
PESQ_DECL int ispow2(unsigned long X);
PESQ_DECL int intlog2(unsigned long X);
PESQ_DECL void FFTInit(FFT_t *pfft, unsigned long N);
PESQ_DECL void FFTFree(FFT_t *pfft);
PESQ_DECL void RealFFT(FFT_t *pfft, float * xr, unsigned long N);
PESQ_DECL void RealIFFT(FFT_t *pfft, float * xr, unsigned long N);
PESQ_DECL unsigned long FFTNXCorr(FFT_t *pfft, 
    float * x1, unsigned long n1, float * x2, unsigned long n2, float * y );
PESQ_DECL void IIRsos(
    float * x, unsigned long Nx,
    float b0, float b1, float b2, float a1, float a2,
    float * tz1, float * tz2 );
PESQ_DECL void IIRFilt(
    const float * h, unsigned long Nsos, float * z,
    float * x, unsigned long Nx, float * y );
PESQ_DECL int FIRFilt(
    float * h, unsigned long Nh, float * z,
    float * x, unsigned long Nx, float * y );

#endif /* DSP_INCLUDED_H */

/* END OF FILE */
