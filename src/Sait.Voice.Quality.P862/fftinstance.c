
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fftinstance.h"
#include "dsp.h"


FFT_t* create_new_fft_instance()
{
    FFT_t *inst = (FFT_t *) safe_malloc(sizeof(FFT_t));
    memset(inst, 0, sizeof(FFT_t));
    inst->FFTSwapInitialised = 0;
    return inst;
}

void free_fft_instance(FFT_t** ppfft)
{
    safe_free(*ppfft);
    *ppfft = NULL;
}
