/***********************************************************
*
*  PESQ C library implementation 
*
*  pesqio.c      File input and output routines
*
*  PESQ-ITU ANSI C source code
*  Copyright (C) British Telecommunications plc, 2000.
*  Copyright (C) Royal KPN NV, 2000.
*
*  Additional modifications
*  Copyright (C) Psytechnics Limited, 2001 - 2006.
*  All rights reserved.
*
*  For more information
*  - visit our websites www.psytechnics.com or www.pesq.net
*
*  - write to us at
*      Psytechnics Limited, Fraser House
*      23 Museum Street, Ipswich IP1 1HN, United Kingdom
*
*  - e-mail us at  info@psytechnics.com
*
************************************************************
*
*   Functions contained:
*   select_rate
*   file_exist
*   load_src
*   alloc_other
*   
************************************************************
*
*  Version control
*   $Id: pesqio.c,v 1.17 2006/03/21 10:15:30 intra+barrettp Exp $
*
*	31/3/2006		Release 2.2
*
*   30/10/2004		Release 2.1
*
*   04/10/2002		Release 2.0
*     Inserted additional check in load_src on file length.
*
*   14/11/2001		Release 1.4
*
*   21/05/2001		Release 1.3
*
*   07/03/2001		Release 1.2
*
***********************************************************/
#define _CRT_SECURE_NO_WARNINGS

#include <windows.h>
#include <stdio.h>
#include <string.h>
#include "pesq.h"
#include "dsp.h"
#include "pesqerr.h"

/******************************************************************************
						Internal Constant definitions   
******************************************************************************/
#define OWPCMINFILE_PSY_WAVE_FORMAT_LINPCM	0x0001  /* Linear PCM		*/
#define OWPCMINFILE_PSY_WAVE_FORMAT_ALAW    0x0006  /* A-Law            */
#define OWPCMINFILE_PSY_WAVE_FORMAT_MULAW   0x0007  /* mu-Law           */
#define OWPCMINFILE_FMT_SIZE_IN_FILE		16		/* Size of Fmt chunk in file */

float mulawTable[256] =
{
    -32124, -31100, -30076, -29052, -28028,
    -27004, -25980, -24956, -23932, -22908,
    -21884, -20860, -19836, -18812, -17788,
    -16764, -15996, -15484, -14972, -14460,
    -13948, -13436, -12924, -12412, -11900,
    -11388, -10876, -10364,  -9852,  -9340,
     -8828,  -8316,  -7932,  -7676,  -7420,
     -7164,  -6908,  -6652,  -6396,  -6140,
     -5884,  -5628,  -5372,  -5116,  -4860,
     -4604,  -4348,  -4092,  -3900,  -3772,
     -3644,  -3516,  -3388,  -3260,  -3132,
     -3004,  -2876,  -2748,  -2620,  -2492,
     -2364,  -2236,  -2108,  -1980,  -1884,
     -1820,  -1756,  -1692,  -1628,  -1564,
     -1500,  -1436,  -1372,  -1308,  -1244,
     -1180,  -1116,  -1052,   -988,   -924,
      -876,   -844,   -812,   -780,   -748,
      -716,   -684,   -652,   -620,   -588,
      -556,   -524,   -492,   -460,   -428,
      -396,   -372,   -356,   -340,   -324,
      -308,   -292,   -276,   -260,   -244,
      -228,   -212,   -196,   -180,   -164,
      -148,   -132,   -120,   -112,   -104,
       -96,    -88,    -80,    -72,    -64,
       -56,    -48,    -40,    -32,    -24,
       -16,     -8,      0,  32124,  31100,
     30076,  29052,  28028,  27004,  25980,
     24956,  23932,  22908,  21884,  20860,
     19836,  18812,  17788,  16764,  15996,
     15484,  14972,  14460,  13948,  13436,
     12924,  12412,  11900,  11388,  10876,
     10364,   9852,   9340,   8828,   8316,
      7932,   7676,   7420,   7164,   6908,
      6652,   6396,   6140,   5884,   5628,
      5372,   5116,   4860,   4604,   4348,
      4092,   3900,   3772,   3644,   3516,
      3388,   3260,   3132,   3004,   2876,
      2748,   2620,   2492,   2364,   2236,
      2108,   1980,   1884,   1820,   1756,
      1692,   1628,   1564,   1500,   1436,
      1372,   1308,   1244,   1180,   1116,
      1052,    988,    924,    876,    844,
       812,    780,    748,    716,    684,
       652,    620,    588,    556,    524,
       492,    460,    428,    396,    372,
       356,    340,    324,    308,    292,
       276,    260,    244,    228,    212,
       196,    180,    164,    148,    132,
       120,    112,    104,     96,     88,
        80,     72,     64,     56,     48,
        40,     32,     24,     16,      8,
         0
};

float alawTable[256] =
{
      -5504,   -5248,   -6016,   -5760,   -4480,
      -4224,   -4992,   -4736,   -7552,   -7296,
      -8064,   -7808,   -6528,   -6272,   -7040,
      -6784,   -2752,   -2624,   -3008,   -2880,
      -2240,   -2112,   -2496,   -2368,   -3776,
      -3648,   -4032,   -3904,   -3264,   -3136,
      -3520,   -3392,  -22016,  -20992,  -24064,
     -23040,  -17920,  -16896,  -19968,  -18944,
     -30208,  -29184,  -32256,  -31232,  -26112,
     -25088,  -28160,  -27136,  -11008,  -10496,
     -12032,  -11520,   -8960,   -8448,   -9984,
      -9472,  -15104,  -14592,  -16128,  -15616,
     -13056,  -12544,  -14080,  -13568,    -344,
       -328,    -376,    -360,    -280,    -264,
       -312,    -296,    -472,    -456,    -504,
       -488,    -408,    -392,    -440,    -424,
        -88,     -72,    -120,    -104,     -24,
         -8,     -56,     -40,    -216,    -200,
       -248,    -232,    -152,    -136,    -184,
       -168,   -1376,   -1312,   -1504,   -1440,
      -1120,   -1056,   -1248,   -1184,   -1888,
      -1824,   -2016,   -1952,   -1632,   -1568,
      -1760,   -1696,    -688,    -656,    -752,
       -720,    -560,    -528,    -624,    -592,
       -944,    -912,   -1008,    -976,    -816,
       -784,    -880,    -848,    5504,    5248,
       6016,    5760,    4480,    4224,    4992,
       4736,    7552,    7296,    8064,    7808,
       6528,    6272,    7040,    6784,    2752,
       2624,    3008,    2880,    2240,    2112,
       2496,    2368,    3776,    3648,    4032,
       3904,    3264,    3136,    3520,    3392,
      22016,   20992,   24064,   23040,   17920,
      16896,   19968,   18944,   30208,   29184,
      32256,   31232,   26112,   25088,   28160,
      27136,   11008,   10496,   12032,   11520,
       8960,    8448,    9984,    9472,   15104,
      14592,   16128,   15616,   13056,   12544,
      14080,   13568,     344,     328,     376,
        360,     280,     264,     312,     296,
        472,     456,     504,     488,     408,
        392,     440,     424,      88,      72,
        120,     104,      24,       8,      56,
         40,     216,     200,     248,     232,
        152,     136,     184,     168,    1376,
       1312,    1504,    1440,    1120,    1056,
       1248,    1184,    1888,    1824,    2016,
       1952,    1632,    1568,    1760,    1696,
        688,     656,     752,     720,     560,
        528,     624,     592,     944,     912,
       1008,     976,     816,     784,     880,
        848
};

/// <summary>
/// Converts the specified mulaw sample to float linear.
/// </summary>
/// <param name="sample">mulaw sample to convert</param>
/// <returns>float linear PCM value</returns>
#define MulawToFloat(sample_) (mulawTable[sample_])

/// <summary>
/// Converts the specified alaw sample to float linear.
/// </summary>
/// <param name="sample">alaw sample to convert</param>
/// <returns>float linear PCM value</returns>
#define AlawToFloat(sample_) (alawTable[sample_])

/// <summary>
/// Converts the specified array of mulaw samples to 16-bit linear float.
/// </summary>
/// <param name="mulawSamples">Array of mulaw samples to convert</param>
/// <returns>Array of float linear PCM values</returns>
void MulawArrayToFloatArray(const unsigned char *mulawSamples, int length, float *result)
{
    //int length = mulawSamples.Length;
    //short[] result = new short[length];
    int i;
    for (i = 0; i < length; i++)
    {
        result[i] = MulawToFloat(mulawSamples[i]);
    }
    //return result;
}


/// <summary>
/// Converts the specified array of Alaw samples to 16-bit linear.
/// </summary>
/// <param name="alawSamples">Array of alaw samples to convert</param>
/// <returns>Array of float linear PCM values</returns>
void AlawArrayToFloatArray(const unsigned char *alawSamples, int length, float *result)
{
    //int length = alawSamples.Length;
    //short[] result = new short[length];
    int i;
    for (i = 0; i < length; i++)
    {
        result[i] = AlawToFloat(alawSamples[i]);
    }
    //return result;
}


/******************************************************************************
						Internal Enum definitions
******************************************************************************/
enum endian {e_little, e_big};

/************************************************************
*	Internal Type definitions
************************************************************/

/* Standard Wave format structure for PCM format */
typedef struct 
{ 
	unsigned short usFormatTag; 
	unsigned short usNbChannels; 
	unsigned long ulNbSamplesPerSec; 
	unsigned long ulNbAvgBytesPerSec; 
	unsigned short usNbBlockAlign; 
	unsigned short usBitsPerSample; 
} 
PCM_wave_format_struct;

/************************************************************
*	Internal Static functions for wav file handling
************************************************************/

static void check_endian(enum endian *peEndian);
static void check_wav_header(FILE *fpIn, long *plNbSamples, short *psHeaderLen, long *plFormatTag,
						long *plErrorFlag, char **ppcErrorType );
static void open_wav_header(FILE *fpIn,
						long *plNbSamples, long *plNbChannels, 
						long *plNbSamplesPerSec, 
						short *psBitsPerSample, short *psHeaderLen,
                        long *plFormatTag,
						long *plErrorFlag, char **ppcErrorType );
static long chunk_type(FILE *fpIn, char *pcType);
static long chunk_lgth(FILE * fpIn, unsigned long * pulLength);
static long is_type(long lA, char * pcB);
static long chunk_search(
						FILE *fpIn, void *pvBuf, unsigned long ulBufLen, 
						char *pcType, unsigned long * pulLength);
static long chunk_skip(FILE * fpIn, unsigned long ulLen);
static long interpret_fmt_chunk(
						unsigned char * pucBuffer, 
						PCM_wave_format_struct * ptFmt);
static void move_8to32(unsigned char **pucInput, unsigned long *pulOutput );
static void move_8to16(unsigned char **pucInput, unsigned short *pulOutput );

/************************************************************
*   select_rate
*
*   Syntax        void select_rate( long sample_rate,
*                  long * Error_Flag, char ** Error_Type )
*
*   Description
*   Sets global variables to appropriate sample-rate dependent
*    quantities.
*
*   Inputs        sample_rate    Sample rate in Hz (8000 or 16000)    
*
*   Modifies    Fs
*                Downsample
*                InIIR_Hsos
*                InIIR_Nsos
*                Align_Nfft
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL void select_rate(PESQ_t *inst, long sample_rate, long * Error_Flag, char ** Error_Type )
{
    if( inst->Fs == sample_rate )
        return;
    if( Fs_16k == sample_rate )
    {
        inst->Fs = Fs_16k;
        inst->Downsample = Downsample_16k;
        inst->InIIR_Hsos = InIIR_Hsos_16k;
        inst->InIIR_Nsos = InIIR_Nsos_16k;
        inst->Align_Nfft = Align_Nfft_16k;
        return;
    }
    if( Fs_8k == sample_rate )
    {
        inst->Fs = Fs_8k;
        inst->Downsample = Downsample_8k;
        inst->InIIR_Hsos = InIIR_Hsos_8k;
        inst->InIIR_Nsos = InIIR_Nsos_8k;
        inst->Align_Nfft = Align_Nfft_8k;
        return;
    }

    (*Error_Flag) = PESQ_ERR_INVALID_SAMPLE_RATE;
    (*Error_Type) = "Invalid sample rate specified";    
}

int file_exist( char * fname )
{
    FILE * fp = fopen( fname, "rb" );
    if( fp == NULL )
        return 0;
    else
    {
        fclose( fp );
        return 1;
    }
}


void load_src_from_alaw_wav(PESQ_t *inst, long * Error_Flag, char ** Error_Type, SIGNAL_INFO * sinfo)
{
    short header_size = 0;
    long formatTag = 0;
    long name_len;
    long file_size;
    long Nsamples;
    long to_read;
    long read_count;
	long wavflag;
	enum endian order;
    long Downsample = inst->Downsample;
    long Fs = inst->Fs;

    float *read_ptr;
    unsigned char *input_data;
    FILE *Src_file = NULL;
    errno_t err = _wfopen_s(&Src_file, sinfo->file_name, L"rb" );

    input_data = (unsigned char *) safe_malloc( 16384 * sizeof(unsigned char) );
    if( input_data == NULL )
    {
        *Error_Flag = PESQ_ERR_OUT_OF_MEMORY;
        *Error_Type = "Could not allocate storage for file reading";
        printf ("%s!\n", *Error_Type);
        fclose( Src_file );
        return;
    }

    if( err != 0 )
    {
        *Error_Flag = PESQ_ERR_FILE_OPEN_READ;
        *Error_Type = "Could not open source file";
        printf ("%s!\n", *Error_Type);
        safe_free( input_data );
        return;
    }

    if( fseek( Src_file, 0L, SEEK_END ) != 0 )
    {
        *Error_Flag = PESQ_ERR_FILE_READ;
        *Error_Type = "Could not reach end of source file";
        safe_free( input_data );
        printf ("%s!\n", *Error_Type);
        fclose( Src_file );
        return;
    }
    file_size = ftell( Src_file );
    if( file_size < 0L )
    {
        *Error_Flag = PESQ_ERR_FILE_READ;
        *Error_Type = "Could not measure length of source file";
        safe_free( input_data );
        printf ("%s!\n", *Error_Type);
        fclose( Src_file );
        return;
    }
    if( fseek( Src_file, 0L, SEEK_SET ) != 0 )
    {
        *Error_Flag = PESQ_ERR_FILE_READ;
        *Error_Type = "Could not reach start of source file";
        safe_free( input_data );
        printf ("%s!\n", *Error_Type);
        fclose( Src_file );
        return;
    }
    name_len = (long) wcslen( sinfo-> file_name );

	/* RIFF format WAVE file handling */
	wavflag=0;
    if( name_len > 4 )
    {
        if( _wcsicmp( sinfo-> file_name + name_len - 4, L".wav" ) == 0 )
            wavflag = 1;
    }

	/* WAVE file processing */
	if ( wavflag )
	{
		check_wav_header(Src_file, &Nsamples, &header_size, &formatTag, Error_Flag, Error_Type );

		if ( *Error_Flag )
        {
            safe_free( input_data );
            fclose( Src_file );
			return;
        }

		check_endian( &order );
		if ( order == e_big )
			sinfo->apply_swap = 1;
	}
	else
	{
		Nsamples = file_size - header_size * 2;
	}

    if( Nsamples < (Fs / 4) )
    {
        *Error_Flag = PESQ_ERR_INSUFFICIENT_DATA;
        *Error_Type = "Source file must contain a minimum of 0.25s of data";
        printf ("%s!\n", *Error_Type);
        safe_free( input_data );
        fclose( Src_file );
        return;
    }

    sinfo-> Nsamples = Nsamples + 2 * SEARCHBUFFER * Downsample;

    sinfo-> data =
        (float *) safe_malloc( (sinfo-> Nsamples + DATAPADDING_MSECS  * (Fs / 1000)) * sizeof(float) );
    if( sinfo-> data == NULL )
    {
        *Error_Flag = PESQ_ERR_OUT_OF_MEMORY;
        *Error_Type = "Failed to allocate memory for source file";
        printf ("%s!\n", *Error_Type);
        safe_free( input_data );
        fclose( Src_file );
        return;
    }

    read_ptr = sinfo-> data;
    for( read_count = SEARCHBUFFER*Downsample; read_count > 0; read_count-- )
      *(read_ptr++) = 0.0f;

    to_read = Nsamples;
    while( to_read > 0 )
    {
        long max_read_count = min(16384, to_read);
        read_count = (long)fread( input_data, sizeof(unsigned char), max_read_count, Src_file );
        if( read_count < max_read_count )
        {
            *Error_Flag = PESQ_ERR_FILE_READ;
            *Error_Type = "Error reading source file.";
            printf ("%s!\n", *Error_Type);
            safe_free( input_data );
            safe_free( sinfo-> data );
            sinfo-> data = NULL;
            fclose( Src_file );
            return;
        }

        to_read -= read_count;
        if (formatTag == 0 || formatTag == OWPCMINFILE_PSY_WAVE_FORMAT_ALAW)
        {
            AlawArrayToFloatArray(input_data, read_count, read_ptr);
            read_ptr += read_count;
        }
        else
        {
            short *p_input = (short *)input_data;
            while( read_count > 0 )
            {
                read_count -= sizeof(short);
                *(read_ptr++) = (float)(*(p_input++));
            }
        }
    }

    for( read_count = DATAPADDING_MSECS  * (Fs / 1000) + SEARCHBUFFER * Downsample; read_count > 0; read_count-- )
      *(read_ptr++) = 0.0f;

    fclose( Src_file );
    safe_free( input_data );

    sinfo-> VAD = (float *) safe_malloc( sinfo-> Nsamples * sizeof(float) / Downsample );
    sinfo-> logVAD = (float *) safe_malloc( sinfo-> Nsamples * sizeof(float) / Downsample );
    if( (sinfo-> VAD == NULL) || (sinfo-> logVAD == NULL))
    {
        *Error_Flag = PESQ_ERR_OUT_OF_MEMORY;
        *Error_Type = "Failed to allocate memory for VAD";
        printf ("%s!\n", *Error_Type);
        return;
    }
    printf("load_src_from_alaw_wav for %d samples returning\n", Nsamples);
}


/************************************************************
*   alloc_other
*
*   Syntax        void alloc_other( SIGNAL_INFO * ref_info, 
*                    SIGNAL_INFO * deg_info, long * Error_Flag, 
*                       char ** Error_Type, float ** ftmp)
*
*   Description
*   Allocates temporary memory storage for PESQ algorithm
*
*   Inputs        (*ref_info).Nsamples    size of reference size
*                (*deg_info).Nsamples    size of degraded size
*
*   Modifies    *ftmp
*
*   Returns        None
*
*   History
*   Created        27/02/2001 - AR
*    Documented    06/03/2001 - RR
************************************************************/
PESQ_DECL void alloc_other(PESQ_t *inst, SIGNAL_INFO * ref_info, SIGNAL_INFO * deg_info, 
        long * Error_Flag, char ** Error_Type, float ** ftmp)
{
    long Align_Nfft = inst->Align_Nfft;
    long Fs = inst->Fs;
// TODO: possible 2x diff - check with original
    *ftmp = (float *)safe_malloc(
       max( max(
            (*ref_info).Nsamples + DATAPADDING_MSECS  * (Fs / 1000),
            (*deg_info).Nsamples + DATAPADDING_MSECS  * (Fs / 1000) ),
           12 * Align_Nfft) * sizeof(float) );
    if( (*ftmp) == NULL )
    {
        *Error_Flag = PESQ_ERR_OUT_OF_MEMORY;
        *Error_Type = "Failed to allocate memory for temporary storage.";
        printf ("%s!\n", *Error_Type);
        return;
    }
}

/************************************************************
*
*	Internal Static functions for wav file handling
*
************************************************************/


/************************************************************
*
*	FUNCTION: check_endian
*
*	DESCRIPTION:
*	Checks endian-ness of operating system	
*	
*	INPUTS:
*	None
*	
*	MODIFIES:
*	peEndian - Enum specifying whether operating system is little or big endian
*	
*	RETURNS:
*	Error code
*
************************************************************/
static void check_endian(enum endian *peEndian)
{
	static short *psNum;
	static char pcNum[2] = { 1, 0 };

	psNum = (short *) pcNum;

	if(*psNum == 1) 
		*peEndian = e_little; 
	else 
		*peEndian = e_big;

	return;
}

/************************************************************
*
*	FUNCTION: check_wav_header
*
*	DESCRIPTION:
*	Checks file with name pcFileName for WAVE header.
*	Checks format specified in header is supported.
*	If an error occurs an error message is printed to stderr, 
*	
*	INPUTS:
*	*pcFileName        - Pointer to the filename string.
*
*	MODIFIES:
*	*psWaveFormat	   - Wave format type - one of
*            G711_LINPCM	Linear  PCM
*			G711_LOG_A	G.711   A-law PCM
*			G711_LOG_MU	G.711   Mu-law PCM
*	*plNbSamples	   - Number of data samples (length in bytes/(*nBlockAlign))
*	*psHeaderLen	   - WAVE header length.  0 implies raw data.
*
*	RETURNS:
*
*	Error code
*
************************************************************/
static void check_wav_header(FILE *fpIn, long *plNbSamples, short *psHeaderLen, long *plFormatTag,
						long *plErrorFlag, char **ppcErrorType )
{
	/*******************
		Declarations
	*******************/
	long lNumChannels;
	long lNumSamplesPerSec;
	short sNumBitsPerSample;

	*plErrorFlag = 0L;

	/******************************************
		Check wav header is properly formed
	******************************************/
	open_wav_header(fpIn, plNbSamples, &lNumChannels, &lNumSamplesPerSec, 
		&sNumBitsPerSample, psHeaderLen, plFormatTag, plErrorFlag, ppcErrorType );

	if(*plErrorFlag != 0) 
		return;

	/*******************************
		Check number of channels
	*******************************/
	if(lNumChannels != 1)
	{
        *plErrorFlag = PESQ_ERR_FILE_READ;
        *ppcErrorType = "WAVE file has more than 1 channel (must be 1)";
		return;
	}
	
	/************************
		Check sample rate
	************************/
	if(lNumSamplesPerSec != 8000 && lNumSamplesPerSec != 16000)
	{
        *plErrorFlag = PESQ_ERR_FILE_READ;
        *ppcErrorType = "Unsupported sample rate in WAVE file (must be 8 or 16kHz)";
		return;
	}

	/**************************************
		Check number of bits per sample
	**************************************/
    if(sNumBitsPerSample != 16 && *plFormatTag == OWPCMINFILE_PSY_WAVE_FORMAT_LINPCM)
	{
        *plErrorFlag = PESQ_ERR_FILE_READ;
        *ppcErrorType = "Unsupported PCM sample size in WAVE file (must be 16-bits per sample)";
		return;
	} 
    if (sNumBitsPerSample != 8 && *plFormatTag == OWPCMINFILE_PSY_WAVE_FORMAT_ALAW && lNumSamplesPerSec != 8000)
	{
        *plErrorFlag = PESQ_ERR_FILE_READ;
        *ppcErrorType = "Unsupported A-Law sample size or rate in WAVE file (must be 8-bits per sample @ 8kHz)";
		return;
	}

	return;
}


/************************************************************
*
*	FUNCTION: open_wave_PCM_file
*
*	DESCRIPTION:
*	Checks file for WAVE header.
*	If an error occurs an error message is printed to stderr, 
*	
*	INPUTS:
*	*psFilePtr		   - Input file pointer
*
*	MODIFIES:
*	*plNbSamples	   - Number of data samples (length in bytes/(*nBlockAlign))
*	*plNbChannels	   - Number of channels in the waveform-audio data
*	*plNbSamplesPerSec - Sample rate, in samples per second (hertz)
*	*psBitsPerSample   - Bits per sample.
*	*psHeaderLen	   - WAVE header length.  0 implies raw data.
*
*	RETURNS:
*
*	Error code - note not finding a WAVE header is not considered to be an 
*		error.  If no header is found, psHeaderLen is set to 0.
*
************************************************************/
static void open_wav_header(FILE *fpIn,
						long *plNbSamples, long *plNbChannels, 
						long *plNbSamplesPerSec, 
						short *psBitsPerSample, short *psHeaderLen,
                        long *plFormatTag,
						long *plErrorFlag, char **ppcErrorType )
{
	unsigned long ulLength;
	PCM_wave_format_struct tFmt;
	unsigned char pucFmtBuffer[OWPCMINFILE_FMT_SIZE_IN_FILE];
	long lRetCode;

	*plErrorFlag = 0L;

	*psHeaderLen = 0;
	
	/* Search for RIFF format */
	if( chunk_type(fpIn, "RIFF") == 0 ) 
	{
        *plErrorFlag = PESQ_ERR_FILE_READ;
        *ppcErrorType = "File with .wav suffix is not a WAVE file";
		return;
	}
	if( chunk_lgth(fpIn, &ulLength) != 0)
	{
		*plErrorFlag = PESQ_ERR_FILE_READ;
        *ppcErrorType = "WAVE file format error";
		return;
	};

	/* Check for WAVE chunk */
	if( chunk_type(fpIn, "WAVE") == 0 )
	{
		*plErrorFlag = PESQ_ERR_FILE_READ;
        *ppcErrorType = "File with .wav suffix is not a WAVE file";
		return;
	}
	
	/* Search for format chunk */
	do
	{
		lRetCode = chunk_search(fpIn, pucFmtBuffer, 
			OWPCMINFILE_FMT_SIZE_IN_FILE, "fmt ", &ulLength);

		if( lRetCode < 0 ) break;
	}
	while( lRetCode == 0 );
	
	if( ulLength < OWPCMINFILE_FMT_SIZE_IN_FILE )
	{
		*plErrorFlag = PESQ_ERR_FILE_READ;
        *ppcErrorType = "File with .wav suffix is not a WAVE file";
		return;
	}
				
	interpret_fmt_chunk(pucFmtBuffer, &tFmt);

	if (tFmt.usFormatTag != OWPCMINFILE_PSY_WAVE_FORMAT_LINPCM &&
        tFmt.usFormatTag != OWPCMINFILE_PSY_WAVE_FORMAT_ALAW)
	{		
		*plErrorFlag = PESQ_ERR_FILE_READ;
        *ppcErrorType = "WAVE file is not PCM or A-Law format";
		return;
	}

	*plNbChannels = (long) tFmt.usNbChannels;
	*plNbSamplesPerSec = (long) tFmt.ulNbSamplesPerSec;
	*psBitsPerSample = (short) tFmt.usBitsPerSample;
    *plFormatTag = (long) tFmt.usFormatTag;
	
	if( (tFmt.usNbBlockAlign * 8) != (tFmt.usBitsPerSample * tFmt.usNbChannels) )
	{
		*plErrorFlag = PESQ_ERR_FILE_READ;
        *ppcErrorType = "WAVE file has invalid block align field";
		return;
	}
	
	if( tFmt.ulNbAvgBytesPerSec != (tFmt.usNbBlockAlign * tFmt.ulNbSamplesPerSec) )
	{
		*plErrorFlag = PESQ_ERR_FILE_READ;
        *ppcErrorType = "WAVE file has invalid bytes per second field";
		return;
	}
	
	/* Search for data chunk */
	while( (chunk_type(fpIn, "data") == 0) && !feof(fpIn) )
	{ 
		if(chunk_lgth(fpIn, &ulLength) != 0)
		{
			*plErrorFlag = PESQ_ERR_FILE_READ;
			*ppcErrorType = "No data in WAVE file";
			return;
		};
		if(chunk_skip(fpIn, ulLength) != 0)
		{
			*plErrorFlag = PESQ_ERR_FILE_READ;
			*ppcErrorType = "No data in WAVE file";
			return;
		};

	}	
	if( feof(fpIn) )
	{
		*plErrorFlag = PESQ_ERR_FILE_READ;
		*ppcErrorType = "No data in WAVE file";
		return;
	}

	if( chunk_lgth(fpIn, &ulLength) != 0)
	{
		*plErrorFlag = PESQ_ERR_FILE_READ;
		*ppcErrorType = "WAVE file does not have data length field";
		return;
	}
	*plNbSamples = (long)(ulLength / ((long)tFmt.usNbBlockAlign));
	
	*psHeaderLen = (short) ftell(fpIn);

	return;

}


/************************************************************
*
*	FUNCTION: chunk_type
*
*	DESCRIPTION:
*	Read a 4-byte chunk header field and check if 
*	it is the type that we are searching for.
*
*	INPUTS:
*	fpIn - File handler
*	pcType - Type to compare with
*	
*	MODIFIES:
*
*	RETURNS:
*	0 if the read chunck is not the searched type,
*	1 if it is.
*
************************************************************/
static long chunk_type(FILE *fpIn, char *pcType)
{
	long lHead;

	lHead = 0;
	if( fread(&lHead, 4, 1, (FILE *) fpIn) != 1 ) return 0;

	return is_type(lHead, pcType);
}

/************************************************************
*
*	FUNCTION: chunk_lgth
*
*	DESCRIPTION:
*	Read the chunk length field.  
*
*	INPUTS:
*	fpIn - File handler
*
*	MODIFIES:
*	pulLength - Chunk Length
*
*	RETURNS:
*	0 if length successfully read, -1 if an error occurs.
*
************************************************************/
static long chunk_lgth(FILE * fpIn, unsigned long * pulLength)
{
	unsigned char pucLength[4];
	unsigned char * pucLength2;

	pucLength2 = pucLength;

	if( fread(pucLength, 1, 4, (FILE *) fpIn) != 4 ) return -1;
	move_8to32( &pucLength2, pulLength);

	return 0;
}


/************************************************************
*
*	FUNCTION: is_type
*
*	DESCRIPTION:
*	Compare 4-byte type fields, which consist of four characters,
*	ignoring case.
*
*	INPUTS:
*	lA - long integer storing the first number
*	pcB - Pointer to the part of memory storing the second number
*
*	MODIFIES:
*
*	RETURNS:
*	1 if it is equal, 0 if it is not.
*
************************************************************/
static long is_type(long lA, char * pcB)
{
	char pcCA[4];
	long iI;

	*((long *)pcCA) = lA;
	for( iI = 0; iI < 4; iI++ ) 
	{ 
		if( (pcCA[iI] & 95) != (pcB[iI] & 95) ) return 0; 
	}

	return 1;
}


/************************************************************
*
*	FUNCTION: chunk_search
*
*	DESCRIPTION:
*	Search for a RIFF chunk of the specified type.
*	If the specified type is found, the format/payload information is stored
*	in pvBuf, which should be at least as long as the required data size.  The
*	actual number of bytes read is returned.
*
*	Many RIFF types can contain optional information appended to the end of
*	a standard chunk - if you are interested in this data, declare sufficient
*	space and check the length that is returned.
*
*	INPUTS:
*	fpIn - File handler
*	ulBufLen - Number of elements in the buffer
*	pcType - Type
*
*	MODIFIES:
*	pvBuf - Pointer to the buffer
*	pulLength - Number of bytes read if chunk is of specified type or number
*		 of bytes skipped if chunk is of a different type
*
*	RETURNS:
*	If a chunk of a different type is found, the function skips the whole
*	chunk and returns zero. If an error occurs, -1 is returned. If a chunk
*	of the required type is found, 1 is returned.
*
************************************************************/
static long chunk_search(FILE *fpIn, void *pvBuf, unsigned long ulBufLen, 
	char *pcType, unsigned long * pulLength)
{
	long iRightType;
	long lRetCode;
	unsigned long ulReadLength;	

	iRightType = chunk_type(fpIn, pcType);
	lRetCode= chunk_lgth(fpIn, pulLength );
	if( lRetCode != 0 ) return -1;

	ulReadLength = *pulLength;	
	
	if( iRightType )
	{
		if( ulReadLength > ulBufLen ) ulReadLength = ulBufLen;
		if( fread(pvBuf, 1, ulReadLength, fpIn) != ulReadLength ) return -1;
		if(chunk_skip(fpIn, *pulLength - ulReadLength) != 0) return -1;
		return 1;
	}
	else
	{
		if(chunk_skip(fpIn, *pulLength) != 0) return -1;
		return 0;
	}
}

/************************************************************
*
*	FUNCTION: chunk_skip
*
*	DESCRIPTION:
*	Skip the specified number of bytes
*
*	INPUTS:
*	fpIn - File handler
*	ulLen - Number of bytes to skip
*
*	MODIFIES:
*
*	RETURNS:
*	Zero if successful, non-zero if not successful
*
************************************************************/
static long chunk_skip(FILE * fpIn, unsigned long ulLen)
{
	if( ulLen > 0 ) return fseek((FILE *) fpIn, ulLen, SEEK_CUR);
	return 0;
}

/************************************************************
*
*	FUNCTION: interpret_fmt_chunk
*
*	DESCRIPTION:
*	Interprets the fields in the fmt chunk, correctly taking account of the
*	Endian-ness of the machine.
*
*	INPUTS:
*	pucBuffer - pointer to fmt data read directly from file
*
*	MODIFIES:
*	ptFmt - pointer to correctly interpreted fmt data
*
*	RETURNS:
*	Error status
*
************************************************************/
static long interpret_fmt_chunk(unsigned char * pucBuffer, PCM_wave_format_struct * ptFmt)
{
	move_8to16(&pucBuffer, &(ptFmt->usFormatTag));
	move_8to16(&pucBuffer, &(ptFmt->usNbChannels));
	move_8to32(&pucBuffer, &(ptFmt->ulNbSamplesPerSec));
	move_8to32(&pucBuffer, &(ptFmt->ulNbAvgBytesPerSec));
	move_8to16(&pucBuffer, &(ptFmt->usNbBlockAlign));
	move_8to16(&pucBuffer, &(ptFmt->usBitsPerSample));

	return NO_ERROR;
}

/************************************************************
*
*	FUNCTION: move_8to32
*
*	DESCRIPTION:
*	Copies the contents of 4 bytes into a 32-bit word using 
*	little-endian convention.
*
*	INPUTS:
*	pucInput - pointer to pointer to first input byte
*	pulOutput - pointer to 32-bit output
*
*	MODIFIES:
*	*pulOutput - data loaded into output
*	*pucInput - pointer to next unread byte
*		
*	RETURNS:
*	None
*
************************************************************/
static void move_8to32( unsigned char **pucInput, unsigned long *pulOutput )
{
	short i;
	*pulOutput=0x0L;
	for (i=3; i>=0; i--) {
		*pulOutput<<=8;
		*pulOutput|=(*pucInput)[i];
	}
	(*pucInput)+=4;
}

/************************************************************
*
*	FUNCTION: move_8to16
*
*	DESCRIPTION:
*	Copies the contents of 2 bytes into a 16-bit word using 
*	little-endian convention.
*
*	INPUTS:
*	pucInput - pointer to pointer to first input byte
*	pusOutput - pointer to 16-bit output
*
*	MODIFIES:
*	*pusOutput - data loaded into output
*	*pucInput - pointer to next unread byte
*		
*	RETURNS:
*	None
*
************************************************************/
static void move_8to16( unsigned char **pucInput, unsigned short *pusOutput )
{
	short i;
	*pusOutput=0x0L;
	for (i=1; i>=0; i--) {
		*pusOutput<<=8;
		*pusOutput|=(*pucInput)[i];
	}
	(*pucInput)+=2;
}


/* END OF FILE */
