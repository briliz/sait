/***********************************************************
*
*  PESQ C library implementation 
*
*  pesqdll.c      DLL function interface program.
*
*  PESQ-ITU ANSI C source code
*  Copyright (C) British Telecommunications plc, 2000.
*  Copyright (C) Royal KPN NV, 2000.
*
*  Additional modifications
*  Copyright (C) Psytechnics Limited, 2001-2006.
*  All rights reserved.
*
*  For more information
*  - visit our websites www.psytechnics.com or www.pesq.net
*
*  - write to us at
*      Psytechnics Limited, Fraser House
*      23 Museum Street, Ipswich IP1 1HN, United Kingdom
*
*  - e-mail us at  info@psytechnics.com
*
************************************************************
*
*  Functions contained:
*   PESQ_version 
*   PESQ_core_run
*
************************************************************
*
*  Version control
*   $Revision: 1.16 $
*
*	31/3/2006		Release 2.2
*		PESQ API calls changed to include LV_PESQ_P862_2 
*
*   30/10/2004		Release 2.1
*	   PESQ_core_run API changed to
*	   include additional parameter for 862.1 score
*	   Internal Comment C2.1.1
*
*   04/10/2002		Release 2.0
*      Deleted API function PESQ_run and changed
*      PESQ_ext_run to PESQ_core_run, with minor
*      additional modifications.
*	   Version format changed.
*
*   14/11/2001		Release 1.4
*      Modifications to the API of PESQ_ext_run to allow new
*      PESQ-LQ score and new level and spectral measures to be
*      returned.
*      New PESQ model -1 for version 1.0-1.3 processing.
*
*   21/05/2001		Release 1.3
*      New API call PESQ_ext_run allowing additional information
*      to be returned.
*
*      Modified function PESQ_run to include copy protection.
*
*   07/03/2001		Release 1.2
*
***********************************************************/
#define _CRT_SECURE_NO_WARNINGS

#include <stdlib.h>
#include <math.h>
#include "pesq.h"
#include "dsp.h"
#include "pesqerr.h"


/***********************************************************
*  Export declarations.
*
*  This code can be used to build a number of different types of
*  library/executable by pre-defining the appropriate macro.
*
*  Stand-alone Windows DLL:
*     WATCOM C: pre-define WATCOMC_DLL
*     Microsoft Visual C++: pre-define MSVC_DLL
*
*  Static library or static linking as part of a project to
*  build executable code: pre-define empty macro PESQExport.
*
*  Dynamically linked library on other systems:
*  pre-define PESQExport as appropriate.
*
*  By default, if none of the above macros are defined,
*  PESQExport is assumed to be empty e.g. for static linking.
***********************************************************/

#ifdef WATCOMC_DLL
  #define PESQExport __export __stdcall
#endif
#ifdef MSVC_DLL
  #define PESQExport __declspec ( dllexport ) __stdcall
#endif
#ifndef PESQExport
  #define PESQExport
#endif

/* Function declarations - see pesqdll.h for documentation */
#include "pesqdll.h"

/************************************************************
*   PESQ_version
*   See pesqdll.h for documentation
************************************************************/
long PESQExport PESQ_version( void )
{
    return (2L << 24) + (2L << 16) + (0L << 8) + 0L;
}

/************************************************************
*   PESQ_core_run
*   See pesqdll.h for documentation
************************************************************/
void PESQExport PESQ_core_run(
  float * LV_ref_data, long LV_ref_Nsamples, long LV_Reserved_ref,
  float * LV_deg_data, long LV_deg_Nsamples, long LV_Reserved_deg,
  long LV_Fs, long LV_Model,
  long LV_Rtn_Bands, long LV_Nframes,
  long * LV_Error_Flag,
  float * LV_PESQ_score, float * LV_PESQ_LQ, 
  float * LV_PESQ_Ie, float LV_Params[100],
  float * LV_ref_surf, float * LV_deg_surf,
  long * LV_FrameDelay, float * LV_Frame_score,
  float * LV_Sframe_err, float * LV_Aframe_err,
  float * LV_TFE,
  float * LV_PESQ_P862_2, float * LV_PESQ_P862_1 )
{
    SIGNAL_INFO ref_info;
    SIGNAL_INFO deg_info;
    ERROR_INFO err_info;
    long count, frame, band;
    
    char * Error_Type = "Unknown error type.";

    PESQ_t *inst = create_new_pesq_instance();

    /* Basic initialisation */
    *LV_Error_Flag = 0;
    wcscpy (ref_info.file_name, L"");
    ref_info.apply_swap = 0;
    wcscpy (deg_info.file_name, L"");
    deg_info.apply_swap = 0;
    err_info.model = LV_Model;
    err_info.Nutterances = 0;
    err_info.tools = 0;

    /* Initialise the elements in err_info that will be used to pass
       data back out to the calling code. */
    err_info.LV_ref_data = LV_ref_data;
    err_info.LV_ref_Nsamples = LV_ref_Nsamples;
    err_info.LV_deg_data = LV_deg_data;
    err_info.LV_deg_Nsamples = LV_deg_Nsamples;
    err_info.rtn_bands = LV_Rtn_Bands;
    err_info.LV_Nframes = LV_Nframes;
    err_info.LV_PESQ_score = LV_PESQ_score;
    err_info.LV_PESQ_LQ = LV_PESQ_LQ;
	err_info.LV_PESQ_P862_1 = LV_PESQ_P862_1;
	err_info.LV_PESQ_P862_2 = LV_PESQ_P862_2;
    err_info.LV_PESQ_Ie = LV_PESQ_Ie;
    err_info.LV_Params = LV_Params;
    err_info.LV_ref_surf = LV_ref_surf;
    err_info.LV_deg_surf = LV_deg_surf;
    err_info.LV_ref_surf_preeq = NULL;
    err_info.LV_deg_surf_preeq = NULL;
    err_info.LV_FrameDelay = LV_FrameDelay;
    err_info.LV_Frame_score = LV_Frame_score;
    err_info.LV_Sframe_err = LV_Sframe_err;
    err_info.LV_Aframe_err = LV_Aframe_err;
    err_info.LV_TFE = LV_TFE;
    err_info.LV_Class_err = NULL;
    err_info.LV_Diagnosis = NULL;
    err_info.LV_Level_Spectra = NULL;
    err_info.LV_DlyHistCentres = NULL;
    err_info.LV_DlyHistCount = NULL;
    err_info.LV_Time_err = NULL;
    err_info.LV_Clipping_est = NULL;
    err_info.LV_Lin_Spectra = NULL;
    err_info.LV_Lin_tfe = NULL;
    err_info.LV_ref_spectrogram = NULL;
    err_info.LV_ref_lpc_spectrogram = NULL;
    err_info.LV_ref_excitation = NULL;
    err_info.LV_ref_speech_parms = NULL;
    err_info.LV_deg_spectrogram = NULL;
    err_info.LV_deg_lpc_spectrogram = NULL;
    err_info.LV_deg_excitation = NULL;
    err_info.LV_deg_speech_parms = NULL;

    /* Check sampling rate and model */
    if( (*LV_Error_Flag == 0) && (LV_Fs != 8000) && (LV_Fs != 16000) ) {
        *LV_Error_Flag = PESQ_ERR_INVALID_SAMPLE_RATE;
        Error_Type = "Invalid sample rate";
    }
    if( (*LV_Error_Flag == 0) && (LV_Model != 0) && (LV_Model != -1) &&
        (LV_Model != 1) && (LV_Model != 3) ) {
        *LV_Error_Flag = PESQ_ERR_INVALID_MODEL;
        Error_Type = "Invalid model";
    }

    /* Initialise output arrays */
    if( LV_ref_surf != NULL ) {
        /* ref_surf is the reference psychoacoustic loudness surface,
           LV_Rtn_Bands bands x LV_Nframes */
        for( frame = 0; frame < LV_Nframes; frame++ )
            for( band = 0; band < LV_Rtn_Bands; band++ )
                LV_ref_surf[frame*LV_Rtn_Bands+band] = 0.0f;
    }
    if( LV_deg_surf != NULL ) {
        /* deg_surf is the degraded psychoacoustic loudness surface,
           LV_Rtn_Bands bands x LV_Nframes */
        for( frame = 0; frame < LV_Nframes; frame++ )
            for( band = 0; band < LV_Rtn_Bands; band++ )
                LV_deg_surf[frame*LV_Rtn_Bands+band] = 0.0f;
    }
    if( LV_TFE != NULL ) {
        /* TFE is the transfer function estimate over LV_Rtn_Bands bands */
        for( band = 0; band < LV_Rtn_Bands; band++ )
            LV_TFE[band] = 0.0f;
    }
    if( LV_Frame_score != NULL ) {
        /* Frame_score is the frame-by-frame estimate of PESQ score. */
        for( frame = 0; frame < LV_Nframes; frame++ )
            LV_Frame_score[frame] = 4.5f;
    }
    if( LV_Sframe_err != NULL ) {
        /* Sframe_err is the frame-by-frame symmetric disturbance. */
        for( frame = 0; frame < LV_Nframes; frame++ )
            LV_Sframe_err[frame] = 0.0f;
    }
    if( LV_Aframe_err != NULL ) {
        /* Aframe_err is the frame-by-frame asymmetric disturbance. */
        for( frame = 0; frame < LV_Nframes; frame++ )
            LV_Aframe_err[frame] = 0.0f;
    }
    if( LV_Params != NULL ) {
        /* Params receives miscellaneous internal variables */
        for( count = 0; count < 100; count++ )
            LV_Params[count] = 0.0f;
    }
    if( LV_FrameDelay != NULL ) {
        /* FrameDelay is the frame-by-frame delay, initialised to ref_Nsamples
           as a flag to show when no delay value exists for a frame. */
        for( frame = 0; frame < LV_Nframes; frame++ )
            LV_FrameDelay[frame] = LV_ref_Nsamples;
    }

    /* Run the model */
    if( *LV_Error_Flag == 0 )
        select_rate(inst, LV_Fs, LV_Error_Flag, &Error_Type );
    if( *LV_Error_Flag == 0 )
        pesq_measure(inst, &ref_info, &deg_info, &err_info, LV_Error_Flag, &Error_Type );

    free_pesq_instance(&inst);


    /* Return */
}

/* END OF FILE */
